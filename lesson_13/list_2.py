import random

num_lists = 150
for list_index in range(0, num_lists):
    line = ''
    num_elements = random.randint(50, 100)
    for element_index in range(0, num_elements):
        line += str(random.randint(0, 100000)) + ','
    print(line[:-1])
