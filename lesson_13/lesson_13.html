<!DOCTYPE html>
<meta charset="UTF-8">
<html>
<head>
    <link href="../common/prism.css" rel="stylesheet" />
    <link rel="stylesheet" href="../common/styles.css">
</head>
<script src="../common/prism.js"></script>
<script src="../common/settings.js"></script>
<body>
<div class="centered" style="max-width: 55em;">

<h1>
    Lesson 13: Common Data Structures
</h1>

<p>
    We have learned about the primitive data types as well as arrays. Arrays are great, but they have some severe limitations, and definitely don't cover all cases where we want to collect data together.
</p>

<p>
    In this lesson, we'll look at some classes that Java provides to manage our data better. This will also be a good opportunity to explore classes and objects more.
</p>

<h2>
    List
</h2>

<p>
    One of the greatest weaknesses of an array is that the size is fixed. If you don't know the maximum number of items you need to hold, it will be difficult to use an array.
</p>

<p>
    A <code>List</code> is an ordered collection of 0 or more values, all having the same type. Java provides a few classes that behave like a <code>List</code>, but the one we're going to use is called <code>ArrayList</code>. It is so named because it uses an array to store the data. There are lots of different ways to store data!
</p>

<h3>
    Exercise 0
</h3>

<pre class="language-java"><code>
import java.util.List;
import java.util.ArrayList;

public class Ex0 {
    public static void main(String[] args) {
        List&lt;Integer&gt; example = new ArrayList&lt;&gt;();
        for (int count = 0; count < 15; count++) {
            example.add(100 + count);
        }

        System.out.println("Length: " + example.size());
        for (int index = 0; index < example.size(); index++) {
            System.out.print(example.get(index) + ",");
        }
        System.out.println();

        example.add(10, 200);
        System.out.println("Length: " + example.size());
        for (int index = 0; index < example.size(); index++) {
            System.out.print(example.get(index) + ",");
        }
        System.out.println();

        Integer removed = example.remove(12);
        System.out.println("Removed index 12, value " + removed);
        System.out.println("Length: " + example.size());
        for (int index = 0; index < example.size(); index++) {
            System.out.print(example.get(index) + ",");
        }
        System.out.println();

        System.out.println("Contains a 155? " + example.contains(155));
        System.out.println("Contains a 104? " + example.contains(104));
        System.out.println("Index of 104: " + example.indexOf(104));
    }
}

</pre></code>

Output
<pre class="lang-xxx"><code>
> java Ex0
Length: 15
100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,
Length: 16
100,101,102,103,104,105,106,107,108,109,200,110,111,112,113,114,
Removed index 12, value 111
Length: 15
100,101,102,103,104,105,106,107,108,109,200,110,112,113,114,
Contains a 155? false
Contains a 104? true
Index of 104: 4
</code></pre>

<p>
    This should generally make sense, but we'll go through a few points.
</p>

<p>
    Remember that when we create an object, we use the <code>new</code> keyword with the object's <strong>constructor</strong>. The constructor is a function that creates the object. This is something that sets objects apart from data like <code>int</code>s or <code>double</code>s. (Strings are a special case because they are so common.)
</p>

<p>
    <code>List</code> is something known as a <strong>template</strong> type, meaning we have to specify what type of data we want the object to hold. We use the angle brackets <code>&lt;</code>, <code>&gt;</code> to do this. A template type must be a class, it cannot be a primitive, which means we use <code>Integer</code> and not <code>int</code>. You can repeat this in the constructor, but it has to match the declaration. You can shortcut with <code>&lt;&gt;</code> and Java will figure it out for you.
</p>

<p>
    This means we can have a <code>List</code> of <code>Integer</code>s, or a <code>List</code> of <code>Double</code>s, or even a <code>List</code> of <code>List</code>s, or custom classes.
</p>

<p>
    When you have an object, you can access that object's functions by using a dot (<code>.</code>) followed by the function call. We saw some examples of that above. <code>example.size()</code>, <code>example.add(100)</code>. Think of an object like an object in the real world with a bunch of buttons attached to it. Each button is a function. When you call a function, you are not using a "loose" function like in math that just exists, the function <em>belongs to</em> that object.
</p>

<p>
    This is true of all functions in Java. <code>println</code> belongs to the <code>System.out</code> object. <code>System.out</code> is an object that manages output to the terminal. You have to use that object if you want to print something out. The other functions you have been writing belong to your main class.
</p>

<p>
    Java is a heavily object oriented programming language. Not all languages are like this, most others will let you create functions that do not belong to any particular object.
</p>

<p>
    As long as we're in Java, remember that functions belong to objects. Think of it like hitting a button on a device you hold in your hands. The button is on the device. It will use what's in the device (plus whatever else you give it) to do it's job.
</p>

<p>
    You can see that we can shove as many elements into the list as we like by using <code>add</code>. The list will grow to accept whatever we put in. The actual maximum number of elements is the max value of an <code>int</code>, or 2,147,483,647. When you "press this button", you need to tell it what you want to add to the list, and then it changes the list to contain the new data.
</p>

<p>
    You can also insert an element into the middle of the list by specifying the index. You can see that the 200 gets placed at index 10 and everything shifts to the right.
</p>

<p>
    Elements can be removed by index. <code>remove</code> returns the element that was removed.
</p>

<p>
    Since Lists do not have a fixed size, you will often find that you need to check the size of the list using the <code>size</code> function. You might need this if you want to loop through all items in a list, or make sure you aren't about to access an element that doesn't exist.
</p>

<p>
    Lists are search-able, you can check if it contains a given object, and find where the object is in the list.
</p>

<p>
    For a complete reference, see <a href=https://docs.oracle.com/javase/8/docs/api/java/util/List.html>the official documentation</a>. To effectively use these collections, you will need to become good at reading this documentation.
</p>

<h2>
    Map
</h2>

<p>
    <code>List</code>s work if we care about keeping our data in a certain order, but that is not always true. <code>Map</code>s make it easier to reason about an orderless collection, and it is almost always faster than searching the list over and over again trying look things up.
</p>

<p>
    A <code>Map</code> "maps" keys to values. This works a lot like a function. You can ask for the value that goes with a given key, but not really the other way around. In the example below, we have a map that maps cities to baseball teams. If you know the city, you can get the team, see how the arrow only goes one way?
</p>

<div class="center-children">
    <img src="img/python_dict.png" style="width: 50%;">
    <p><em>Image from https://www.digitalvidya.com/blog/python-dictionary/</em></p>
</div>

<p>
    Just like <code>List</code>, there are several classes that act like <code>Map</code>s, but we will use <code>HashMap</code>.
</p>

<h3>
    Exercise 1
</h3>

<pre class="language-java"><code>
import java.util.HashMap;

public class Ex1 {
    public static void main(String[] args) {
        HashMap&lt;String, String&gt; capitals = new HashMap&lt;&gt;();
        capitals.put("md", "Annapolis");
        capitals.put("va", "Richmond");
        capitals.put("pa", "Harrisburg");
        capitals.put("de", "Dover");
        capitals.put("wv", "Charleston");
        System.out.println("States tracked: " + capitals.size());

        System.out.println("Maryland: " + capitals.get("md"));
        System.out.println("Arkansas: " + capitals.get("ak"));

        String nonsense = capitals.get("dne");
        if (nonsense == null) {
            System.out.println("There is no state called 'dne'!");
        }

        System.out.println("We moved the capital of Maryland!");
        capitals.put("md", "Columbia");
        System.out.println("Maryland: " + capitals.get("md"));

        System.out.println("Delaware fell into the ocean!");
        capitals.remove("de");
        System.out.println("States remaining: " + capitals.size());
        for (String state : capitals.keySet()) {
            System.out.println(state + ", " + capitals.get(state));
        }
    }
}

</code></pre>

Output

<pre class="lang-xxx"><code>
> java Ex1
States tracked: 5
Maryland: Annapolis
Arkansas: null
There is no state called 'dne'!
We moved the capital of Maryland!
Maryland: Columbia
Delaware fell into the ocean!
States remaining: 4
pa, Harrisburg
md, Columbia
va, Richmond
wv, Charleston

</code></pre>

<p>
    <code>Map</code> is also a template class, but we have two types of data, the key type and the value type. They are allowed to be different.
</p>

<p>
    <code>Map</code> dynamically resizes just like <code>List</code>, we can add as many elements as we want in.
</p>

<p>
    To add a new item to the map, you can use <code>put</code>, which lets you give the key and the value of the new element.
</p>

<p>
    To access a value by its key, use <code>get</code>.
</p>

<p>
    If you try to access a key that doesn't exist, <code>null</code> will be returned. We haven't seen this before. <code>null</code> doesn't mean "0", it literally means nothing is there. There is no data at all. This is another difference between objects and primitives. Primitives always have some value, objects might have nothing at all.
</p>

<p>
    You can check if an object is <code>null</code> as demonstrated in the exercise. If you forget to do this, you will run into exceptions if you ever try to do anything with a <code>null</code> object, so be careful!
</p>

<p>
    <code>put</code> also updates entries if they already exist. This is pretty handy, in a lot of situations we can just <code>put</code> new data and not worry if old data was there.
</p>

<p>
    To remove elements, use <code>remove</code> with the key.
</p>

<p>
    You <em>can</em> iterate over all entries in a <code>Map</code> as if it were a list, but they won't be in a particular order. You can see an example of that at the bottom of the exercise. The <code>keySet</code> function returns a list of all the keys, which lets us check each one's value. Look at how the order is different from the order we inserted the keys. If you need your data to be in order, you probably need a list. There are more advanced cases, but you won't encounter them in this class.
</p>

<p>
    Full documentation <a href=https://docs.oracle.com/javase/8/docs/api/java/util/Map.html>here</a>.
</p>

<h2>
    Guided Problem Solving
</h2>

<p>
    I'm going to assign a couple "homework" assignments here that I will work through in class. Please take a stab at them so we can discuss solutions in class. I will be walking through the solution, but I don't intend to just show the answer, this will be a cooperative process where I teach you how to think and solve problems.
</p>

<h3>
    Lists
</h3>

<p>
    Given the following initial code, write a program that searches a List of Lists to find the largest number in the list. The provided code will read the file given as the first program argument and put the list into the <code>data</code> variable.
</p>

<p>
    Why would you ever want a list of lists? It's very useful to represent a 2D space, like a screen. Each element of the first list represents a row of pixels, and your screen has many rows. There are many other useful applications.
</p>

<p>
    To do this, you will need to make one loop that goes through each item in the first list, and within that, a second loop to go through each number and track the biggest one.
</p>

<p>
    Jump down to line 38 to see where you should start. You don't really need to understand the function, I'm just giving you a way to read in some data from a file
</p>

<pre class="language-java line-numbers"><code>
import java.util.ArrayList;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.Scanner;
import java.io.IOException;

public class MaxList {
    public static ArrayList&lt;ArrayList&lt;Integer&gt;&gt; load(Path filePath) {
        ArrayList&lt;ArrayList&lt;Integer&gt;&gt; data = new ArrayList&lt;&gt;();
        try {
            Scanner fileScanner = new Scanner(filePath.toFile());
            while (fileScanner.hasNextLine()) {
                String line = fileScanner.nextLine();
                if (line.length() == 0) {
                    continue;
                }
                ArrayList&lt;Integer&gt; lineData = new ArrayList&lt;&gt;();
                for (String entry : line.split(",")) {
                    lineData.add(Integer.parseInt(entry.trim()));
                }
                data.add(lineData);
            }
            return data;
        }
        catch (IOException e) {
            System.out.println("Failed to read your file!");
            System.exit(0);
        }
        return null;
    }

    public static void main(String[] args) {
        // Get the input file from the program arguments
        Path inputFilePath = Paths.get(args[0]);

        ArrayList&lt;ArrayList&lt;Integer&gt;&gt; data = load(inputFilePath);

        // Find the maximum value in data and print it out
    }
}

</code></pre>

<p>
    Try out the inputs <code>list_1.txt</code> and <code>list_2.txt</code> to test your code.
</p>

<h3>
    Maps
</h3>

<p>
    Given the following initial code, use a map to count how often each letter appears in a file. The provided code will read the file given as the first program argument into the <code>text</code> variable.
</p>

<p>
    This is known as a frequency analysis and is helpful for cracking a type of code called a Caesar cipher. This program will take encrypted text and produce a frequency analysis which will help you determine the original message.
</p>

<p>
    You should create a map with the key type <code>Character</code> and the value type <code>Integer</code>. Use a for loop iterate over all characters in the string. For each character, if it is already in the map, update the value to be 1 more than it was before. This will "count" the fact that we have seen the letter. If it is not in the map, just insert 1 because it's the first time we've seen it.
</p>

<p>
    You may need to search the Internet to figure out how to do some of these things.
</p>

<pre class="language-java"><code>
import java.util.HashMap;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.Scanner;
import java.io.IOException;

public class FreqAnalyze {
    public static String load(Path filePath) {
        StringBuilder data = new StringBuilder();
        try {
            Scanner fileScanner = new Scanner(filePath.toFile());
            while (fileScanner.hasNextLine()) {
                data.append(fileScanner.nextLine());
            }
        }
        catch (IOException e) {
            System.out.println("Failed to read your file!");
            System.exit(0);
        }
        return data.toString();
    }

    public static void main(String[] args) {
        String text = load(Paths.get(args[0]));

        HashMap&lt;Character, Integer&gt; map = new HashMap&lt;&gt;();
    }
}

</code></pre>

<p>
    Use the included <code>cipher.txt</code> to test your code. Can you figure out the shift and what book this is from?
</p>

</div>
</body>
</html>
