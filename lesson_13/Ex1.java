import java.util.HashMap;

public class Ex1 {
    public static void main(String[] args) {
        HashMap<String, String> capitals = new HashMap<>();
        capitals.put("md", "Annapolis");
        capitals.put("va", "Richmond");
        capitals.put("pa", "Harrisburg");
        capitals.put("de", "Dover");
        capitals.put("wv", "Charleston");
        System.out.println("States tracked: " + capitals.size());

        System.out.println("Maryland: " + capitals.get("md"));
        System.out.println("Arkansas: " + capitals.get("ak"));

        String nonsense = capitals.get("dne");
        if (nonsense == null) {
            System.out.println("There is no state called 'dne'!");
        }

        System.out.println("We moved the capital of Maryland!");
        capitals.put("md", "Columbia");
        System.out.println("Maryland: " + capitals.get("md"));

        System.out.println("Delaware fell into the ocean!");
        capitals.remove("de");
        System.out.println("States remaining: " + capitals.size());
        for (String state : capitals.keySet()) {
            System.out.println(state + ", " + capitals.get(state));
        }
    }
}
