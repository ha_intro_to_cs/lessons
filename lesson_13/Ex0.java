import java.util.List;
import java.util.ArrayList;

public class Ex0 {
    public static void main(String[] args) {
        List<Integer> example = new ArrayList<>();
        for (int count = 0; count < 15; count++) {
            example.add(100 + count);
        }

        System.out.println("Length: " + example.size());
        for (int index = 0; index < example.size(); index++) {
            System.out.print(example.get(index) + ",");
        }
        System.out.println();

        example.add(10, 200);
        System.out.println("Length: " + example.size());
        for (int index = 0; index < example.size(); index++) {
            System.out.print(example.get(index) + ",");
        }
        System.out.println();

        Integer removed = example.remove(12);
        System.out.println("Removed index 12, value " + removed);
        System.out.println("Length: " + example.size());
        for (int index = 0; index < example.size(); index++) {
            System.out.print(example.get(index) + ",");
        }
        System.out.println();

        System.out.println("Contains a 155? " + example.contains(155));
        System.out.println("Contains a 104? " + example.contains(104));
        System.out.println("Index of 104: " + example.indexOf(104));
    }
}
