import java.util.ArrayList;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.Scanner;
import java.io.IOException;

public class MaxList {
    public static ArrayList<ArrayList<Integer>> load(Path filePath) {
        ArrayList<ArrayList<Integer>> data = new ArrayList<>();
        try {
            Scanner fileScanner = new Scanner(filePath.toFile());
            while (fileScanner.hasNextLine()) {
                String line = fileScanner.nextLine();
                if (line.length() == 0) {
                    continue;
                }
                ArrayList<Integer> lineData = new ArrayList<>();
                for (String entry : line.split(",")) {
                    lineData.add(Integer.parseInt(entry.trim()));
                }
                data.add(lineData);
            }
            return data;
        }
        catch (IOException e) {
            System.out.println("Failed to read your file!");
            System.exit(0);
        }
        return null;
    }

    public static void main(String[] args) {
        // Get the input file from the program arguments
        Path inputFilePath = Paths.get(args[0]);

        ArrayList<ArrayList<Integer>> data = load(inputFilePath);

        // Find the maximum value in data and print it out
        int max = 0;
        for (ArrayList<Integer> inner : data) {
            for (Integer item : inner) {
                if (item > max) {
                    max = item;
                }
            }
        }
        System.out.println("Max: " + max);
    }
}
