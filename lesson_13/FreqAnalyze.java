import java.util.HashMap;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.Scanner;
import java.io.IOException;

public class FreqAnalyze {
    public static String load(Path filePath) {
        StringBuilder data = new StringBuilder();
        try {
            Scanner fileScanner = new Scanner(filePath.toFile());
            while (fileScanner.hasNextLine()) {
                data.append(fileScanner.nextLine());
            }
        }
        catch (IOException e) {
            System.out.println("Failed to read your file!");
            System.exit(0);
        }
        return data.toString();
    }

    public static void main(String[] args) {
        String text = load(Paths.get(args[0]));

        HashMap<Character, Integer> map = new HashMap<>();
        for (int index = 0; index < text.length(); index++) {
            char key = text.charAt(index);
            Integer count = map.get(key);
            if (count == null) {
                map.put(key, 1);
            }
            else {
                map.put(key, count + 1);
            }
        }

        for (Character key : map.keySet()) {
            System.out.println(key + ": " + map.get(key));
        }
    }
}
