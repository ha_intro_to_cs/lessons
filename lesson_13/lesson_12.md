# Lesson 12: Common Data Structures

We have learned about the primitive data types as well as arrays. Arrays are great, but they have some severe limitations, and definitely don't cover all cases where we want to collect data together.

In this lesson, we'll look at some classes that Java provides to manage our data better.

## List

One of the greatest weaknesses of an array is that the size is fixed. If you don't know the maximum number of items you need to hold, it will be difficult to use an array.

A `List` is an abstract idea of an ordered collection of 0 or more values, all having the same type. The `List` class is actually an a special type of class called an **interface**, which means that it specifies methods which must exist, but it does not implement them. Java offers `ArrayList` and `LinkedList` implementations, and anyone can make their own. It is very powerful to be able to work with the abstract idea of a list while choosing specific implementations.

The difference between `ArrayList` and `LinkedList` is important, but beyond the scope of this introduction. For now, we will just use `ArrayList`.

### Exercise 0

~~~java
import java.util.List;
import java.util.ArrayList;

public class Ex0 {
    public static void main(String[] args) {
        List<Integer> example = new ArrayList<>();
        for (int count = 0; count < 15; count++) {
            example.add(100 + count);
        }

        System.out.println("Length: " + example.size());
        for (Integer value : example) {
            System.out.print(value + ",");
        }
        System.out.println();

        example.add(10, 200);
        System.out.println("Length: " + example.size());
        for (Integer value : example) {
            System.out.print(value + ",");
        }
        System.out.println();

        Integer removed = example.remove(12);
        System.out.println("Removed index 12, value " + removed);
        System.out.println("Length: " + example.size());
        for (Integer value : example) {
            System.out.print(value + ",");
        }
        System.out.println();

        System.out.println("Contains a 155? " + example.contains(155));
        System.out.println("Contains a 104? " + example.contains(104));
        System.out.println("Index of 104: " + example.indexOf(104));
    }
}
~~~

Output
~~~
> java Ex0
Length: 15
100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,
Length: 16
100,101,102,103,104,105,106,107,108,109,200,110,111,112,113,114,
Removed index 12, value 111
Length: 15
100,101,102,103,104,105,106,107,108,109,200,110,112,113,114,
Contains a 155? false
Contains a 104? true
Index of 104: 4
~~~

This should generally make sense, but we'll go through a few points.

* We declare the type to be `List`, even though we use the `ArrayList` constructor. This is more flexible and most of the time works out, if you really need something special that only `ArrayList` has, then you should use that for the type, but I can't imagine you should need that in this class.

* `List` is known as a **template** type, meaning we have to specify some types of data to work with. We use the angle brackets `<, >` to do this. A template type must be a class, it cannot be a primitive, which means we use `Integer` and not `int`. You can repeat this in the constructor, but it has to match the declaration. You can shortcut with `<>` and Java will figure it out for you.

This means we can have a `List` of `Integer`s, or a `List` of `Double`s, or even a `List` of `List`s, or custom classes.

* You can see that we can shove as many elements into the list as we like by using `add`. The list will grow to accept whatever we put in. The actual maximum number of elements is the max value of an `int`, or 2,147,483,647.

* You can also insert an element into the middle of the list by specifying the index. You can see that the 200 gets placed at index 10 and everything shifts to the right.

* Elements can be removed by index. `remove` returns the element that was removed.

* Lists are searchable, you can check if it contains a given object, and find where the object is in the list.

For a complete reference, see https://docs.oracle.com/javase/8/docs/api/java/util/List.html To effectively use these collections, you will need to become good at reading this documentation.

## Map

`List`s work if we care about keeping our data in a certain order, but that is not always true. Maps make it easier to reason about an orderless collection, and it is almost always faster than searching the list over and over again trying look things up.

A `Map` "maps" keys to values. This works a lot like a function.

***graphic showing mapping***

Just like `List`, `Map` is an interface with several implementations to choose from. For most purposes, `HashMap` is good enough.

### Exercise 1

~~~java
import java.util.Map;
import java.util.HashMap;

public class Ex1 {
    public static void main(String[] args) {
        Map<String, String> capitals = new HashMap<>();
        capitals.put("md", "Annapolis");
        capitals.put("va", "Richmond");
        capitals.put("pa", "Harrisburg");
        capitals.put("de", "Dover");
        capitals.put("wv", "Charlston");
        System.out.println("States tracked: " + capitals.size());

        System.out.println("Maryland: " + capitals.get("md"));
        System.out.println("Arkansas: " + capitals.get("ak"));

        System.out.println("We moved the capital of Maryland!");
        capitals.put("md", "Columbia");
        System.out.println("Maryland: " + capitals.get("md"));

        System.out.println("Delaware fell into the ocean!");
        capitals.remove("de");
        System.out.println("States remaining: " + capitals.size());
        for (String state : capitals.keySet()) {
            System.out.println(state + ", " + capitals.get(state));
        }
    }
}
~~~

Output

~~~
> java Ex1
States tracked: 5
Maryland: Annapolis
Arkansas: null
We moved the capital of Maryland!
Maryland: Columbia
Delaware fell into the ocean!
States remaining: 4
pa, Harrisburg
md, Columbia
va, Richmond
wv, Charlston
~~~

* `Map` is also a template class, but we have two types, the key type and the value type, they are allowed to be different.

* `Map` dynamically resizes just like `List`, we can put as many elements as we want in.

* To access a value by it's key, use `get`.

This relationship is one way. If you have the value but need to find the key for some reason, you will have to search through every element in the map.

* `put` also updates entries if they already exist. This makes it extremely versatile and limits the need for error checking.

* To remove objects, use `remove` with the key.

* You *can* iterate over all entries in a `Map`, but they won't be in a particular order, see how it's different from insertion order.

Full documentation: https://docs.oracle.com/javase/8/docs/api/java/util/Map.html
