public class Ex2 {
    public static void main(String[] args) {
        System.out.println("Let's do some weird math");
        System.out.println(5 * 7);
        System.out.println(5.0 * 7.0);
        System.out.println(5 / 7);
        System.out.println(5.0 / 7.0);
        System.out.println(5 % 7);
        System.out.println(21 % 7);
        System.out.println(22 % 7);
    }
}
