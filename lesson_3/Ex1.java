public class Ex1 {
    public static void main(String[] args) {
        System.out.println("Let's do some more complex math");
        System.out.println(5 + 2 * 10);
        System.out.println(5 + (2 * 10));
        System.out.println((5 + 2) * 10);
        System.out.println(12 / 6 * 3 / 2);
    }
}
