# Lesson 3: Numbers and Math

Computers are really good at math. Absurdly good. Billions of operations per second good. We're going to learn how to leverage some of this power.

You may remember there was one type of token that we did not cover last lesson, operators. These let you perform mathematical operations. In this lesson, we will discuss a small number of operators, we will learn more later.

| Symbol |          Name         |
|:------:|:---------------------:|
|    +   |          plus         |
|    -   |         minus         |
|    *   |        asterisk       |
|    /   |     forward slash     |
|    %   |        percent        |

What do all of these operators do? Some are obvious, others less so. Let's try an example.

## Exercise 0

Make a new directory called lesson_3, and within that create a new directory ex0. You should get used to this, we'll be doing it a lot.

Use the following source to create Ex0.java.

Ex0.java
~~~java
public class Ex0 {
    public static void main(String[] args) {
        System.out.println("Let's do some math");
        System.out.println(2);
        System.out.println(2 + 2);
        System.out.println(5 - 3);
        System.out.println(8 * 4);
        System.out.println(100 / 10);
    }
}
~~~

Before you compile and run this, add a comment to each print statement explaining what you expect to be printed.

***Cover comments before this point, maybe in assignment?***

Compile and run your program. Here's what you should see.

~~~
> java Ex0
Let's do some math
2
4
2
32
10
~~~

This example reveals what the first four operators do.

***fill in table with answers?***

In our hello world program, we saw a literal string. Here we see some literal numbers, specifically integers. Literal integers can be combined with operators to make the computer perform math operations.

Let's continue to a more complex exercise to uncover more interesting behavior.

## Exercise 1

Same story here, take a guess at what it should print before you run it.

Ex1.java
~~~java
public class Ex1 {
    public static void main(String[] args) {
        System.out.println("Let's do some more complex math");
        System.out.println(5 + 2 * 10);
        System.out.println(5 + (2 * 10));
        System.out.println((5 + 2) * 10);
        System.out.println(12 / 6 * 3 / 2);
    }
}
~~~

~~~
> java Ex1
Let's do some more complex math
25
25
70
3
~~~

You may remember PEMDAS from math class, Java follows the same rules. If the answers here don't line up, search for a refresher on order of operations.

This introduces us to the concept of an **expression**. The difference between expressions and statements can be tricky, so let's take our time here.

An expression is a piece of code that can be evaluated to a single value.

A statement is an instruction telling the computer to do something.

If we inserted `(5 + 2) * 10;` on line three, this doesn't tell the computer to do anything. We might as well just write `70;`. It's not a statement because we don't tell the computer to _do_ anything with this value.

Java helpfully points this out if you try to compile that, by the way.

~~~
> javac Ex1.java 
Ex1.java:3: error: not a statement
        (5 + 2) * 10;
                ^
1 error
~~~

When we say `System.out.println((5 + 2) * 10);`, we're telling the computer to print the value of the expression to the output, so that is a statement.

You will become comfortable with this difference shortly.

## Exercise 2

There's one more tricky piece with these math operations - division. Let's start with some code like the last two exercises, but I'm going to warn you that this will be much harder to correctly predict what will happen.

Ex2.java
~~~java
public class Ex2 {
    public static void main(String[] args) {
        System.out.println("Let's do some weird math");
        System.out.println(5 * 7);
        System.out.println(5.0 * 7.0);
        System.out.println(5 / 7);
        System.out.println(5.0 / 7.0);
        System.out.println(5 % 7);
        System.out.println(21 % 7);
        System.out.println(22 % 7);
    }
}
~~~

Before reading on, look at the output and see if you can figure out what's happening.

~~~
> java Ex2
Let's do some weird math
35
35.0
0
0.7142857142857143
5
0
1
~~~

The first real headscratcher should be `5 / 7 = 0`. What in the world is happening here?

Computers handle numbers differently than we do. They have separate ways of handles integers and decimal numbers. We will cover this in depth in the next lesson. For now, all you need to know is that integers stay integers. When you ask for `5 / 7`, you're really asking "how many integer times does five go into seven?" The answer is 0 if we're stuck with integers.

When we ask for `5.0 / 7.0`, we're telling the computer to handle the numbers like decimals, so we are allowed to get a decimal answer. That's also why `5.0 * 7.0 = 35.0`. We started with decimals, so we end with decimals.

The next mystery is the percent operator. I'm willing to bet that you haven't seen this used this way, normally `%` means "divided by 100", but here we're using like multiplication or division with two parameters.

This operator is called the **modulo** operator. This operator finds the *modulus*, or remainder of two integers. Five goes into seven 0 times with five remaining. You may remember answering division questions this way from elementary school. "Five divided by seven equals zero remainder 5".

"Twenty one divided by seven equals three remainder zero".

"Twenty two divided by seven equals three remainder one".

This completes the picture for integer division. If we didn't have the ability to handle decimal numbers (or shouldn't for some reason) then we need this tool for division with non-integer results. It's also handy in a few other scenarios.

## What's not here?

You may have noticed that there is no exponentiation operator. This is intentional. Early computers did not have assembly instructions for exponentiation, so it was left out of the built-in operators by most languages. Some newer languages offer `**`, but Java does not. We will learn how to handle this in Java later.
