<!DOCTYPE html>
<meta charset="UTF-8">
<html>
<head>
    <link href="../common/prism.css" rel="stylesheet" />
    <link rel="stylesheet" href="../common/styles.css">
</head>
<script src="../common/prism.js"></script>
<script src="../common/settings.js"></script>
<body>
<div class="centered" style="max-width: 55em;">

<h1>
    Lesson 3: Numbers and Math
</h1>

<p>
    Computers are really good at math. Absurdly good. Billions of operations per second good. We're going to learn how to leverage some of this power.
</p>

<p>
    You may remember there was one type of token that we did not cover last lesson, operators. These let you perform mathematical operations. In this lesson, we will discuss a small number of operators, we will learn more later.
</p>

<table>
    <thead>
    <tr>
        <th style="text-align:center">Symbol</th>
        <th style="text-align:center">Name</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="text-align:center">+</td>
        <td style="text-align:center">plus</td>
    </tr>
    <tr>
        <td style="text-align:center">-</td>
        <td style="text-align:center">minus</td>
    </tr>
    <tr>
        <td style="text-align:center">*</td>
        <td style="text-align:center">asterisk</td>
    </tr>
    <tr>
        <td style="text-align:center">/</td>
        <td style="text-align:center">forward slash</td>
    </tr>
    <tr>
        <td style="text-align:center">%</td>
        <td style="text-align:center">percent</td>
    </tr>
    </tbody>
</table>

<p>
    What do all of these operators do? Some are obvious, others less so. Let's try an example.
</p>

<h2>
    Exercise 0
</h2>

<p>
    Make a new directory called lesson_3, and within that create a new directory ex0. You should get used to this, we'll be doing it a lot.
</p>

<p>
    Use the following source to create Ex0.java.
</p>

<pre class="language-java line-numbers"><code>
public class Ex0 {
    public static void main(String[] args) {
        System.out.println("Let's do some math");
        System.out.println(2);
        System.out.println(2 + 2);
        System.out.println(5 - 3);
        System.out.println(8 * 4);
        System.out.println(100 / 10);
    }
}

</code></pre>

<p>
    Before you compile and run this, add a comment to each print statement explaining what you expect to be printed.
</p>

<p>
    Compile and run your program. Here's what you should see.
</p>

<pre class="lang-xxx"><code>
> java Ex0
Let's do some math
2
4
2
32
10
</code></pre>

<p>
    This example reveals what the first four operators do.
</p>

<table>
    <thead>
    <tr>
        <th style="text-align:center">Symbol</th>
        <th style="text-align:center">Name</th>
        <th style="text-align:center">Operation</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="text-align:center">+</td>
        <td style="text-align:center">plus</td>
        <td style="text-align:center">addition</td>
    </tr>
    <tr>
        <td style="text-align:center">-</td>
        <td style="text-align:center">minus</td>
        <td style="text-align:center">subtraction</td>
    </tr>
    <tr>
        <td style="text-align:center">*</td>
        <td style="text-align:center">asterisk</td>
        <td style="text-align:center">multiplication</td>
    </tr>
    <tr>
        <td style="text-align:center">/</td>
        <td style="text-align:center">forward slash</td>
        <td style="text-align:center">division</td>
    </tr>
    <tr>
        <td style="text-align:center">%</td>
        <td style="text-align:center">percent</td>
        <td style="text-align:center"></td>
    </tr>
    </tbody>
</table>

<p>
    In our hello world program, we saw a literal string. Here we see some literal numbers, specifically integers. Literal integers can be combined with operators to make the computer perform math operations.
</p>

<p>
    Let's continue to a more complex exercise to uncover more interesting behavior.
</p>

<h2>
    Exercise 1
</h2>

<p>
    Same story here, take a guess at what it should print before you run it.
</p>

<pre class="language-java line-numbers"><code>
public class Ex1 {
    public static void main(String[] args) {
        System.out.println("Let's do some more complex math");
        System.out.println(5 + 2 * 10);
        System.out.println(5 + (2 * 10));
        System.out.println((5 + 2) * 10);
        System.out.println(12 / 6 * 3 / 2);
    }
}

</code></pre>

<pre class="lang-xxx"><code>
> java Ex1
Let's do some more complex math
25
25
70
3
</code></pre>

<p>
    You may remember PEMDAS from math class, Java follows the same rules. If the answers here don't line up, search for a refresher on order of operations.
</p>

<p>
    This introduces us to the concept of an <strong>expression</strong>. The difference between expressions and statements can be tricky, so let's take our time here.
</p>

<div class="definition">
    <span class="definition-h">Definition</span>
    <p>
        An <strong>expression</strong> is a piece of code that can be evaluated to a single value.
    </p>
</div>

<p>
    Remember that a statement is a command telling the computer to do something.
</p>

<p>
    If we inserted <code>(5 + 2) * 10;</code> on line three, this doesn't tell the computer to do anything. We might as well just write <code>70;</code>. It's not a statement because we don't tell the computer to <em>do</em> anything with this value.
</p>

<p>
    Java helpfully points this out if you try to compile that, by the way.
</p>

<pre class="lang-xxx"><code>
> javac Ex1.java 
Ex1.java:3: error: not a statement
        (5 + 2) * 10;
                ^
1 error
</code></pre>

<p>
    When we say <code class="language-java">System.out.println((5 + 2) * 10);</code>, we're telling the computer to print the value of the expression to the output, so that is a statement.
</p>

<p>
    You will become comfortable with this difference shortly.
</p>

<h2>
    Exercise 2
</h2>

<p>
    There's one more tricky piece with these math operations - division. Let's start with some code like the last two exercises, but I'm going to warn you that this will be much harder to correctly predict what will happen.
</p>

<pre class="language-java line-numbers"><code>
public class Ex2 {
    public static void main(String[] args) {
        System.out.println("Let's do some weird math");
        System.out.println(5 * 7);
        System.out.println(5.0 * 7.0);
        System.out.println(5 / 7);
        System.out.println(5.0 / 7.0);
        System.out.println(5 % 7);
        System.out.println(21 % 7);
        System.out.println(22 % 7);
    }
}

</code></pre>

<p>
    Before reading on, look at the output and see if you can figure out what's happening.
</p>

<pre class="lang-xxx"><code>
> java Ex2
Let's do some weird math
35
35.0
0
0.7142857142857143
5
0
1
</code></pre>

<p>
    The first real headscratcher should be <code>5 / 7 = 0</code>. What in the world is happening here?
</p>

<p>
    Computers handle numbers differently than we do. They have separate ways of handling integers and decimal numbers. We will cover this in depth in the next lesson. For now, all you need to know is that integers stay integers. When you ask for <code>5 / 7</code>, you're really asking "how many integer times does five go into seven?" The answer is 0 if we're stuck with integers.
</p>

<p>
    When we ask for <code>5.0 / 7.0</code>, we're telling the computer to handle the numbers like decimals, so we are allowed to get a decimal answer. That's also why <code>5.0 * 7.0 = 35.0</code>. We started with decimals, so we end with decimals.
</p>

<p>
    The next mystery is the percent operator. I'm willing to bet that you haven't seen this used this way, normally <code>%</code> means "divided by 100", but here we're using like multiplication or division with two parameters.
</p>

<p>
    This operator is called the <strong>modulo</strong> operator. This operator finds the <em>modulus</em>, or remainder of two integers. Five goes into seven 0 times with five remaining. You may remember answering division questions this way from elementary school. "Five divided by seven equals zero remainder 5".
</p>

<p>
    "Twenty one divided by seven equals three remainder zero".
</p>

<p>
    "Twenty two divided by seven equals three remainder one".
</p>

<p>
    This completes the picture for integer division. If we didn't have the ability to handle decimal numbers (or shouldn't for some reason) then we need this tool for division with non-integer results. It's also handy in a few other scenarios.
</p>

<h2>
    What's not here?
</h2>

<p>
    You may have noticed that there is no exponentiation operator. This is intentional. Early computers did not have assembly instructions for exponentiation, so it was left out of the built-in operators by most languages. Some newer languages offer <code>**</code>, but Java does not. We will learn how to handle this in Java later.
</p>

</div>
</body>
</html>
