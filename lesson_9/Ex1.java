public class Ex1 {
    static void primitiveSetFive(int input) {
        input = 5;
    }

    static void objectSetFive(Integer input) {
        input = 5;
    }

    static void objectReinitSetFive(Integer input) {
        input = new Integer(5);
    }

    public static void main(String[] args) {
        int primitive = 10;
        System.out.println("---Primitive---");
        System.out.println(primitive);

        primitiveSetFive(primitive);
        System.out.println(primitive);

        Integer object = new Integer(10);
        System.out.println("---Object---");
        System.out.println(object);
        objectSetFive(object);
        System.out.println(object);

        Integer objectReinit = new Integer(10);
        System.out.println("---Object Reinitialize---");
        System.out.println(objectReinit);
        objectReinitSetFive(objectReinit);
        System.out.println(objectReinit);
    }
}
