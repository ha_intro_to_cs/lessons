import java.util.Scanner;

public class Ex1a {
    public static int getCheckedInt() {
        int userInteger;
        Scanner inputScanner = new Scanner(System.in);
        while (true) {
            System.out.print("Enter an integer: ");
            String userInput = inputScanner.nextLine();
            try {
                userInteger = Integer.parseInt(userInput);
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid integer: " + userInput);
                continue;
            }
            break;
        }

        return userInteger;
    }

    public static void printCharacter(int magic, int physical, int hp) {
        System.out.println();
        System.out.println("--Character--");
        System.out.println("Magic: " + magic);
        System.out.println("Physical: " + physical);
        System.out.println("HP: " + hp);
    }

    public static void main(String[] args) {
        System.out.println("Magic Power:");
        int magicPower = getCheckedInt();

        System.out.println();
        System.out.println("Physical Power:");
        int physicalPower = getCheckedInt();

        System.out.println();
        System.out.println("HP:");
        int hp = getCheckedInt();

        printCharacter(magicPower, physicalPower, hp);
    }
}
