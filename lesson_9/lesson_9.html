<!DOCTYPE html>
<meta charset="UTF-8">
<html>
<head>
    <link href="../common/prism.css" rel="stylesheet" />
    <link rel="stylesheet" href="../common/styles.css">
</head>
<script src="../common/prism.js"></script>
<script src="../common/settings.js"></script>
<body>
<div class="centered" style="max-width: 55em;">

<h1>
    Lesson 9: Functions
</h1>

<p>
    We've been skirting around the idea of functions for too long. It's time we cover exactly what they are and how they work.
</p>

<p>
    A function is a reusable set of computer instructions. This is remarkably similar to our definition of a program. In reality, how they are used is the only difference. When we write our program, we are really writing a function called <code>main</code>. We only treat this differently because users see the entire program as one unit, whereas a single program likely uses many different functions.
</p>

<p>
    <strong>Calling</strong> other people's functions is not hard. <code>println</code> is a function that we have been using a lot. It is a set of instructions that sends some text to the standard output. You can usually find information about a function online or through documentation tools.
</p>

<div class="definition">
    <span class="definition-h">Definition</span>
    <p>
        <strong>Calling</strong> a function just means running the function.
    </p>
</div>


<p>
    The important thing I want you to learn is how to make your own functions. To do this, we need to understand all the pieces of a function. Let's break down the syntax for declaring a function. To do that, let's look at this example function.
</p>

<pre class="language-java"><code>
    double exp(double base, int exponent) {
        statements;
        return 0.0;
    }
</code></pre>

<p>
    The first token is an identifier, this is the type of data that the function returns. In the context of a shell program, you can think of the output as the returned data. In general functions can return any type of data, not just text. You can also use <code>void</code> if the function doesn't need to return anything, which you will find lots of uses for. In Java, functions can have at most one return value. Other programming languages let you have as many as you like. We will learn to solve this problem later.
</p>

<p>
    Next is another identifier, which is the name of the function. Functions must be named in order to refer to them later. Just like we have to give our program a name, users couldn't run it without being able to refer to it.
</p>

<p>
    Then we place the arguments to the function in the parenthesis. You can specify zero or more arguments, but it's going to be hard to manage more than a few. Each argument must have a name and a type, separated by a comma. Here we have two arguments, <code>base</code> and <code>exponent</code>.
</p>

<p>
    Next we enclose statements in braces. You can have zero or more statements. This defines the scope of the function.
</p>

<p>
    Lastly, we return some value using the <code>return</code> keyword. If the return type is <code>void</code>, you can use <code>return</code> by itself or omit it entirely.
</p>

<pre class="language-java"><code>
void update() {
    statements;
    return;
}
</code></pre>

<pre class="language-java"><code>
void update() {
    statements;
}
</code></pre>

<p>
    Having covered all this, let's look at a full function definition.
</p>

<pre class="language-java"><code>
double exp(double base, int exponent) {
    double result = 1;

    for (int count = 0; count < exponent; count++) {
        result = result * base;
    }

    return result;
}
</code></pre>

<p>
    Can you guess what this function does? Stop reading right here and try to reason it out. If you can't get it, I'll tell you the answer in the exercise.
</p>

<h3>
    Exercise 0
</h3>

<p>
    There's a few more pieces of context that we need to actually put our <code>exp</code> function into our program.
</p>

<pre class="language-java line-numbers"><code>
    public class Ex0 {
        static double exp(double base, int exponent) {
            double result = 1;
            for (int count = 0; count < exponent; count++) {
                result = result * base;
            }

            return result;
        }

        public static void main(String[] args) {
            System.out.println(
                exp(Double.parseDouble(args[0]), Integer.parseInt(args[1]))
            );
        }
    }

</code></pre>

<p>
    What's different here? The <code>static</code> keyword is required because it is called by <code>main</code>, which is static. Static is like a disease, it infects everything it touches. Remember that static means that you don't have to have an instance of the class to use the function, so static class functions cannot depend on non-static class functions.
</p>

<p>
    We could use <code>public</code> or <code>private</code> if we wanted to, but it doesn't matter since <code>Ex0</code> is the only class. If we don't specify, the default is "package-private", which is a bit weird, we'll get into it later, it doesn't matter for now.
</p>

<p>
    If you haven't figured it out, this function evaluates an exponent. I created a super quick <code>main</code> wrapper to test our function. See if you can be even lazier than me, it might be possible.
</p>

<p>
    You should get output like this if you copied it correctly.
</p>

<pre class="lang-xxx"><code>
    > java Ex0 2 4
    16.0
    > java Ex0 2 3
    8.0
    > java Ex0 2 2
    4.0
    > java Ex0 2 1
    2.0
    > java Ex0 2 0
    1.0
</code></pre>

<h2>
    Scope
</h2>

<p>
    Scope applies here just like it did when we learned about conditional blocks. We created the double variable "result" inside the function "exp". If we try to use it in any other scope, we will get an error. Watch out for this!
</p>

<h2>
    Evaluating Functions
</h2>

<p>
    When Java evaluates an expression containing a function, the function falls into the "parenthesis" step of the order of operations. This means they're handled first. Therefore the following is valid code.
</p>

<pre class="language-java"><code>
    double result = exp(7, 3) + exp(3.14, 2);
</code></pre>

<p>
    This means "compute the result of the function <code>exp(7, 3)</code>, compute the result of the function <code>exp(3.14, 2)</code>, then add the two together."
</p>

<h2>
    More Practice
</h2>

<p>
    Let's do another exercise to make sure that we understand how functions work.
</p>

<h3>
    Exercise 1
</h3>

<pre class="language-java line-numbers"><code>
    import java.util.Scanner;

    public class Ex1 {
        public static int getCheckedInt() {
            int userInteger;
            Scanner inputScanner = new Scanner(System.in);
            while (true) {
                System.out.print("Enter an integer: ");
                String userInput = inputScanner.nextLine();
                try {
                    userInteger = Integer.parseInt(userInput);
                }
                catch (NumberFormatException e) {
                    System.out.println("Invalid integer: " + userInput);
                    continue;
                }
                break;
            }

            return userInteger;
        }

        public static void printCharacter(int magic, int physical, int hp) {
            System.out.println();
            System.out.println("--Character--");
            System.out.println("Magic: " + magic);
            System.out.println("Physical: " + physical);
            System.out.println("HP: " + hp);
        }

        public static void main(String[] args) {
            System.out.println("Magic Power:");
            int magicPower = getCheckedInt();

            System.out.println();
            System.out.println("Physical Power:");
            int physicalPower = getCheckedInt();

            System.out.println();
            System.out.println("HP:");
            int hp = getCheckedInt();

            printCharacter(magicPower, physicalPower, hp);
        }
    }

</code></pre>

<p>
    In this exercise we create a function called <code>getCheckedInt</code> that uses a loop to make sure that the user inputs an integer properly. There is a lot of code here to do that, we have the loop, getting the input, converting the input, and catching exceptions. If we put it into a function, we can easily use it over and over.
</p>

<p>
    Also note that this function has no arguments. That is allowed! Here it makes sense because our input is coming from the standard input, we don't have anything from <code>main</code> that can help. It still returns a value just the same though.
</p>

<p>
    The other function we made is <code>printCharacter</code>. This function has a <code>void</code> return type, it doesn't return anything. It doesn't need to. It's only job is print out the character.
</p>

<p>
    It has three arguments, <code>magic</code>, <code>physical</code>, <code>hp</code>. When we call the function, we're using different names. This is okay, the names on line 23 are the variables that will be used <em>inside</em> the function. Outside the function you can name things whatever you want. You can even use the same name like we did for <code>hp</code>. This is allowed because the contents of <code>main</code> and <code>printCharacter</code> don't overlap at all. In past when we've looked at <code>if</code> or <code>try</code> blocks, we've found that we can't reuse the same name, but the scope of the <code>if</code> is inside of <code>main</code> which is why it's disallowed.
</p>

<p>
    This might be confusing. If it is, don't worry! You can always be absolutely clear by using different names for everything. Ask for help if it doesn't make sense!
</p>

<h2>
    Don't Repeat Yourself (DRY)
</h2>

<p>
    Any time you find yourself writing code that you already wrote, it's probably a good idea to turn it into a function. If if it's not a lot of code, it's still a good idea to do it in case you decide to change how it works, or you find a bug. If it's in a function, you only have one thing to change. Without a function, you'll have to find everywhere you copy / pasted and fix it in each place.
</p>

<p>
    This is known as the "Don't Repeat Yourself" (DRY) principle.
</p>

</div>
</body>
</html>
