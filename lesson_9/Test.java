public class Test {
    private static class Container {
        private int data;
        public Container(int data) {
            this.data = data;
        }

        public int getData() {
            return data;
        }

        public void setData(int data) {
            this.data = data;
        }
    }

    private static void setFive(Container container) {
        container.setData(5);
    }

    public static void main(String[] args) {
        Container test = new Container(10);
        System.out.println(test.getData());
        setFive(test);
        System.out.println(test.getData());
    }
}