# Lesson 9: Functions

We've been skirting around the idea of functions for too long. It's time we cover exactly what they are and how they work.

A function is a reusable set of computer instructions. This is remarkably similar to our definition of a program. In reality, how they are used is the only difference. When we write our program, we are really writing a function called `main`. We only treat this differently because users see the entire program as one unit, whereas a single program likely uses many different functions.

Calling other people's functions is not hard. `println` is a function that we have been using a lot. It is a set of instructions that sends some text to the standard output. You can usually find information about a function online or through documentation tools.

***terminology: calling?***

The important thing I want you to learn is how to make your own functions. To do this, we need to understand all the pieces of a function. Let's break down the syntax for declaring a function.

~~~java
double exp(double base, int exponent) {
    statements;
    return 0.0;
}
~~~

The first token is an identifier, this is the type of data that the function returns. In the context of a shell program, you can think of the output as the returned data. In general functions can return any type of data, not just text. You can also use `void` if the function doesn't need to return anything, which you will find lots of uses for. In Java, functions can have at most one return value. Other programming languages let you have as many as you like. We will learn to solve this problem later.

Next is another identifier, which is the name of the function. Functions must be named in order to refer to them later. Just like we have to give our program a name, users couldn't run it without being able to refer to it.

Then we place the arguments to the function in the parenthesis. You can specify zero or more arguments, but it's going to be hard to manage more than a few. Each argument must have a name and a type, separated by a comma. Here we have two arguments, `base` and `exponent`.

Next we enclose statements in braces. You can have zero or more statements.

Lastly, we return some value using the `return` keyword. If the return type is `void`, you can use `return` by itself or omit entirely.

~~~java
void update() {
    statements;
    return;
}
~~~

~~~java
void update() {
    statements;
}
~~~

Having covered all this, let's look at a full function definition.

~~~java
double exp(double base, int exponent) {
    double result = 1;

    for (int count = 0; count < exponent; count++) {
        result = result * base;
    }

    return result;
}
~~~

Can you guess what this function does? Stop reading right here and try to reason it out. If you can't get it, I'll tell you the answer in the exercise.

## Exercise 0

There's a few more pieces of context that we need to actually put our `exp` function into our program.

~~~java
public class Ex0 {
    static double exp(double base, int exponent) {
        double result = 1;
        for (int count = 0; count < exponent; count++) {
            result = result * base;
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(
            exp(Double.parseDouble(args[0]), Integer.parseInt(args[1]))
        );
    }
}
~~~

What's different here? The `static` keyword is required because it is called by `main`, which is static. Static is like a disease, it infects everything it touches. Remember that static means that you don't have to have an instance of the class to use the function, so static class functions cannot depend on non-static class functions.

We could use `public` or `private` if we wanted to, but it doesn't matter since `Ex0` is the only class. If we don't specify, the default is "package-private", which is a bit weird, we'll get into it later, it doesn't matter for now.

If you haven't figured it out, this function evaluates an exponent. I created a super quick `main` wrapper to test our function. See if you can be even lazier than me, it might be possible.

You should get output like this if you copied it correctly.

~~~
> java Ex0 2 4
16.0
> java Ex0 2 3
8.0
> java Ex0 2 2
4.0
> java Ex0 2 1
2.0
> java Ex0 2 0
1.0
~~~

## References

We've skipped a sneaky issue with our presentation here by using primitive types. Remember when we discussed variables, we learned that creating a variable tells Java to reserve some memory for us, and the name is just for us to refer to it. In most programs, including Java, program memory is divided into two types, the stack and the heap.

The stack is for data with a defined lifetime. Variables with primitive types live here. The stack expands each time we enter a new scope, like a function call. When we exit the scope, all the memory in that scope is freed.

***diagram of stack***

The heap is for data without a defined lifetime. Java tracks objects here and every so often stops everything to see if there are any references on the stack. If there are not, the memory is freed. This process is called **garbage collection**.

When we create an Object, the memory goes on the heap,but we get a **pointer** to that memory on the stack, which is what our variable actually gives us.

When we pass a variable to a function, we are **copying** the data that is on the stack. For this reason, we say that Java is "pass by value". Let's examine what that means with an exercise.

## Exercise 1

~~~java
public class Ex1 {
    static void primitiveSetFive(int input) {
        input = 5;
    }

    static void objectSetFive(Integer input) {
        input = new Integer(5);
    }

    public static void main(String[] args) {
        int primitive = 10;
        System.out.println("---Primitive---");
        System.out.println(primitive);

        primitiveSetFive(primitive);
        System.out.println(primitive);

        Integer object = new Integer(10);
        System.out.println("---Object---");
        System.out.println(object);
        objectSetFive(object);
        System.out.println(object);
    }
}
~~~

If you run this program, you should see the following output.

~~~
> java Ex1
---Primitive---
10
10
---Object---
10
10
~~~

Neither of these functions modify the value passed to them.

In `primitiveSetFive`, this should make sense because primitives are completely on the stack, when we copy the value into the function, the original value isn't changing.

`objectSetFive` may be surprising, but remember that we are copying the pointer. When we change the pointer in the function, the original pointer still exists.

***diagram, 1) original pointer, 2) copy pointer, 3) change copy, original still exists***

In order to actually change the data that an object contains, you need to use functions the object gives you access to. Many objects, like `Integer` and `String` do not give programmers any way to modify the data, the only thing you can do is reassign. You can use a function to compute the new value, but you have to return the value and reassign like so.

~~~java
static Integer objectSetFive() {
    return new Integer(5);
}

public static void main(String[] args) {
    Integer object = new Integer(10);
    System.out.println(object);
    object = objectSetFive();
    System.out.println(object);
}
~~~

When you reassign this way, the old value of 10 is still in memory, it's not until the garbage collector runs that it gets freed.

We will revisit this topic when we cover more complex data types.
