# Lesson 0

Welcome to Computer Science! This is Lesson 0, which isn't really a lesson, but is a guide to setting up your computer for the first half of the course. You should do this before our first meeting so we can jump right into the fun stuff on day one.

The instructions here are for Windows computers. You should be free to use Linux or Mac if you desire, I don't think we'll run into problems, but I'm assuming that everyone will use Windows. If not, let me know and I'll help out.

## Sublime Text (Text Editor)

The first thing we need to install is Sublime Text. This will be our primary tool for writing code. Sublime is a nice cross-platform text editor, so you should be able to find it with all its features no matter which Operating System you use.

First, download the installer from the homepage: https://www.sublimetext.com/3.

Install using the default settings, or customize it if you feel comfortable doing so. You will likely need an Administrator password on your computer for this. Ask your parents for help if needed, but we will be installing more software later in the course, so make sure you have a good strategy to handle this.

Sublime is paid software, but it has an unlimited free trial. You can buy it if you really like it, but it's not necessary. It may occasionally give you pop-ups like this, they are safe to ignore.

***image sublime trial pop-up***

Sublime is frequently updated, you may also see pop-ups asking your to update, you can if you like, but there's no harm in refusing.

***image sublime update***

## JDK 1.8 (Programming Language)

The next thing to install in JDK (Java Development Kit) version 1.8. This is our programming language. 

For now, the best way to get the installer is through Google Classroom, under Material. Oracle, the company that primarily manages the Java language, makes you sign up for an account to download the files, and I don't want you to have to mess with that.

### Note

If you need this installer after losing access to this course, you can find it from Oracle here: https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html. Create an account and then you can download it. I don't recommend that you give them your real information, Oracle has no legitimate use for all of that personal info from you, you are allowed to make up a name, address, and phone number. You shouldn't give out information like that to any website unless it needs it to work properly, like Amazon needs your address to deliver you packages. Even if you trust the company (Oracle is largely trustworthy), they could get hacked, so protect your information by giving it to as few people as possible.

***installation instructions, with pictures***

## Windows Terminal (Terminal Emulator)

If you are on Windows, your time in this class will be made easier with Windows Terminal. Windows Terminal is a Terminal Emulator, we will learn about what that is in class. For now, just know that it is the primary tool we will be using to run our programs.

Download the installer from Microsoft here: https://github.com/microsoft/terminal/releases.

You may have to expand the "Assets" drop-down. Download the `msixbundle` file.

***installation instructions, with pictures***

## Conclusion

If you've done everything correctly, you should be able to open a Windows Terminal, type `java --version`, and then press Enter. You should see something like the following.

***image successful install***

If not, send me an email, we'll figure it out.

I'm excited to see you in class!
