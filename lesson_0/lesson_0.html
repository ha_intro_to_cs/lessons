<!DOCTYPE html>
<meta charset="UTF-8">
<html>
<head>
    <link href="../common/prism.css" rel="stylesheet" />
    <link rel="stylesheet" href="../common/styles.css">
</head>
<script src="../common/prism.js"></script>
<script src="../common/settings.js"></script>
<body>
<div class="centered" style="max-width: 55em;">

<h1>
    Lesson 0: Setting Up
</h1>

<p>
    Welcome to Computer Science! This is Lesson 0, which isn't really a lesson, but is a guide to setting up your computer for the first half of the course. You should do this before our first meeting so we can jump right into the fun stuff on day one.
</p>

<h2>
    Requirements
</h2>

<p>
    To complete this lesson, you just need your computer and an Internet connection. As noted in the syllabus, the course is designed around Windows 10, but I am happy to support Linux or Mac. This limitation is only present because I haven't found time to write literature for multiple OSes and I assume that most of you will use Windows 10. The vast majority of the course content is platform agnostic, this lesson is really the only one where it matters a lot.
</p>

<p>
    If you are using Linux or Mac, or need help for any other reason, I'm available even if the course hasn't officially started.
</p>

<h2>
    Sublime Text
</h2>

<p>
    The first thing we need to install is Sublime Text. This will be our primary tool for writing code. Sublime is a nice cross-platform text editor, so you should be able to find it with all its features no matter which Operating System you use.
</p>

<p>
    First, download the installer from the <a href=https://www.sublimetext.com/3>homepage</a>.
</p>

<p>
    Follow the installation wizard, pictures included below. It should be fine to keep the default install location if you don't care about this, but you want to make sure to check the "Add to explorer context menu" box. You will likely need an Administrator password on your computer for this. Ask your parents for help if needed, but we will be installing more software later in the course, so make sure you have a good strategy to handle this.
</p>

<div style="display: flex;">
    <img src="img/sublime_install_1.png" alt="Step 1" style="flex: 50%; padding: 5px;">
    <img src="img/sublime_install_2.png" alt="Step 2" style="flex: 50%; padding: 5px;">
</div>

<div style="display: flex;">
    <img src="img/sublime_install_3.png" alt="Step 3" style="flex: 50%; padding: 5px;">
    <img src="img/sublime_install_4.png" alt="Step 4" style="flex: 50%; padding: 5px;">
</div>
<div class="center-children"><span><em>Sublime Text Installation Steps</em></span></div>

<p>
    Sublime is paid software, but it has an unlimited free trial. You can buy it if you really like it, but it's not necessary. It may occasionally give you pop-ups asking you to purchase a license, they are safe to ignore.
</p>

<p>
    Sublime is frequently updated, you may also see pop-ups asking your to update, you can if you like, but there's no harm in refusing.
</p>

<p>
    Once Sublime is installed, you should open it and make sure it's been installed correctly. Open the program and create a new file. Save the file as <code>test.txt</code> with File > Save, or Control-S.
</p>

<div class="center-children">
    <img src="img/sublime_test.png" style="width: 75%;">
</div>

<p>
    Close Sublime. To re-open the file, right click on the file and choose "Open with Sublime Text".
</p>

<div class="center-children">
    <img src="img/sublime_open_with.png" style="width: 75%;">
</div>

<p>
    If you don't want to do this every time (you probably don't), you can tell Windows to always open <code>*.txt</code> files with Sublime. Right click on the file, choose Properties > "Change..." under "Open with" > More apps > Look for another app on this PC > Select Sublime Text. Click Apply, then close the Properties window.
</p>

<div style="display: flex;">
    <img src="img/sublime_default_1.png" style="height: 100%; flex: 33.33%; padding: 5px;">
    <img src="img/sublime_default_2.png" style="height: 100%; flex: 33.33%; padding: 5px;">
    <img src="img/sublime_default_3.png" style="height: 100%; flex: 33.33%; padding: 5px;">
</div>

<div style="display: flex;">
    <img src="img/sublime_default_4.png" style="flex: 50%; padding: 5px;">
    <img src="img/sublime_default_5.png" style="flex: 50%; padding: 5px;">
</div>
<div class="center-children"><span><em>Set Sublime Text as default program</em></span></div>

<p>
    It's likely that you'll want to do this for a few other file types we'll be working with, so remember this process.
</p>

<p>
    If Windows isn't showing you the file extension, you can change that in the View menu.
</p>

<div class="center-children">
    <img src="img/windows_show_file_extensions.png" style="width: 75%;">
</div>

<p>
    We'll be using Sublime a lot, you might want to add it to your Start menu, Desktop, or task bar, wherever you like to keep frequently used things.
</p>

<h2>
    Windows Terminal (Terminal Emulator)
</h2>

<p>
    If you are on Windows, your time in this class will be made easier with Windows Terminal. Windows Terminal is a Terminal Emulator, we will learn about what that is in class. For now, just know that it is the primary tool we will be using to run our programs.
</p>

<p>
    Download the installer from Microsoft <a href=https://github.com/microsoft/terminal/releases>here</a>. Look for the post that says "Latest release" in green on the right. You don't want a Pre-release, it may be unstable. Expand the "Assets" drop-down at the bottom of the release. Download the <code>msixbundle</code> file.
</p>

<p>
    This is what it looked like when I downloaded it.
</p>

<div style="display: flex;">
    <img src="img/windows_terminal_download_1.png" style="height: 100%; flex: 50%; padding: 5px;">
    <img src="img/windows_terminal_download_2.png" style="flex: 50%; padding: 5px;">
</div>

<p>
    Launch the installer and click "Install". It should be that easy!
</p>

<div class="center-children">
    <img src="img/windows_terminal_install_1.png" style="width: 75%;">
</div>

<p>
    After installation is complete, the terminal should automatically open. To check that it's working, type <code>ls</code> and press Enter. You should see something like what's pictured below.
</p>

<div style="display: flex;">
    <img src="img/windows_terminal_check_1.png" style="flex: 50%; padding: 5px;">
    <img src="img/windows_terminal_check_2.png" style="flex: 50%; padding: 5px;">
</div>

<p>
    Like Sublime, we will be using this a lot, so put it somewhere easy to get to.
</p>

<h2>
    JDK 1.8 (Programming Language)
</h2>

<p>
    The next thing to install in JDK (Java Development Kit) version 1.8. This is our programming language.
</p>

<p>
    For now, the best way to get the installer is through Google Classroom, under Material > <code>jdk-8u261-windows-x64</code>. Oracle, the company that primarily manages the Java language, makes you sign up for an account to download the files, and I don't want you to have to mess with that.
</p>

<div class="note">
    <span class="note-h">
        Note
    </span>
    <p>
        If you need this installer after losing access to this course, you can find it from Oracle <a href=https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html>here</a>. Create an account and then you can download it. I don't recommend that you give them your real information, Oracle has no legitimate use for all of that personal info, you are allowed to make up a name, address, and phone number. You shouldn't give out information like that to any website unless it needs it to work properly, like Amazon needs your address to deliver you packages. Even if you trust the company (Oracle is largely trustworthy), they could get hacked, so protect your information by giving it to as few people as possible.
    </p>
</div>

<p>
    Run the installer and accept all the default options. Check the included pictures if something doesn't feel right.
</p>

<div style="display: flex;">
    <img src="img/jdk_install_1.png" style="height:100%; flex: 50%; padding: 5px;">
    <img src="img/jdk_install_2.png" style="flex: 50%; padding: 5px;">
</div>

<div style="display: flex;">
    <img src="img/jdk_install_3.png" style="height:100%; flex: 50%; padding: 5px;">
    <img src="img/jdk_install_4.png" style="flex: 50%; padding: 5px;">
</div>
<div class="center-children"><span><em>JDK 1.8 Installation Steps</em></span></div>

<p>
    To check that it is installed correctly, open Windows Terminal, type <code>java -version</code>, and press Enter. Verify that your output looks the same as the picture.
<p>

<div class="center-children">
    <img src="img/jdk_check.png" style="width: 75%;">
</div>

<h3>
    Add Java to your PATH
</h3>

<p>
    Now that Java is installed, we need to tell Windows where we installed it so that we can use some of the tools that come with it. You might expect this to happen automatically, but almost everyone who installs Java doesn't need this, so it isn't automatically done.
</p>

<p>
    Follow along with the steps outlined below. Each image corresponds to one step.
</p>

<p>
    <ol>
        <li>Open the Start Menu, search for the Control Panel and open it.</li>
        <li>Click on User Accounts</li>
        <li>Click on User Accounts again on the new page</li>
        <li>Click Change my environment variables</li>
        <li>Select "Path" and click "Edit"</li>
        <li>Click "New"</li>
        <li>Confirm you Java install location. It's probably <code>C:\Program Files\Java\jdk1.8.0_261\bin</code>, but you should double check. Open Windows Explorer and find that folder and make sure it looks right.</li>
        <li>Copy and paste the path into the new line in environment variable window. You can probably just copy from the previous step, but if your Java was installed in a different place you can copy the path by clicking in the address bar of Windows Explorer and pressing Control-C.</li>
    </ol>
</p>

<div style="display: flex;">
    <img src="img/path_1.png" style="height:100%; flex: 50%; padding: 5px;">
    <img src="img/path_b2.png" style="flex: 50%; padding: 5px;">
</div>

<div style="display: flex;">
    <img src="img/path_b3.png" style="flex: 50%; padding: 5px;">
    <img src="img/path_b4.png" style="flex: 50%; padding: 5px;">
</div>

<div style="display: flex;">
    <img src="img/path_6.png" style="flex: 50%; padding: 5px;">
    <img src="img/path_7.png" style="flex: 50%; padding: 5px;">
</div>

<div style="display: flex;">
    <img src="img/path_6a.png" style="height: 100%; flex: 50%; padding: 5px;">
    <img src="img/path_8.png" style="flex: 50%; padding: 5px;">
</div>

<p>
    If you did everything right, you should be able to run <code> javac -version</code> and see the following output. (Make sure to type <code>javac</code> and not <code>java</code> like before!)
</p>

<div class="center-children">
    <img src="img/path_check.png" style="width: 75%;">
</div>

<h2>
    Conclusion
</h2>

<p>
    If you're not able to get things working, send me an email or give me a call and we'll figure it out.
</p>

<p>
    I'm excited to see you in class!
</p>

</div>
</body>
</html>
