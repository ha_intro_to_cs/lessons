# Lesson 2: Hello World

## Instructions

Remember that a program is a set of instructions that tell a computer how to do something. How do we create those instructions?

Computers process instructions encoded using 1s and 0s. These instructions are referred to as **machine code**. Here is an example.

***probably should convert these examples into an actual hello c program***

~~~
1000101100000011
100010010001110100000000000000000000000000000000
100010110100011011111100
100010000000110000000110
100010110001010010011110
~~~

Each line represents a single instruction. The first few bits of each instruction represent a command, everything after is arguments to that command, similar to how a shell operates. The computer continuously fetches instructions and executes them as fast as it can. For now, it's okay to pretend that this happens magically, entire courses are taught just on computer architecture.

The big downside here is that this is almost unreadable to humans. To program this way, you have to know the codes for each command and the codes for all your arguments. This is a lot of work.

Programmers quickly developed a better representation called **assembly language**. Here is the same example in assembly.

~~~
mov eax, [ebx]
mov [var], ebx
mov eax, [esi-4]
mov [esi+eax], cl
mov edx, [esi+4*ebx]
~~~

Each line still represents a single instruction, but this is much nicer for humans to handle. A program called an **assembler** converts the assembly code into machine code, it is responsible for remembering all the commands and the arguments.

However, this is still pretty hard to read, and it takes a lot of instructions to do simple things. Programmers wanted a better language, so a number of **high level languages** were invented. One that is still popular today is **C**.

These languages are primarily designed to be understood by humans. But they still have to be convertible into machine code, so they are not plain English. (Some people are working on converting natural language requests into computer programs using AI, this is a very new field.)

The process of converting a program from high level to machine code is called **compiling**. Programs that compile other programs are called **compilers**. (The first compiler was written in assembly, and the first assembler was written in machine code.)

For most high level languages, once you compile your program to machine code, you're done and you can use it like any other program.

The language we will be using is known as **Java**. Java is a bit different than most compiled languages, but we will discuss that when we have a working example to use.

## Exercise 0

Make a new directory called `lesson_2`, and within that create a new directory `ex0`. You don't actually have to use this directory structure, but I'm trying to make your life easier.

You can use Windows Explorer to do this, or you can use terminal. The `mkdir` command will MaKe a DIRectory for you. You might do this like so.

~~~powershell
> cd Documents/intro_to_CS
> mkdir lesson_2
> cd lesson_2
> mkdir ex0
> cd ex0
~~~

(Remember that the `>` represents the prompt, so you can see each command we're using)

Open Sublime Text and create a new document. Save it as `HelloWorld.java`.

Carefully enter the following text. Again, **do not copy and paste**.

~~~java
public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
~~~

Notice how each line is at a different level of indentation? If Sublime doesn't do that automatically for you, you can insert the spaces using the Tab key.

Don't worry if none of this makes sense, we will break down what it all means shortly. For now let's just get our program working.

The next step is to compile our program. Open your terminal and change directories to wherever you saved `HelloWorld.java`, if you don't already have it open. The Java compiler is called `javac`, and it accepts a list of files to compile. We only have one file, so we will use the following command to compile:

~~~
> javac HelloWorld.java
~~~

If you have typed everything correctly, you will get your prompt back with no output.

Now we can run our program. Because it's a Java program, we use the following command:

~~~
> java HelloWorld
~~~

You should see it produces the following output:

~~~
> java HelloWorld
Hello World!
> 
~~~

Great! We have created a program that always produces the same output. This isn't much, but we're still laying the foundation.

I can already hear you asking, "Why can't I run my program by using it's name? Why do I have to run the `java` program with my program name as an argument?" This is a good question.

Remember that I said Java was different from most compiled languages. Java uses a strategy called "Just in time compilation", or JIT. `javac` does not produce machine code, but rather an intermediate representation known as "Java byte code". You can see this if you like by opening `HelloWorld.class` in Sublime.

When you run your program with `java`, the byte code is compiled to machine code. If you run it again, Java remembers what it did last time and reuses the same machine code.

The reasons for doing it this way are complex, but there are some benefits. It can speed up compilation, and it makes it easier for the makers of Java to support lots of different types of computers.

You may find that other programmers (like me) dislike Java for this and other decisions like it. This is not a big deal, when you are learning programming, it does not matter which language you start with, the point of this course is to teach high level concepts that are useful to any programming discipline. Most times in the real world, you have no choice about the language to use anyway because it's decided by external requirements, or it was decided by people who started the project 10 years ago.

Let's experiment with our program a bit more. Modify it so that it looks like this.

~~~java
public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        System.out.println("My name is Mr. McFalls");
    }
}
~~~

Instead of my name, insert your own.

Compile it and run it again. You should see two lines of output now.

`System.out.println` is a **function** that prints the text you give it. We will learn more about functions and text later.

Congratulations! You've taken your first steps into a larger world!

## Syntax

This is a simple program in terms of what it does, but there is still a lot going on that has so far been completely unexplained. We will discuss Java's **syntax** to understand how to read this program. Remember that Java and other high level languages are designed primarily for humans to be able to read and write.

The smallest meaningful part of a language is called a **token**. In English, examples of tokens would be

* Words
* Spaces
* Punctuation
* Numbers
* etc

In Java, we will work with the following kinds of tokens:

* Keywords
* Identifiers
* Separators
* Operators
* Literals

Let's break down our Hello World program to see what each token means.

~~~java
public class HelloWorld {
~~~

This constitutes a **declaration**. We are telling the compiler that we want to define a new class of object. In Java, everything is an "object", including the program itself.

`public` is a keyword. Keywords have special meaning to the compiler. This one means that we want the declaration to be visible outside of this file. Without this, Java could not compile the program because it couldn't find it. We will see use cases for private classes later.

The space following `public` is also a token. This space is a separator, which are used to separate tokens. Separators are meaningful parts of language, without them, language would be ambiguous. Consider "everyday" vs "every day". The former means "ordinary", the latter means "daily".

***need a better use here, preferably an example of rebracketing, like "analbumcover" being ambiguous***

Because spaces are important, it is useful to have your text editor display them visibly. Most modern text editors can do this.

***sublime instructions and pictures***

`class` is another keyword, which is used to declare a class.

Next we have another space.

Then we have `HelloWorld`, which is an identifier. This identifier names the class we are declaring. The `class` keyword always expects an identifier to follow, we have to name our classes. Identifiers name things we declare or create. Identifiers can be anything you want as long as you follow these rules:

* Identifiers must not be equal to a keyword. (How would the compiler know the difference between your identifier and the keyword? Keywords are reserved for this reason)
* Identifiers cannot start with a digit.
* Identifiers cannot contain a separator. (Otherwise you would be creating *two* identifiers)

Then we have another space, followed by a `{` (open brace). Braces are separators. Here, the braces are used to contain the class **definition**. Everything inside those braces is part of the class definition.

Because braces are separators, they do not need additional spaces around them, but they don't hurt. You can add as many additional spaces as you like.

Continuing on, we have a line break. Line breaks are separators.

Next we have a tab character. Also a separator (shocker, right?).

Tabs, spaces, and newlines are all interchangeable. As long as you have at least one to separate your tokens, Java doesn't care which one. These are all grouped under the label **whitespace**. You can also have as many whitespace characters in a row as you like without changing the meaning.

~~~java
public static void main(String[] args) {
~~~

Here we declare a function which is the main function which runs our program.

`public` means the same here as it did before.

`static` is a bit confusing until we learn more about objects, but it means that you are allowed to use this function if you know about the class, even if you haven't created an object belonging to that class, which is necessary for the compiler to use it.

`void` is an identifier that designates what this function **returns**. `void` means that it doesn't return anything. This is also important for our program since we can't return anything.

`main` is an identifier which is the name of the function. The compiler searches for a function named "main", so we call our function that.

`(` (open parenthesis) is a separator which separates a function's name from it's arguments.

`String` is an identifier which refers to the type of the argument. We will learn more about types later. The `[]` (brackets) together form an operator which modifies the type to be list of Strings instead of a single string. `args` is an idenifier which is the name of the first argument.

Taken together, `String[] args` means that our program takes a list of strings as it's arguments. This matches with what we've learned from shells, and it is also required by the compiler.

The `)` is a separator which ends our list of arguments.

We have already seen `{`, here we're defining the function.

`System.out.println("Hello World!");` is a **statement**. Statements represent something we want our program to *do*. Prior to this, everything has been defining a bunch of things that we don't really care about.

`System` is an identifier which refers to a global class which is available to all Java programs.

`.` is a separator which is used to access a specific part of the System class. To access our main function, you would say `HelloWorld.main`.

`out` is an identifier which refers to a sub-class within `System`.

`println` is a function which prints text to the output followed by a new line. `print` also exists which does not print a new line. Try this out and see what it does!

`(` is familiar, we use it almost just like when we declared a function above. Instead of declaring what arguments we expect, we are passing the arguments we want it to use.

`;` is a separator which separates statements from each other.

The rest of the program is just cleanup, the close braces close the definitions of the function and the class. You can see why we use the indentation, so we know which code belongs to which definitions.

Knowing all this, the following is equally valid as far as Java is concerned.

~~~java
public
class      HelloWorld{public static void main(String[] args)   { System.out.println("Hello World!");System.out.println("My name is Mr. McFalls");



}}
~~~

You can copy and paste this into a new file and try to compile it.

This is much harder to read and writing programs like this is discouraged. Seriously, please don't do this, I will take off points.

### Syntax highlighting

You may have noticed that Sublime changes the color of certain tokens. This is called **syntax highlighting**. It is intended to make reading programs easier, your eyes automatically pick up on the differences in color.

Sublime's syntax highlighting is not perfect, it does not have a built-in Java compiler, so it's doing the best that it can. Later, we will learn to use editors that do leverage the compiler to correctly identify each token.

Sublime guesses the highlighting scheme to used based on the file extension. Until you save your files as `*.java`, it won't apply any highlighting.

### Syntax errors

Humans are extremely good at processing language which lets us be pretty relaxed about syntax. If I misspell a word or forget an apostrophe, most people will still understand me. Computers are not good at this, even the smallest deviation from proper syntax will result in an unrecoverable error. This is known as a syntax error and you will likely spend many hours in this course fighting with the compiler. This is normal, not worry if you do not immediately understand how to make the compiler happy. One of the goals of this course is to help you get comfortable with writing correct code.

## Tabs vs Spaces

There's one last *very important* issue to discuss. After you made Sublime show you your whitespace, you should have noticed the tab characters.

***image highlighting tab characters***

Tab characters are evil and should never be used to indent your code. Instead, you should use **4 spaces** to indent your code. Sublime has a handy option to switch that.

***image showing how to switch***

You should still use the Tab key on your computer, but with this mode, Sublime will insert 4 spaces instead of a tab character. You would never know the difference without seeing the whitespace, but it does actually matter. Tabs are crafty devils, they can render as different widths on different computers which makes code harder to read.

...

In case it wasn't clear, this is satire, tabs are not actually evil, plenty of programmers use them for alignment. This debate is a bit of a "holy war" among programmers. See the following references.

https://blog.codinghorror.com/death-to-the-space-infidels/

https://softwareengineering.stackexchange.com/questions/57/tabs-versus-spaces-what-is-the-proper-indentation-character-for-everything-in-e

https://i.redd.it/l2s9wtkw651z.jpg

As much as it pains me to admit it, the truth is that it really doesn't matter as long as you are consistent with your team. Since I am the tutor, I get to decide that we will all use spaces in this course. If you would like to hear my reasoning, feel free to ask.

To set Sublime to automatically use 4 spaces, add the following lines Sublime settings.

~~~
"tab_size": 4,
"translate_tabs_to_spaces": true,
~~~
