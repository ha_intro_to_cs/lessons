<!DOCTYPE html>
<meta charset="UTF-8">
<html>
<head>
    <link href="../common/prism.css" rel="stylesheet" />
    <link rel="stylesheet" href="../common/styles.css">
</head>
<script src="../common/prism.js"></script>
<script src="../common/settings.js"></script>
<body>
<div class="centered" style="max-width: 55em;">

<h1>
    Lesson 2: Hello World
</h1>

<h2>
    Instructions
</h2>

<p>
    Remember that a program is a set of instructions that tell a computer how to do something. How do we create those instructions?
</p>

<p>
    Computers process instructions encoded using 1s and 0s. These instructions are referred to as <strong>machine code</strong>. Here is an example.
</p>

<pre class="lang-xxx"><code>
    1011100000100001000010100000000000000000
    1010001100001100000100000000000000000110
    1011100001101111011100100110110001100100
    1010001100001000000100000000000000000110
    1011100001101111001011000010000001010111
    1010001100000100000100000000000000000110
    1011100001001000011001010110110001101100
    1010001100000000000100000000000000000110
    1011100100000000000100000000000000000110
    1011101000010000000000000000000000000000
    1011101100000001000000000000000000000000
    1011100000000100000000000000000000000000
    1100110110000000
    1011100000000001000000000000000000000000
    1100110110000000
</code></pre>

<p>
    Each line represents a single instruction. The first few bits of each instruction represent a command, everything after is arguments to that command, similar to how a shell operates. The computer continuously fetches instructions and executes them as fast as it can. For now, it's okay to pretend that this happens magically, entire courses are taught just on computer architecture.
</p>

<p>
    The big downside here is that this is almost unreadable to humans. To program this way, you have to know the codes for each command and the codes for all your arguments. This is a lot of work.
</p>

<p>
    Programmers quickly developed a better representation called <strong>assembly language</strong>. Here is the same example in assembly.
</p>

<pre class="lang-xxx"><code>
    mov    eax,0xa21
    movabs ds:0x6c726fb80600100c,eax

    movabs fs:0x202c6fb806001008,eax

    push   rdi
    movabs ds:0x6c6548b806001004,eax

    ins    BYTE PTR es:[rdi],dx
    movabs ds:0x1000b906001000,eax

    (bad)
    mov    edx,0x10
    mov    ebx,0x1
    mov    eax,0x4
    int    0x80
    mov    eax,0x1
    int    0x80
</code></pre>

<p>
    (Credit to XlogicX on <a href=https://stackoverflow.com/a/25483473/3287359>StackOverflow</a>)
</p>

<p>
    Each line still represents a single instruction (roughly), but this is much nicer for humans to handle. A program called an <strong>assembler</strong> converts the assembly code into machine code, it is responsible for remembering all the commands and the arguments.
</p>

<p>
    However, this is still pretty hard to read, and it takes a lot of instructions to do simple things. Programmers wanted a better language, so a number of <strong>high level languages</strong> were invented. One that is still popular today is <strong>C</strong>.
</p>

<p>
    These languages are primarily designed to be understood by humans. But they still have to be convertible into machine code, so they are not plain English. (Some people are working on converting natural language requests into computer programs using AI, this is a very new field.)
</p>

<p>
    The process of converting a program from high level to machine code is called <strong>compiling</strong>. Programs that compile other programs are called <strong>compilers</strong>. (The first compiler was written in assembly, and the first assembler was written in machine code.)
</p>

<p>
    For most high level languages, once you compile your program to machine code, you're done and you can use it like any other program.
</p>

<p>
    The language we will be using is known as <strong>Java</strong>. Java is a bit different than most compiled languages, but we will discuss that when we have a working example to use.
</p>

<p>
    Do not be alarmed by the scary examples! Java is <em>much</em> easier to read and write. I'm showing you these examples so you have some concept of what's happening behind the scenes, and so you appreciate the tools we have today.
</p>

<h2>
    Exercise 0
</h2>

<p>
    Make a new directory called <code>lesson_2</code>, and within that create a new directory <code>ex0</code>. You don't have to use this directory structure, but I'm trying to make your life easier.
</p>

<p>
    You can use Windows Explorer to do this, or you can use terminal. The <code>mkdir</code> command will MaKe a DIRectory for you. You might do this like so.
</p>

<pre class="lang-xxx"><code>
    > cd Documents/intro_to_CS
    > mkdir lesson_2
    > cd lesson_2
    > mkdir ex0
    > cd ex0
</code></pre>

<p>
    (Remember that the <code>></code> represents the prompt, so you can see each command we're using)
</p>

<p>
    Open Sublime Text and create a new document. Save it as <code>HelloWorld.java</code>.
</p>

<p>Carefully enter the following text. Again, <strong>do not copy and paste</strong>.</p>

<pre class="language-java line-numbers"><code>
    public class HelloWorld {
        public static void main(String[] args) {
            System.out.println("Hello World!");
        }
    }

</code></pre>

<p>
    You don't need to type the line numbers, they are not part of the program, then are just for our reference. Sublime will number lines similarly.
</p>

<p>
    Notice how each line is at a different level of indentation? If Sublime doesn't do that automatically for you, you can insert the spaces using the Tab key.
</p>

<p>
    Also note the blank line at the end. You should include that as well, it is customary for text files to end with a blank line.
</p>

<p>
    If you're having trouble finding the bracket (<code>[</code>) or brace (<code>{</code>), they are to the right of the "p" key.
</p>

<p>
    Don't worry if none of what you just typed makes sense, we will break down what it all means shortly. For now let's just get our program working.
</p>

<p>
    The next step is to compile our program. Open your terminal and change directories to wherever you saved <code>HelloWorld.java</code>, if you don't already have it open. The Java compiler is called <code>javac</code>, and it accepts a list of files to compile. We only have one file, so we will use the following command to compile:
</p>

<pre class="lang-xxx"><code>
    > javac HelloWorld.java
</code></pre>

<p>
    If you have typed everything correctly, you will get your prompt back with no output.
</p>

<p>
    Now we can run our program. Because it's a Java program, we use the following command:
</p>

<pre class="language-powershell"><code>
    > java HelloWorld
</code></pre>

<p>
    You should see it produces the following output:
</p>

<pre class="lang-xxx"><code>
    > java HelloWorld
    Hello World!
</code></pre>

<p>
    Great! We have created a program that always produces the same output. This isn't much, but we're still laying the foundation.
</p>

<p>
    I can already hear you asking, "Why can't I run my program by using it's name? Why do I have to run the <code>java</code> program with my program name as an argument?" This is a good question.
</p>

<p>
    Remember that I said Java was different from most compiled languages. Java uses a strategy called "Just in time compilation", or JIT. <code>javac</code> does not produce machine code, but rather an intermediate representation known as "Java byte code". You can see this if you like by opening <code>HelloWorld.class</code> in Sublime.
</p>

<p>
    When you run your program with <code>java</code>, the byte code is compiled to machine code. If you run it again, Java remembers what it did last time and reuses the same machine code.
</p>

<p>
    The reasons for doing it this way are complex, but there are some benefits. It can speed up compilation, and it makes it easier for the makers of Java to support lots of different types of computers.
</p>

<p>
    You may find that other programmers dislike Java for this and other decisions like it. This is not a big deal, when you are learning programming, it does not matter which language you start with, the point of this course is to teach high level concepts that are useful to any programming discipline. Most times in the real world, you have no choice about the language to use anyway because it's decided by external requirements, or it was decided by people who started the project 10 years ago.
</p>

<p>
    Let's experiment with our program a bit more. Modify it so that it looks like this.
</p>

<pre class="language-java line-numbers"><code>
    public class HelloWorld {
        public static void main(String[] args) {
            System.out.println("Hello World!");
            System.out.println("My name is Mr. McFalls");
        }
    }

</code></pre>

<p>
    Instead of my name, insert your own.
</p>

<p>
    Compile it and run it again. You should see two lines of output now.
</p>

<p>
    <code class="language-java">System.out.println()</code> is a <strong>function</strong> that prints the text you give it. We will learn more about functions later, but for now you can think of it as a "mini-program". Functions are sets of instructions, they take input and produce output. They're a way for us to reuse code and not have to write everything from scratch all the time.
</p>

<p>
    One last modification!
</p>

<pre class="language-java line-numbers"><code>
    public class HelloWorld {
        public static void main(String[] args) {
            System.out.println("Hello World!");
            // I want to identify myself
            System.out.println("My name is Mr. McFalls");
        }
    }

</code></pre>

<p>
    On line 4, we added a <strong>comment</strong>. Comments start with two forward slashes (<code>//</code>) and are ignored by the compiler, so they can contain any text. Recompile and rerun the program, you will see that it behaves exactly the same no matter what we put in the comment.
</p>

<p>
    Code alone will tell you <em>what</em> is happening, but not <em>why</em> it should be happening, comments are useful to explain this. You should get in the habit of commenting your code to justify it, but we will spend more time on this later.
</p>

<p>
    Congratulations! You've taken your first steps into a larger world!
</p>

<h3>
    Side note
</h3>

<p>
    Those of you who have programmed before may be wondering (or complaining about) why we're not using an IDE. For you, I will quote Tony Stark, "If you're nothing without the IDE, you shouldn't have it."
</p>

<p>
    IDEs do a lot of very useful things, and we will cover them, but you need to learn the fundamentals. If you're taking shortcuts on day one, you'll make more progress initially, but later down the road gaps in your knowledge will start to become a problem, and it's hard to know how to fill them if you don't even know what's missing.
</p>

<h2>
    Syntax
</h2>

<p>
    This is a simple program in terms of what it does, but there is still a lot going on that has so far been completely unexplained. We will discuss Java's <strong>syntax</strong> to understand how to read this program. Remember that Java and other high level languages are designed primarily for humans to be able to read and write.
</p>

<h3>
    Coarse details
</h3>

<p>
    This section is fairly heavy and most students might not need this level of detail immediately. If you don't have a burning desire to understand exactly how the code is read by the computer, you can just read this abridged section for now. You should come back after a few lessons and try this out, it is important to understand. Skip to "Syntax errors" when you finish this.
</p>

<p>
    The first line of code <code class="language-java">public class HelloWorld {</code> declares a publicly visible <strong>class</strong>. In Java, all code must be inside a class, and it needs to be a public class if you want to use it outside of the file, like if we're compiling it. The braces mark the start and end of the class. This class represents your program, so it should have your program's name, in this case, <code>HelloWorld</code>.
</p>

<p>
    The second line of code <code class="language-java">public static void main(String[] args) {</code> declares a publicly visible function. In order to run a program from a shell, Java requires a function exactly like this. It must be named <code>main</code>, it must accept one argument called <code>args</code>, it must be public and static. We will cover static when we learn about objects in Lesson 4.
</p>

<p>
    Line three is a <strong>statement</strong> inside the <code>main</code> function.
</p>

<div class="definition">
    <span class="definition-h">Definition</span>
    <p>
        An <strong>statement</strong> is a piece of code that commands the computer to take a particular action.
    </p>
</div>

<p>
    Statements are similar to instructions! The previous two lines just define structure that Java requires, statements are needed to do anything useful. Each statement ends with a semicolon.
</p>

<p>
    The last few lines are just braces closing off the definitions, nothing interesting.
</p>

<h3>
    Fine details
</h3>

<p>
    The smallest meaningful part of a language is called a <strong>token</strong>. In English, examples of tokens would be
</p>

<ul>
    <li>Words</li>
    <li>Spaces</li>
    <li>Punctuation</li>
    <li>Numbers</li>
    <li>etc</li>
</ul>

<p>
    In Java, we will work with the following kinds of tokens:
</p>

<ul>
    <li>Keywords</li>
    <li>Identifiers</li>
    <li>Separators</li>
    <li>Operators</li>
    <li>Literals</li>
</ul>

<p>
    Let's break down our Hello World program to see what each token means.
</p>

<pre class="language-java line-numbers" data-start="1"><code>
    public class HelloWorld {
</code></pre>

<p>
    This constitutes a <strong>declaration</strong>. We are telling the compiler that we want to define a new class of object. In Java, everything is an "object", including the program itself.
</p>

<p>
    <code class="language-java">public</code> is a keyword. Keywords have special meaning to the compiler. This one means that we want the declaration to be visible outside of this file. Without this, Java could not compile the program because it couldn't find it. We will see use cases for private classes later.
</p>

<p>
    The space following <code class="language-java">public</code> is also a token. This space is a separator, which are used to separate tokens. Separators are meaningful parts of language, without them, language would be ambiguous. Consider "hotdog" vs "hot dog". The former is a type of sausage sandwich, the latter is a canine at elevated temperature. For a more comical example, see "Heal Thy Burgers." Hopefully I've convinced you that spacing is important.
</p>

<div class="center-children">
    <img src="img/heal_thy_burgers.jpg" style="width: 50%;">
</div>
<div class="center-children"><span><em>"Heal Thy Burgers", bad graphic design!</em></span></div>

<p>
    Because spaces are important, it is useful to have your text editor display them visibly. Most modern text editors can do this. Open your Sublime settings with Preferences > Settings. Sublime will open two text files. In the file on the right, enter <code>"draw_white_space": "all"</code> and save. You can close this window. Now you should be able to see the whitespace.
</p>

<div class="center-children">
    <img src="img/sublime_set_show_whitespace.png" style="width: 75%;">
</div>
<div class="center-children"><span><em>Configure Sublime to show whitespace.</em></span></div>

<br>

<div class="center-children">
    <img src="img/sublime_show_whitespace_example.png" style="width: 75%;">
</div>
<div class="center-children"><span><em>Hello World with whitespace showing.</em></span></div>

<p>
    <code class="language-java">class</code> is another keyword, which is used to declare a class.
</p>

<p>
    Next we have another space.
</p>

<p>
    Then we have <code>HelloWorld</code>, which is an identifier. This identifier names the class we are declaring. An identifier should always follow the <code class="language-java">class</code> keyword, we have to name our classes. Identifiers name things we declare or create. Identifiers can be anything you want as long as you follow these rules:
</p>

<ul>
    <li>Identifiers must not be equal to a keyword. (How would the compiler know the difference between your identifier and the keyword? Keywords are reserved for this reason)</li>
    <li>Identifiers cannot start with a digit.</li>
    <li>Identifiers cannot contain a separator. (Otherwise you would be creating <em>two</em> identifiers)</li>
</ul>

<p>
    Then we have another space, followed by a <code>{</code> (open brace). Braces are separators. Here, the braces are used to contain the class <strong>definition</strong>. Everything inside those braces is part of the class definition.
</p>

<p>
    Because braces are separators, they do not need additional spaces around them, but they don't hurt. You can add as many additional spaces as you like.
</p>

<p>
    Continuing on, we have a line break. Line breaks are separators.
</p>

<p>
    Next we have a tab character. Also a separator (shocker, right?).
</p>

<p>
    Tabs, spaces, and newlines are all interchangeable. As long as you have at least one to separate your tokens, Java doesn't care which one. These are all grouped under the label <strong>whitespace</strong>. You can also have as many whitespace characters in a row as you like without changing the meaning.
</p>

<pre class="language-java line-numbers" data-start="2"><code>
    public static void main(String[] args) {
</code></pre>

<p>
    Here we declare a function which is the main function which runs our program.
</p>

<p>
    <code class="language-java">public</code> means the same here as it did before.
</p>

<p>
    <code class="language-java">static</code> is a bit confusing until we learn more about objects, but it means that you are allowed to use this function if you know about the class, even if you haven't created an object belonging to that class, which is necessary for the compiler to use it.
</p>

<p>
    <code class="language-java">void</code> is an identifier that designates what this function <strong>returns</strong>. <code class="language-java">void</code> means that it doesn't return anything. This is also important for our program since we can't return anything.
</p>

<p>
    <code>main</code> is an identifier which is the name of the function. The compiler searches for a function named "main", so we call our function that.
</p>

<p>
    <code>(</code> (open parenthesis) is a separator which separates a function's name from it's arguments.
</p>

<p>
    <code>String</code> is an identifier which refers to the type of the argument. We will learn more about types later. The <code>[]</code> (brackets) together form an operator which modifies the type to be list of Strings instead of a single string. <code>args</code> is an identifier which is the name of the first argument.
</p>

<p>
    Taken together, <code>String[] args</code> means that our program takes a list of strings as it's arguments. This matches with what we've learned from shells, and it is also required by the compiler.
</p>

<p>
    The <code>)</code> is a separator which ends our list of arguments.
</p>

<p>
    We have already seen <code>{</code>, here we're defining the function.
</p>
<p>
    <code class="language-java">System.out.println("Hello World!");</code> is a statement. Refer to the definition above.
</p>

<p>
    <code>System</code> is an identifier which refers to a global class which is available to all Java programs.
</p>

<p>
    <code>.</code> is a separator which is used to access a specific part of the System class. To access our main function, you would say <code>HelloWorld.main</code>.
</p>

<p>
    <code>out</code> is an identifier which refers to a sub-class within <code>System</code>.
</p>

<p>
    <code>println</code> is a function which prints text to the output followed by a new line. <code>print</code> also exists which does not print a new line. Try this out and see what it does!
</p>

<p>
    <code>(</code> is familiar, we use it almost just like when we declared a function above. Instead of declaring what arguments we expect, we are passing the arguments we want it to use.
</p>

<p>
    <code>;</code> is a separator which separates statements from each other.
</p>

<p>
    The rest of the program is just cleanup, the close braces close the definitions of the function and the class. You can see why we use the indentation, so we know which code belongs to which definitions.
</p>

<p>
    Knowing all this, the following is equally valid as far as Java is concerned.
</p>

<pre class="language-java line-numbers" data-start="2"><code>
    public
    class      HelloWorld{public static void main(String[] args)   { System.out.println("Hello World!");System.out.println("My name is Mr. McFalls");



    }}
</code></pre>

<p>
    You can copy and paste this into a new file and try to compile it.
</p>

<p>
    This is much harder to read and writing programs like this is discouraged. Seriously, please don't do this, I will take off points.
</p>

<h3>Syntax highlighting</h3>

<p>
    You may have noticed that Sublime changes the color of certain tokens. This is called <strong>syntax highlighting</strong>. It is intended to make reading programs easier, your eyes automatically pick up on the differences in color. For example you can see that all keywords are the same color.
</p>

<p>
    Sublime's syntax highlighting is not perfect, it does not have a built-in Java compiler, so it's doing the best that it can. Later, we will learn to use editors that do leverage the compiler to correctly identify each token.
</p>

<p>
    Sublime guesses the highlighting scheme to used based on the file extension. Until you save your files as <code>*.java</code>, it won't apply any highlighting.
</p>

<h3>
    Syntax errors
</h3>

<p>
    Humans are extremely good at processing language which lets us be pretty relaxed about syntax. If I misspell a word or forget an apostrophe, most people will still understand me. This is a big strength. Computers are not good at this, even the smallest deviation from proper syntax will result in an unrecoverable error. This is known as a syntax error and you will likely spend many hours in this course fighting with the compiler. This is normal, don't worry if you do not immediately understand how to make the compiler happy. One of the goals of this course is to help you get comfortable with writing correct code.
</p>

<h2>
    Tabs vs Spaces
</h2>

<p>
    There's one last <em>very important</em> issue to discuss. In our discussion on syntax, we learned how tabs are used to indent our code and we also configured Sublime to show us the invisible tabs.
</p>

<p>
    Tab characters are evil and should never be used to indent your code. Instead, you should use <strong>4 spaces</strong> to indent your code. Sublime has a handy option to switch that.
</p>

<div style="display: flex;">
    <img src="img/submlime_set_spaces_1.png" style="flex: 50%; padding: 5px;">
    <img src="img/submlime_set_spaces_2.png" style="flex: 50%; padding: 5px;">
</div>
<div class="center-children"><span><em>Switch to spaces.</em></span></div>

<p>
    Unfortunately this doesn't automatically reformat your existing code, to do that, select your text with Control-A and choose Edit > Line > Reindent.
</p>

<p>
    You should still use the Tab key on your computer, but with this mode, Sublime will insert 4 spaces instead of a tab character. You would never know the difference without seeing the whitespace, but it does actually matter. Tabs are crafty devils, they can render as different widths on different computers which makes code harder to read.
</p>

<p>
    ...
</p>

<p>
    In case it wasn't clear, this is satire, tabs are not actually evil, plenty of programmers use them for alignment. This debate is a bit of a "holy war" among programmers. See the following references.
</p>

<p>
    <a href="https://blog.codinghorror.com/death-to-the-space-infidels/">Death to Space Infidels</a>
</p>

<p>
    <a href="https://softwareengineering.stackexchange.com/questions/57/tabs-versus-spaces-what-is-the-proper-indentation-character-for-everything-in-e">StackExchange Discussion</a>
</p>

<div class="center-children">
    <img src="img/tabs_vs_spaces_meme_1.jpg" style="width: 50%;">
</div>
<div class="center-children"><span><em>We have memes too!</em></span></div>

<p>
    As much as it pains me to admit it, the truth is that it really doesn't matter as long as you are consistent with your team. Since I am the tutor, I get to decide that we will all use spaces in this course. If you would like to hear my reasoning, feel free to ask, it will be a fun discussion!
</p>

<p>
    To set Sublime to automatically use 4 spaces, add the following lines to your settings.
</p>

<pre class="lang-xxx"><code>
    "tab_size": 4,
    "translate_tabs_to_spaces": true,
</code></pre>

<p>
    If you skipped the syntax section, open your Sublime settings with Preferences > Settings. You should edit the file on the right so that it looks like this after you save it.
</p>

<div class="center-children">
    <img src="img/submlime_set_spaces_default.png" style="width: 75%;">
</div>
<div class="center-children"><span><em>Automatically use spaces.</em></span></div>

</div>
</body>
</html>
