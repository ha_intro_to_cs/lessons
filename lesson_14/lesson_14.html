<!DOCTYPE html>
<meta charset="UTF-8">
<html>
<head>
    <link href="../common/prism.css" rel="stylesheet" />
    <link rel="stylesheet" href="../common/styles.css">
</head>
<script src="../common/prism.js"></script>
<script src="../common/settings.js"></script>
<body>
<div class="centered" style="max-width: 55em;">

<h1>
    Lesson 14: Custom classes
</h1>

<p>
    Java is an Object Oriented language, which means it is designed around Objects. Not everything can be represented by the types we have discussed so far, you will frequently find the need to create your own objects.
</p>

<p>
    As we have seen, classes define objects that can hold data and operate on it with functions (sometimes called methods). Let's first look at a simple case and create a class just to hold some data.
</p>

<h2>
    Example 0
</h2>

<p>
    Things are about to get tricky. In general, we need to create a file for each class that we make. This isn't strictly required, but Java complains if you don't and it's good practice to make sure your source files don't get super long. Each class should have a matching file name, <code>Coordinate2d</code> should be in <code>Coordinate2d.java</code> and <code>Ex0</code> should be in <code>Ex0.java</code> as usual.
</p>

<pre class="lang-java"><code>
public class Coordinate2d {
    public double x;
    public double y;
}
</code></pre>

<pre class="lang-java"><code>
public class Ex0 {
    static Coordinate2d getPosition() {
        Coordinate2d position = new Coordinate2d();
        position.x = 10;
        position.y = 5;
        return position;
    }
    public static void main(String[] args) {
        Coordinate2d position = getPosition();
        System.out.println(position.x + " " + position.y);
    }
}
</code></pre>

<p>
    Notice that we don't have an <code>import</code> statement for our class like we've had to import other classes. This is because both classes belong to the same directory and Java automatically finds it.
</p>

<p>
    Next to compile these two files, we can still use the same command we've been using <code>javac Ex0.java</code>. Again, Java is smart and automatically finds the <code>Coordinate2d</code> class.
</p>

<p>
    If you're using IntelliJ, the only difference is that you will have your <code>package</code> declaration at the top of each file. Everything else should be the same, you don't need to import the class and you can run <code>Ex0</code> normally.
</p>

<p>
    The output isn't very interesting.
</p>

<pre class="lang-xxx"><code>
> java Ex0
10.0 5.0
</code></pre>

<p>
    But that's not the point, the point is that we've learned how to build structures to hold more complex data. This is also an example of how you can return multiple values from a function.
</p>

<p>
    Defining <code>Coordinate2d</code> like this is novel, but shouldn't have any big surprises. We already know how to declare a class. We already know that <code>public</code> makes things visible outside of the class. The only thing we haven't seen before is declaring variables like this. This type of variable is referred to as a <strong>class member variable</strong>, usually just shorted to <strong>member</strong>, or sometimes <strong>field</strong>.
</p>

<p>
    Hopefully our usage of <code>Coordinate2d</code> in <code>Ex0</code> shouldn't be surprising either, we can make an instance with the constructor and we can access public members.
</p>

<p>
    What happens if you make the members private? Try it and find out.
</p>

<hr>

<p>
    Example 0 is the simplest case, <code>Coordinate2d</code> is just a named collection of data. This is plenty useful, but we can do even more. In object oriented languages, we want to not just define what the data is, but what we can do with it. Let's look at a more complex example.
</p>

<h2>
    Example 1
</h2>

<pre class="lang-java line-numbers"><code>
public class Coordinate2d {
    private double x;
    private double y;

    public Coordinate2d() {
        x = 0;
        y = 0;
    }

    public Coordinate2d(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void add(Coordinate2d other) {
        this.setX(this.x + other.getX());
        this.setY(this.y + other.getY());
    }
}

</code></pre>

<pre class="lang-java line-numbers"><code>
public class Ex1 {
    public static void main(String[] args) {
        Coordinate2d point = new Coordinate2d();
        System.out.println("Point: " + point);
        Coordinate2d offset = new Coordinate2d(10, 5);
        point.add(offset);
        System.out.println("Point: " + point);
    }
}

</code></pre>

<p>
    The output still isn't very impressive on the surface.
</p>

<pre class="lang-xxx"><code>
> java Ex1
Point: (0.0, 0.0)
Point: (10.0, 5.0)
</code></pre>

<p>
    It doesn't look like much, but there is a lot going on. Firstly, we changed <code>x</code> and <code>y</code> to <code>private</code>, which means that they aren't accessible outside of the class. This is called <strong>encapsulation</strong>, we are hiding the data from the outside world, which lets us dictate exactly what can be done with the data.
</p>

<p>
    This isn't always required, there are plenty of cases, especially with simple data, where it's much easier to leave your data public, but most of the time you want to encapsulate it.
</p>

<p>
    We've also added a bunch of functions to our class. Let's go through each one.
</p>

<p>
    <code>public Coordinate2d()</code>
</p>

<p>
    This is a constructor. We've seen them before, we use them to create instances of the class. This is specifically called a <strong>default constructor</strong> because you can create an object with no arguments. Here we initialize <code>x</code> and <code>y</code> to <code>0</code> because that makes sense for coordinates. Defaults should be chosen based on what makes sense for what data is being represented.
</p>

<p>
    <code>public Coordinate2d(double x, double y)</code>
</p>

<p>
    This is <em>also</em> a constructor. Yes, you can have more than one constructor! This means there can be multiple ways to make a new object. Here, we accept values for <code>x</code> and <code>y</code> which lets us set them to any value we want.
</p>

<p>
    We need to use <code>this.x = x</code> to assign to the member variable because <code>x = x</code> would cause Java to assign the variable to itself, which doesn't do anything. Java would see <code>x</code> as the same thing both times. <code>this</code> is a keyword which refers to the instance this function is being called on. It refers to "this object". That's a little confusing, but it's important.
</p>

<p>
    <code>public String toString()</code>
</p>

<p>
    This is a standard function that Java will call automatically to convert your object into a <code>String</code> so it can be printed. The function can return any string, you have to make it make sense for your class. For a coordinate, printing out "(x, y)" seems reasonable. Other things may not be so straightforward. If you expect your object to be printed it's a good idea to implement this. Even if you wouldn't expect to print it, this might still be a good idea because it can help with debugging.
</p>

<p>
    <code>public double getX()</code>
</p>

<p>
    This function just returns the value of <code>x</code>. If you need to do anything with the coordinate, you'll need a way read the value. This type of function is called a <strong>getter</strong>. This helps with encapsulation, we provide a way to read the value of <code>x</code>, but no way to write to it (because <code>x</code> is private).
</p>

<p>
    <code>public void setX(double x)</code>
</p>

<p>
    This function modifies the value of <code>x</code>. This is called a <strong>setter</strong>. It works similarly to the constructor, but it just sets the value of <code>x</code>. You can see what we use <code>this.x</code> again to make it clear <em>which</em> <code>x</code> we're talking about.
</p>

<p>
    Since we have both a setter and a getter, it is effectively public, but it is usually still a good idea to keep it private. You might decide to change how you want to store the data and if other people are using your class, you might not want to make them change their code. If you know you're the only person working on a project, you can just make your members public, but it's still good practice to have getters and setters. But you won't always need both, it depends on the data you are representing.
</p>

<p>
    <code>public double getY()</code>
</p>

<p>
    Getter for <code>y</code>.
</p>

<p>
    <code>public void setY(double y)</code>
</p>

<p>
    Setter for <code>y</code>.
</p>

<p>
    <code>public Coordinate2d add(Coordinate2d other)</code>
</p>

<p>
    This is a function that adds two <code>Coordinate2d</code>s together, handy if we're doing geometry. Notice that instead of <em>modifying</em> the members, we create and return a new <code>Coordinate2d</code>.
</p>

<p>
    We also use <code>this.setX()</code> instead of <code>setX()</code>, but this is an aesthetic choice, it's not required. Personally, I like it because it clearly separates <code>this</code> and <code>other</code>.
</p>

<p>
    Using this class in <code>Ex1</code> is straightforward once we understand the functions. It's easy to print the <code>Coordinate2d</code>, and we can modify its contents.
</p>

<hr>

<h2>
    Principles of OOP
</h2>

<p>
    There are several principles that you should keep in mind when working with object oriented design.
</p>

<ul>
    <li>
        <p>
            Abstraction
        </p>
        <p>
            <strong>Abstraction</strong> is the process of representing real world concepts in compute code. Like we represented a 2D point with <code>Coordinate2d</code>, you should seek to represent concepts as classes. This will not come easy at first and there isn't a silver bullet to learning it, you just have to practice, get feedback, repeat.
        </p>
    </li>
    <li>
        <p>
            Single Responsibility
        </p>
        <p>
            Each class should do one thing, and do it well. Classes should represent one type of data, or manage one resource. For example, a class made to read and write save files should not also manage loading saves into the game or manage a list of saves. Each of those should be their own class.
        </p>
    </li>
    <li>
        <p>
            Encapsulation
        </p>
        <p>
            Classes should encapsulate their data and not expose more functionality than is necessary. You should establish a clean interface that exactly describes what you can / should use the class to do.
        </p>
    </li>
    <li>
        <p>
            More
        </p>
        <p>
            There are other principles like Inheritance, but they require more instruction that we don't have time for in this lesson.
        </p>
    </li>
</ul>

<h2>
    Criticisms of OOP
</h2>

<p>
    OOP is not universally well-liked, and Java is strongly married to the concept. In Java, OOP is not a choice, other "OOP" languages like Python and C++ are actually quite flexible and allow you to choose your strategy. This is led to several comical criticisms like <a href=https://steve-yegge.blogspot.com/2006/03/execution-in-kingdom-of-nouns.html>this blog post</a>.
</p>

<p>
    Ultimately, this is mostly personal preference. There isn't any empirical data that suggests a specific programming style is faster or less bug-prone.
</p>

<p>
    Every programming language has strengths and weaknesses, it's unlikely that you'll find one that doesn't annoy you in some way. The truth is that programming languages are just tools, and Java is a good enough tool for a lot of jobs.
</p>

<h2>
    Guided Practice
</h2>

<p>
    Complete the following <code>Cup</code> class.
</p>

<pre class="lang-java"><code>
    public class Cup {

        public Cup() {
            
        }

        public Cup(int capacityOz) {
            
        }

        public void fill(int fillOz) {
            
        }

        public int pour() {
            return -1;
        }

        public int getCapacityOz() {
            return -1;
        }

        public int getContainsOz() {
            return -1;
        }

        public boolean isFull() {
            return false;
        }

        public boolean isEmpty() {
            return false;
        }

        public String toString() {
            return "";
        }
    }
</code></pre>

<p>
    The functions are pre-filled with dummy values like <code>-1</code> so that it will compile, but you should replace these.
</p>

<p>
    <code>public Cup()</code>
</p>

<p>
    The default constructor should set the capacity to 16 ounces and the contains amount to 0 ounces.
</p>

<p>
    <code>public Cup(int capacityOz)</code>
</p>

<p>
    This constructor should set the capacity to <code>capacityOz</code> and still set the contains amount to 0 ounces.
</p>

<p>
    <code>public void fill(int fillOz)</code>
</p>

<p>
    This function should add <code>fillOz</code> to the contains amount. If this would overflow the capacity, set the contains amount equal to the capacity. This represents the cup overflowing, but it will still be full.
</p>

<p>
    <code>public int pour()</code>
</p>

<p>
    Pour out the contents of the cup by setting the contains amount to 0. The function should return the amount the cup contained before emptying it.
</p>

<p>
    <code>public int getCapacityOz()</code>
</p>

<p>
    This function should return the capacity of the cup.
</p>

<p>
    <code>public int getContainsOz()</code>
</p>

<p>
    This function should return how many ounces are currently in the cup.
</p>

<p>
    <code>public boolean isFull()</code>
</p>

<p>
    This function should return <code>true</code> if the cup is full, and <code>false</code> otherwise.
</p>

<p>
    <code>public boolean isEmpty()</code>
</p>

<p>
    This function should return <code>true</code> if the cup is empty, and <code>false</code> otherwise.
</p>

<p>
    <code>public String toString()</code>
</p>

<p>
    This function should return a string with the following format: <code>"Cup: {contains}/{capacity}oz"</code>
</p>

<p>
    For example: <code>"Cup: 0/16oz"</code> or <code>"Cup: 10/48oz"</code>
</p>

<hr>

<p>
    To test your class, you can use the following program.
</p>

<pre class="lang-java"><code>
    public class CupExample {
        public static void main(String[] args) {
            Cup cupA = new Cup();
            System.out.println("This is a default cup: " + cupA);
            System.out.println("It is a " + cupA.getCapacityOz() + "oz cup");
            System.out.println("Is it empty? " + cupA.isEmpty());
            System.out.println("Is it full? " + cupA.isFull());
            System.out.println("Let's fill it with 8oz.");
            cupA.fill(8);
            System.out.println("Here is the cup now: " + cupA);
            System.out.println("Is it empty? " + cupA.isEmpty());
            System.out.println("Is it full? " + cupA.isFull());
            System.out.println("Let's add 8 more ounces");
            cupA.fill(8);
            System.out.println("Here is the cup now: " + cupA);
            System.out.println("Is it empty? " + cupA.isEmpty());
            System.out.println("Is it full? " + cupA.isFull());
            System.out.println("Time to pour it out!");
            int poured = cupA.pour();
            System.out.println("We poured out " + poured + "oz");
            System.out.println("Here is the cup now: " + cupA);
            System.out.println("Is it empty? " + cupA.isEmpty());
            System.out.println("Is it full? " + cupA.isFull());

            System.out.println();
            Cup cupB = new Cup(48);
            System.out.println("This is a bigger cup: " + cupB);
            System.out.println("The cup contains " + cupB.getContainsOz()
                + "oz out of " + cupB.getCapacityOz() + "oz");
            System.out.println("Let's fill it with 20oz.");
            cupB.fill(20);
            System.out.println("The cup contains " + cupB.getContainsOz()
                + "oz out of " + cupB.getCapacityOz() + "oz");
            System.out.println("Let's overfill it with 100 more ounces.");
            cupB.fill(100);
            System.out.println("The cup contains " + cupB.getContainsOz()
                + "oz out of " + cupB.getCapacityOz() + "oz");
            System.out.println("Is it full? " + cupB.isFull());
            System.out.println("Time to pour it out!");
            poured = cupB.pour();
            System.out.println("We poured out " + poured + "oz");
            System.out.println("Here is the cup now: " + cupB);
            System.out.println("Is it empty? " + cupB.isEmpty());
            System.out.println("Is it full? " + cupB.isFull());
            System.out.println("The cup contains " + cupB.getContainsOz()
                + "oz out of " + cupB.getCapacityOz() + "oz");
        }
    }

</code></pre>

<p>
    If everything is working, you should get the following output.
</p>

<pre class="lang-xxx"><code>
    This is a default cup: Cup (0/16oz)
    It is a 16oz cup
    Is it empty? true
    Is it full? false
    Let's fill it with 8oz.
    Here is the cup now: Cup (8/16oz)
    Is it empty? false
    Is it full? false
    Let's add 8 more ounces
    Here is the cup now: Cup (16/16oz)
    Is it empty? false
    Is it full? true
    Time to pour it out!
    We poured out 16oz
    Here is the cup now: Cup (0/16oz)
    Is it empty? true
    Is it full? false

    This is a bigger cup: Cup (0/48oz)
    The cup contains 0oz out of 48oz
    Let's fill it with 20oz.
    The cup contains 20oz out of 48oz
    Let's overfill it with 100 more ounces.
    The cup contains 48oz out of 48oz
    Is it full? true
    Time to pour it out!
    We poured out 48oz
    Here is the cup now: Cup (0/48oz)
    Is it empty? true
    Is it full? false
    The cup contains 0oz out of 48oz
</code></pre>

</div>
</body>
</html>
