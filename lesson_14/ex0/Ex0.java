public class Ex0 {
    static Coordinate2d getPosition() {
        Coordinate2d position = new Coordinate2d();
        position.x = 10;
        position.y = 5;
        return position;
    }
    public static void main(String[] args) {
        Coordinate2d position = getPosition();
        System.out.println(position.x + " " + position.y);
    }
}
