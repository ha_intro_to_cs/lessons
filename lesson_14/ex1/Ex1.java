public class Ex1 {
    public static void main(String[] args) {
        Coordinate2d point = new Coordinate2d();
        System.out.println("Point: " + point);
        Coordinate2d offset = new Coordinate2d(10, 5);
        point.add(offset);
        System.out.println("Point: " + point);
    }
}
