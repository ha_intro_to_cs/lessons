public class Coordinate2d {
    private double x;
    private double y;

    public Coordinate2d() {
        x = 0;
        y = 0;
    }

    public Coordinate2d(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void add(Coordinate2d other) {
        this.setX(this.x + other.getX());
        this.setY(this.y + other.getY());
    }
}
