public class Cup {
    private int capacityOz;
    private int containsOz;

    public Cup() {
        capacityOz = 16;
        containsOz = 0;
    }

    public Cup(int capacityOz) {
        this.capacityOz = capacityOz;
        this.containsOz = 0;
    }

    public void fill(int fillOz) {
        containsOz += fillOz;
        if (containsOz > capacityOz) {
            containsOz = capacityOz;
        }
    }

    public int pour() {
        int prevOz = containsOz;
        containsOz = 0;
        return prevOz;
    }

    public int getCapacityOz() {
        return capacityOz;
    }

    public int getContainsOz() {
        return containsOz;
    }

    public boolean isFull() {
        return containsOz >= capacityOz;
    }

    public boolean isEmpty() {
        return containsOz == 0;
    }

    public String toString() {
        return "Cup (" + containsOz + "/" + capacityOz + "oz)";
    }
}
