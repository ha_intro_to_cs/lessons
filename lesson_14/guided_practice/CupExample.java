public class CupExample {
    public static void main(String[] args) {
        Cup cupA = new Cup();
        System.out.println("This is a default cup: " + cupA);
        System.out.println("It is a " + cupA.getCapacityOz() + "oz cup");
        System.out.println("Is it empty? " + cupA.isEmpty());
        System.out.println("Is it full? " + cupA.isFull());
        System.out.println("Let's fill it with 8oz.");
        cupA.fill(8);
        System.out.println("Here is the cup now: " + cupA);
        System.out.println("Is it empty? " + cupA.isEmpty());
        System.out.println("Is it full? " + cupA.isFull());
        System.out.println("Let's add 8 more ounces");
        cupA.fill(8);
        System.out.println("Here is the cup now: " + cupA);
        System.out.println("Is it empty? " + cupA.isEmpty());
        System.out.println("Is it full? " + cupA.isFull());
        System.out.println("Time to pour it out!");
        int poured = cupA.pour();
        System.out.println("We poured out " + poured + "oz");
        System.out.println("Here is the cup now: " + cupA);
        System.out.println("Is it empty? " + cupA.isEmpty());
        System.out.println("Is it full? " + cupA.isFull());

        System.out.println();
        Cup cupB = new Cup(48);
        System.out.println("This is a bigger cup: " + cupB);
        System.out.println("The cup contains " + cupB.getContainsOz()
            + "oz out of " + cupB.getCapacityOz() + "oz");
        System.out.println("Let's fill it with 20oz.");
        cupB.fill(20);
        System.out.println("The cup contains " + cupB.getContainsOz()
            + "oz out of " + cupB.getCapacityOz() + "oz");
        System.out.println("Let's overfill it with 100 more ounces.");
        cupB.fill(100);
        System.out.println("The cup contains " + cupB.getContainsOz()
            + "oz out of " + cupB.getCapacityOz() + "oz");
        System.out.println("Is it full? " + cupB.isFull());
        System.out.println("Time to pour it out!");
        poured = cupB.pour();
        System.out.println("We poured out " + poured + "oz");
        System.out.println("Here is the cup now: " + cupB);
        System.out.println("Is it empty? " + cupB.isEmpty());
        System.out.println("Is it full? " + cupB.isFull());
        System.out.println("The cup contains " + cupB.getContainsOz()
            + "oz out of " + cupB.getCapacityOz() + "oz");
    }
}
