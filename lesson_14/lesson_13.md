# Lesson 13: Custom classes

Java is an Object Oriented language, which means it is designed around Objects. Not everything can be represented by the types we have discussed so far, you will frequently find the need to create your own objects.

As we have seen, classes define objects that can hold data and operate on it with methods. Let's first look at a simple case and create a class just to hold some data.

## Example 0

Things are about to get tricky. In general, we need to create a file for each class that we make. This isn't strictly required, but Java complains if you don't and it's good practice to make sure your source files don't get super long. Each class should have a matching file name, `Coordinate2d` should be in `Coordinate2d.java` and `Ex0` should be in `Ex0.java` as usual.

~~~java
public class Coordinate2d {
    public double x;
    public double y;
}
~~~

~~~java
public class Ex0 {
    static Coordinate2d getPosition() {
        Coordinate2d position = new Coordinate2d();
        position.x = 10;
        position.y = 5;
        return position;
    }
    public static void main(String[] args) {
        Coordinate2d position = getPosition();
        System.out.println(position.x + " " + position.y);
    }
}
~~~

Notice that we don't have an `import` statement for our class like we've had to import other classes. This is because both classes belong to the same directory and Java automatically finds it.

Next to compile these two files, we can still use the same command we've been using `javac Ex0.java`. Again, Java is smart and automatically finds the `Coordinate2d` class.

The output isn't very interesting.

~~~
> java Ex0
10.0 5.0
~~~

But that's not the point, the point is that we've learned how to build more complex data. This is also an example of how you can return multiple values from a function.

Defining `Coordinate2d` like this is novel, but shouldn't have any big surprises. We already know how to declare a class. We already know that `public` makes things visible outside of the class. The only thing we haven't seen before is declaring variables like this. This type of variable is referred to as a **class member variable**, usually just shorted to **member**, or sometimes **field**.

Hopefully our usage of `Coordinate2d` in `Ex0` shouldn't be surprising either, we can make an instance with the constructor and we can access public members.

What happens if you make the members private? Try it and find out.

---

Example 0 is the simplest case, you just have a named collection of data. This is plenty useful, but we can do even more. In object oriented languages, we want to not just define what the data is, but what we can do with it. Let's look at a more complex example.

## Example 1

~~~java
public class Coordinate2d {
    private double x;
    private double y;

    public Coordinate2d() {
        x = 0;
        y = 0;
    }

    public Coordinate2d(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Coordinate2d add(Coordinate2d other) {
        return new Coordinate2d(this.x + other.getX(), this.y + other.getY());
    }
}
~~~

~~~java
public class Ex1 {
    public static void main(String[] args) {
        Coordinate2d point = new Coordinate2d();
        System.out.println("Point: " + point);
        Coordinate2d offset = new Coordinate2d(10, 5);
        point = point.add(offset);
        System.out.println("Point: " + point);
    }
}
~~~

The output still isn't very impressive on the surface.
~~~
> java Ex1
Point: (0.0, 0.0)
Point: (10.0, 5.0)
~~~

We changed a lot about the `Coordinate2d` class! Firstly, we changed `x` and `y` to `private`, which means that they aren't accessible outside of the class. This is called **encapsulation**, we are hiding the data from the outside world, which lets us dictate exactly what can be done with the data.

This isn't always required, there are plenty of cases, especially with simple data, where it's much easier to leave your data public, but most of the time you want to encapsulate it.

We've also added a bunch of functions to our class. Let's go through each one.

`public Coordinate2d()`

This is a constructor. We've seen them before, we use them to create instances of the class. This is specifically called a **default constructor** because you can create an object with no arguments. Here we initialize `x` and `y` to `0` because that makes sense for coordinates.

`public Coordinate2d(double x, double y)`

This is *also* a constructor. Yes, you can have more than one constructor! This means there can be multiple ways to make a new object. Here, we accept values for `x` and `y` which lets us set them to any value we want.

We need to use `this.x = x` to assign to the member variable because `x = x` would cause Java to assign the argument to itself, which doesn't do anything. `this` is a keyword which refers to the instance this function is being called on.

`public String toString()`

This is a standard function that Java will call automatically to convert your object into a `String` so it can be printed. If you expect your object to be printed it's a good idea to implement this.

`public double getX()`

This function just returns the value of `x`. If you need to do anything with the coordinate, you'll need a way read the value. This type of function is called a **getter**. This helps with encapsulation, we provide a way to read the value of `x`, but no way to write to it. If we wanted to keep `x` private but still write to it, we could make a `public void setX(double x)` function which is called a **setter**.

`public double getY()`

Getter for `y`.

`public Coordinate2d add(Coordinate2d other)`

This is a function that adds two `Coordinate2d`s together, handy if we're doing geometry. Notice that instead of *modifying* the members, we create a new `Coordinate2d`. Because none of the functions modify the members, we say that this class is **immutable**, meaning the data cannot be changed. Classes like `List` and `Map` are called **mutable**.

We also use `this.x` instead of `x`, but this is an aesthetic choice, it's not required. Personally, I like it because it clearly separates `this` and `other`.

Using this class in `Ex1` is straightforward once we understand the functions. It's easy to print the `Coordinate2d`, and we can modify it by reassignment

---

## Principles of OOP

There are several principles that you should keep in mind when working with object oriented design.

* Abstraction

**Abstraction** is the process of representing real world concepts in compute code. Like we represented a 2D point with `Coordinate2d`, you should seek to represent concepts as classes. This will not come easy at first and there isn't a silver bullet to learning it, you just have to practice, get feedback, repeat.

* Single Responsibility

Each class should do one thing, and do it well. Classes should represent one type of data, or manage one resource. For example, a class made to read and write save files should not also manage loading saves into the game or manage a list of saves. Each of those should be their own class.

* Encapsulation

Classes should encapsulate their data and not expose more functionality than is necessary. You should establish a clean interface that exactly describes what you can / should use the class to do.

* More

There are other principles like Inheritance, but they require more instruction that we don't have time for in this lesson.

## Criticisms of OOP

OOP is not universally well-liked, and Java is strongly married to the concept. In Java, OOP is not a choice, other "OOP" languages like Python and C++ are actually quite flexible and allow you to choose your strategy. This is led to several comical criticisms like the following.

https://steve-yegge.blogspot.com/2006/03/execution-in-kingdom-of-nouns.html

Ultimately, this is mostly personal preference. There isn't any empirical data that suggests a specific programming style is faster or less bug-prone.

Every programming language has strengths and weaknesses, it's unlikely that you'll find one that doesn't annoy you in some way. The truth is that programming languages are just tools, and Java is a good enough tool for a lot of jobs.
