<!DOCTYPE html>
<meta charset="UTF-8">
<html>
<head>
    <link href="../common/prism.css" rel="stylesheet" />
    <link rel="stylesheet" href="../common/styles.css">
</head>
<script src="../common/prism.js"></script>
<script src="../common/settings.js"></script>
<body>
<div class="centered" style="max-width: 55em;">

<h1>
    Lesson 8: Loops and Lists
</h1>

<p>
    Programs often need to repeat an action many times. In your first project, you found yourself writing similar code over and over again. This is not much fun and generally bad practice.
</p>

<p>
    Java supports two main ways of making loops.
</p>

<h2>
    <code>for</code> Loops
</h2>

<p>
    The first kind of loop that Java supports is called a "<code>for</code>" loop, this is useful if you want to repeat an action a set number of times, or for each item in some collection. The natural use case is doing something for each item in an array.
</p>

<p>
    When we first learned about the <code>args</code> array, we could see how big it was, but it would have been impractical to print each argument. We would have tried to print every argument up to some value like this.
</p>

<pre class="language-java"><code>
    System.out.println(args[0]);
    System.out.println(args[1]);
    System.out.println(args[2]);
    System.out.println(args[3]);
    ...
</code></pre>

<p>
    Using a <code>for</code> loop, we can do this instead.
</p>

<pre class="language-java"><code>
    for (int index = 0; index < args.length; index++) {
        System.out.println(args[index]);
    }
</code></pre>

<p>
    Here is the syntax for a <code>for</code> loop.
</p>

<pre class="lang-xxx"><code>
    for (initialization statement; boolean expression; update statement) {
        statements;
    }
</code></pre>

<p>
    In the parenthesis, <code>for</code> loops let us give three statements to control the loop. The first statement let's us initialize a variable, the second statement is an expression that controls if the loop will run, and a statement to update a loop condition.
</p>

<p>
    In the example, our initialization is <code>int index = 0;</code>. This is straightforward, we're creating a variable named <code>index</code>. The loop condition is <code>index < args.length</code>, which means that as long as <code>index</code> is less than the length of the arguments, the loop should continue. If the array is empty and <code>length</code> is 0, the loop will not run at all. The last statement is a little confusing because we haven't seen it before. <code>index++</code> is equivalent to <code>index = index + 1</code>, the <code>++</code> operator is called the "increment" operator. There is also the "decrement" operator, <code>--</code>.
</p>

<p>
    All together, this means that we start <code>index</code> at 0, increment it after each loop, and continue looping as long as <code>index</code> is less than <code>args.length</code>.
</p>

<p>
    If you don't need an index, you can alternatively use this syntax.
</p>

<pre class="language-java"><code>
    for (String arg : args) {
        System.out.println(arg);
    }
</code></pre>

<p>
    You can read this as "for arg in args". This skips the index and directly iterators over all items in the collection, this works with lots of list-like objects in Java. In some cases, you really do need the index, as you might need to compare it to something, or you might need to it to access another structure, but most of the time this is simpler and easier to read.
</p>

<h3>
    Exercise 0
</h3>

<p>
    In your Lesson 8 directory, create an Ex0.java to follow along with this exercise. Remember to not copy and paste.
</p>

<pre class="language-java"><code>
    public class Ex0 {
        public static void main(String[] args) {
            for (int index = 0; index < 5; index++) {
                System.out.println("Hey! Listen!");
            }
        }
    }
</code></pre>

<p>
    Before you scroll down and look, take a guess what this program outputs.
</p>

<pre class="lang-xxx"><code>
    > java Ex0
    Hey! Listen!
    Hey! Listen!
    Hey! Listen!
    Hey! Listen!
    Hey! Listen!
</code></pre>

<p>
    This exercise demonstrates that we can use a <code>for</code> loop to simply repeat an action a specific number of times. Not only is this less to type (especially if you need to repeat something many times), it's easier to change your code, since it only needs to change it one place.
</p>

<h3>
    Exercise 1
</h3>

<p>
    Let's look at a less simple case.
</p>

<pre class="language-java"><code>
    public class Ex1 {
        public static void main(String[] args) {
            int m = Integer.parseInt(args[0]);
            System.out.println("Computes y = m * x + 10 for x in [0, 10)");
            for (int x = 0; x < 10; x++) {
                int y = m * x + 10;
                System.out.println(y);
            }
        }
    }

</code></pre>

<p>
    For this exercise, we're going to run the program with the first argument being "1", then "5". Same as Ex0, try to predict what this program will output in both cases before you run it or look at the answer.
</p>

<p>
    If you run this program, with "1" as the first argument, you should get the following output.
</p>

<pre class="lang-xxx"><code>
    > java Ex1 1
    Computes y = m * x + 10 for x in [0, 10)
    10
    11
    12
    13
    14
    15
    16
    17
    18
    19
</code></pre>

<p>
    We can see that the loop runs 10 times, x values from 0 to 9. For each x value, the computer determines <code>1 * x + 10</code>, or simplified, <code>x + 10</code>.
</p>

<p>
    You could use 5 and see this.
</p>

<pre class="lang-xxx"><code>
    > java Ex1 5
    Computes y = m * x + 10 for x in [0, 10)
    10
    15
    20
    25
    30
    35
    40
    45
    50
    55
</code></pre>

<p>
    Now it's computing <code>5 * x + 10</code> for all x values 0 to 9.
</p>

<p>
    Unlike the previous exercise where we just wanted to do something several times, here we are using the index of the loop in a calculation.
</p>

<p>
    This is actually how graphing calculators work! They take each pixel on the screen, convert it to graph coordinates, and evaluate the function for each of those values. (You know they're probably using a for loop because they're doing the same thing <strong>for</strong> each item in a list!)
</p>

<h2>
    <code>while</code> Loops
</h2>

<p>
    <code>while</code> loops are more general. Here is their syntax.
</p>

<pre class="language-xxx"><code>
    while (boolean expression) {
        statements;
    }
</code></pre>

<p>
    This is much simpler, as long as the condition evaluates to <code>true</code>, the loop will keep running. This let's you do more interesting things, like this.
</p>

<pre class="language-java"><code>
    int hp = 100;
    while (hp > 0) {
        // game logic
    }
    System.out.println("Game over");
</code></pre>

<p>
    I mentioned earlier that <code>for</code> loops and <code>while</code> loops are the same. Here's how you could write a <code>for</code> loop with a <code>while</code> loop.

<pre class="language-xxx"><code>
    initialization statement;
    while (boolean expression) {
        statements;
        update statement;
    }
</code></pre>

<p>
    The reverse applies as well, you can just omit the initialization and update statements.
</p>

<pre class="language-xxx"><code>
    for (; boolean expression;) {

    }
</code></pre>

<p>
    If the <code>for</code> loop, we're not required to have all three statements, but we do have to put the semi-colons so that the compiler knows which statement we intend to use.
</p>

<h3>
    Exercise 2
</h3>

<pre class="language-java"><code>
    import java.util.Scanner;

    public class Ex2 {
        public static void main(String[] args) {
            String joke = "Pete and repeat were on a boat. Pete fell off. Who's left?";
            String input = "";
            Scanner in = new Scanner(System.in);

            System.out.println(joke);
            input = in.nextLine();
            while (input.equals("repeat")) {
                System.out.println(joke);
                input = in.nextLine();
            }
            System.out.println("... You're no fun.");
        }
    }

</code></pre>

<p>
    Go ahead and run this program. You should see the joke printed out and the program waiting on you to enter your answer.
</p>

<pre class="lang-xxx"><code>
    > java Ex2
    Pete and repeat were on a boat. Pete fell off. Who's left?


</code></pre>

<p>
    If you type "repeat" and press Enter, the program will enter the loop and tell the joke again. As long as you keep entering "repeat", the joke will repeat over and over and over and over and over and over.......
</p>

<pre class="lang-xxx"><code>
    > java Ex2
    Pete and repeat were on a boat. Pete fell off. Who's left?
    repeat
    Pete and repeat were on a boat. Pete fell off. Who's left?
    repeat
    Pete and repeat were on a boat. Pete fell off. Who's left?
    repeat
    Pete and repeat were on a boat. Pete fell off. Who's left?
    repeat
    Pete and repeat were on a boat. Pete fell off. Who's left?
</code></pre>

<p>
    The program will only exit if we answer something other than "repeat". (Or, of course, if we send ^C.)
</p>

<hr>

<p>
    Even though <code>for</code> and <code>while</code> loops are functionally equivalent, we can see several situations where one is obviously easier to work with than the other. <code>for</code> loops make the most sense when we want to do something a known number of times, and <code>while</code> loops make more sense when it is generally unknown how many times something may need to be done.
</p>

<h2>
    Infinite loops
</h2>

<p>
    You can create programs that never terminate. Sometimes this is on purpose, and sometimes it is a bug.
</p>

<p>
    Here is an example of an infinite <code>while</code> and <code>for</code> loops.
</p>

<pre class="language-java"><code>
    while (true) {
        statements;
    }
</code></pre>

<pre class="language-java"><code>
    for (;;) {
        statements;
    }
</code></pre>

<p>
    A bug might look like this.
</p>

<pre class="language-java"><code>
    Scanner in = new Scanner(System.in);
    int numberOfTries = 0;
    while (numberOfTries < 5) {
        String data = in.nextLine();
        if (data.length < 10) {
            System.out.println("Invalid input");
        }
    }
</code></pre>

<p>
    Here, we forgot to update our <code>numberOfTries</code> counter, so the loop never terminates. Watch out for these kinds of bugs, they are very sneaky.
</p>

<p>
    Side note, it is impossible to determine if any general program terminates or runs forever. Look up the Halting Problem, very interesting reading. It is possible to tell if some programs terminate, but it is impossible to write a general program that can tell if <strong>any</strong> arbitrary program terminates.
</p>

<h2>
    Breaking loops
</h2>

<p>
    Sometimes we need to break out of a loop early. The keywords <code>break</code> and <code>continue</code> help us with that. <code>break</code> will exit the loop entirely regardless of all other conditions. <code>continue</code> will go back to the top of the loop and continue as if that iteration finished normally.
</p>

<h3>
    Exercise 3
</h3>

<p>
    Let's demonstrate the <code>break</code> and <code>continue</code> statements with an exercise. Remember not to copy / paste!
</p>

<pre class="language-java"><code>
    import java.util.Scanner;

    public class Ex3 {
        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            int input;
            while (true) {
                System.out.print("Enter an integer: ");
                String inputString = in.nextLine();
                try {
                    input = Integer.parseInt(inputString);
                }
                catch (NumberFormatException e) {
                    System.out.println("Invalid input: " + inputString);
                    System.out.println("Try again.");
                    continue;
                }
                break;
            }
            System.out.println("You entered: " + input);
        }
    }

</code></pre>

<p>
    This program creates an infinite loop (<code>while (true)</code>) that will repeatedly try to get a valid integer from the user. The they enter something invalid, the program catches a <code>NumberFormatException</code>, informs the user, and thing <code>continue</code>s the loop and tries again. If no exception is generated, we break out of the loop to continue on.
</p>

<pre class="lang-xxx"><code>
    > java Ex3
    Enter an integer: No
    Invalid input: No
    Try again.
    Enter an integer: asdf
    Invalid input: asdf
    Try again.
    Enter an integer: Fifteen
    Invalid input: Fifteen
    Try again.
    Enter an integer: 15.0
    Invalid input: 15.0
    Try again.
    Enter an integer: 15
    You entered: 15
</code></pre>

<p>
    A <code>while</code> loop makes the most sense here since we don't know how many mistakes the user will make, but <code>break</code> and <code>continue</code> both work in <code>for</code> loops too.
</p>

</div>
</body>
</html>
