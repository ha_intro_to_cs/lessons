public class Ex1 {
    public static void main(String[] args) {
        int m = Integer.parseInt(args[0]);
        System.out.println("Computes y = m * x + 10 for x in [0, 10)");
        for (int x = 0; x < 10; x++) {
            int y = m * x + 10;
            System.out.println(y);
        }
    }
}
