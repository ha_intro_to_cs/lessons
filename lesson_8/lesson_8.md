# Lesson 8: Loops

Programs often need to repeat an action many times. In your first project, you found yourself writing similar code over and over again. This is not much fun and generally bad practice.

Java supports two main ways of making loops. We'll learn that they are ultimately equivalent, but sometimes it's a bit cleaner to use one or the other.

## For Loops

The first kind of loop that Java supports is called a "for" loop, this is useful if you want to repeat an action for each item in some collection. The natural use case is doing something for each item in an array.

When we first learned about the `args` array, we could see how big it was, but it would have been impractical to print each argument. We would have tried to print every argument up to some value like this.

~~~java
System.out.println(args[0]);
System.out.println(args[1]);
System.out.println(args[2]);
System.out.println(args[3]);
...
~~~

Using a `for` loop, we can do this instead.

~~~java
for (int index = 0; index < args.length; index++) {
    System.out.println(args[index]);
}
~~~

Here is the syntax for a `for` loop.

~~~
for (initialization statement; conditional expression; update statement) {
    statements;
}
~~~

In the parenthesis, `for` loops let us give three statements to control the loop. The first statement let's us initialize a variable, the second statement is an expression that controls if the loop will run, and a statement to update a loop condition.

In the example, our initialization is `int index = 0;`. This is straightforward, we're creating a variable named `index`. The loop condition is `index < args.length`, which means that as long as `index` is less than the length of the arguments, the loop should continue. If the array is empty and `length` is 0, the loop will not run at all. The last statement is a little confusing because we haven't seen it before. `index++` is equivalent to `index = index + 1`, the `++` operator is called the "increment" operator. There is also the "decrement" operator, `--`.

All together, this means that we start `index` at 0, increment it after each loop, and continue looping as long as `index` is less than `args.length`.

If you don't need an index, you can alternatively use this syntax.

~~~java
for (String arg : args) {
    System.out.println(arg);
}
~~~

You can read this as "for arg in args". This skips the index and directly iterators over all items in the collection, this works with lots of list-like objects in Java. In some cases, you really do need the index, as you might need to compare it to something, or you might need to it to access another structure, but most of the time this is simpler and easier to read.

### Exercise 0

***repeate arg printer with loop***

## While Loops

`while` loops are more general. Here is their syntax.

~~~java
while (conditional expression) {
    statements;
}
~~~

This is much simpler, as long as the condition is true, the loop will keep running. This let's you do more interesting things, like this.

~~~java
int hp = 100;
while (hp > 0) {
    // game logic
}
System.out.println("Game over");
~~~

I mentioned earlier that `for` loops and `while` loops are the same. Here's how you could write a `for` loop with a `while` loop.

~~~java
initialization statement;
while (conditional expression) {
    statements;
    update statement;
}
~~~

The reverse applies as well.

~~~java
for (; conditional expression;) {

}
~~~

If the `for` loop, we're not required to have all three statements, but we do have to put the semi-colons so that the compiler knows which statement we intend to use.

## Infinite loops

You can create programs that never terminate. Sometimes this is on purpose, and sometimes it is a bug.

Here is an example of an infinite `for` and `while` loops.

~~~java
while (true) {
    statements;
}
~~~

~~~java
for (;;) {
    statements;
}
~~~

A bug might look like this.

~~~java
Scanner in = new Scanner(System.in);
int numberOfTries = 0;
while (numberOfTries < 5) {
    String data = in.nextLine();
    if (data.length < 10) {
        System.out.println("Invalid input");
    }
}
~~~

Here, we forgot to update our `numberOfTries` counter, so the loop never terminates. Watch out for these kinds of bugs, they are very sneaky.

Side note, it is impossible to determine if any general program terminates or runs forever. Look up the Halting Problem, very interesting reading. It is possible to tell if some programs terminate, but it is impossible to write a general program that can tell if *any* arbitrary program terminates.

## Breaking loops

Sometimes we need to break out of a loop early. The keywords `break` and continue help us with that. `break` will exit the loop entirely regardless of all other conditions. `continue` will go back to the top of the loop and continue as if that iteration finished normally.
