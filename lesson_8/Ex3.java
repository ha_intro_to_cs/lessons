import java.util.Scanner;

public class Ex3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int input;
        while (true) {
            System.out.print("Enter an integer: ");
            String inputString = in.nextLine();
            try {
                input = Integer.parseInt(inputString);
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid input: " + inputString);
                System.out.println("Try again.");
                continue;
            }
            break;
        }
        System.out.println("You entered: " + input);
    }
}
