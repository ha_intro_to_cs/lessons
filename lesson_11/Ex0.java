public class Ex0 {
    static double exp(double base, int exponent) {
        double result = 1;
        for (int count = 0; count < exponent; count++) {
            result = result * base;
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(exp(Double.parseDouble(args[0]), Integer.parseInt(args[1])));
    }
}
