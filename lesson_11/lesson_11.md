# Lesson 11: Debugging

**Debugging** is the process of removing bugs from a program. It's likely that you've figured it out from context if you didn't already know, but let's define **bug**. A bug is any error that causes a program to behave contrary to expectations.

The term "bug" has been around for a long time, at least since the 1870s, Thomas Edison used it to describe malfunctions. In 1946, a computer operator at Harvard University found a moth trapped in a relay which was causing an error in the computer. The operator noted that this was the "First actual case of bug being found." This story is true (https://commons.wikimedia.org/wiki/File:First_Computer_Bug,_1945.jpg), but it is often misattributed to be the origin of the term "bug".

Debugging is an important tool in a programmer's toolbelt, you almost never will write a program that works on the first try. In my entire experience in college, I did exactly one homework problem right on the first try. You will be debugging a lot, perhaps even more than you will be programming. In fact, as complexity increases, you will spend a small minority of your time actually writing new code, most of your time will be planning and debugging.

## Trace Debugging

Trace Debugging, sometimes called Print Debugging, is a form of debugging that you have likely already discovered. To debug a program, you insert print statements throughtout the program, sometimes to display the value of a variable, sometimes just to check if a certain branch or function is running.

This is fine for quick and hacky debugging, but it falls apart quickly, so we won't dedicate official time to learning this.

## Debuggers

A debugger is a special made program that runs on top of another program to give programmers tools to follow the program and see the data. Using a debugger, you can execute each line of your program one at a time, checking the variables and your data to find where things go wrong. The Java debugger is called `jdb`.

`jdb` comes with the JDK, so it should already be installed. You can verify by running it with the `-version` flag.

~~~
> > jdb -version
This is jdb version 1.8 (Java SE version 1.8.0_251)
~~~

Let's learn the basics through an example.

### Example 0

I'm reusing an old example program, so you can copy / paste here.

~~~java
public class Ex0 {
    static double exp(double base, int exponent) {
        double result = 1;
        for (int count = 0; count < exponent; count++) {
            result = result * base;
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(exp(Double.parseDouble(args[0]), Integer.parseInt(args[1])));
    }
}
~~~

First compile and run the program to make sure it works.

You can use `jdb` just like you use the `java` command to run a program. Try swapping out `jdb` for `java` and see what happens.

~~~
> jdb Ex0 1 0
Initializing jdb ...
> 
~~~

It will be more clear on your terminal, but the second prompt is not the shell prompt, but the `jdb` prompt. `jdb` functions a bit like a shell, it just has less power. To run the program, we use the `run` command.

~~~
> jdb Ex0 1 0
Initializing jdb ...
> run
run Ex0 1 0
Set uncaught java.lang.Throwable
Set deferred uncaught java.lang.Throwable
> 
VM Started: 1.0

The application exited
~~~

`jdb` has exited and our debugging is finished. This isn't very useful, we need to learn some new commands. Restart `jdb`.

~~~
> jdb Ex0 1 0
Initializing jdb ...
> stop at Ex0:12
Deferring breakpoint Ex0:12.
It will be set after the class is loaded.
~~~

The `stop` command sets a **breakpoint** at a specific location. `at` allows us to specify a class:line_number, `in` allows class:function. Usually the line number is easier to use. After setting this breakpoint, let's run the program again.

~~~
> jdb Ex0 1 0
Initializing jdb ...
> stop at Ex0:12
Deferring breakpoint Ex0:12.
It will be set after the class is loaded.
> run
run Ex0 1 0
Set uncaught java.lang.Throwable
Set deferred uncaught java.lang.Throwable
> 
VM Started: Set deferred breakpoint Ex0:12

Breakpoint hit: "thread=main", Ex0.main(), line=12 bci=0
12            System.out.println(exp(Double.parseDouble(args[0]), Integer.parseInt(args[1])));

main[1] 
~~~

The debugger has stopped our program in the middle. It shows us the line that it stopped at. We can use the `list` command for more context.

~~~
main[1] list
8            return result;
9        }
10    
11        public static void main(String[] args) {
12 =>         System.out.println(exp(Double.parseDouble(args[0]), Integer.parseInt(args[1])));
13        }
14    }
main[1] 
~~~

The `print` command will evaluate and print any expression that you give it.

~~~
main[1] print 2 + 2
 2 + 2 = 4
main[1] 
~~~

Nifty! The debugger gives us access to all the variables in scope, so we can do things like this.

~~~
main[1] print args
com.sun.tools.example.debug.expr.ParseException: Name unknown: args
 args = null
main[1] 
~~~

Wait, that's not right... Remember that Java is compiled into bytecode? Well that bytecode doesn't remember the names of your variables by default. It's a waste of space, so the default is to shrink everything down. To tell the compiler to keep all this information, you need to use the `-g` flag when you compile, which stands for "generate all debugging info".

~~~
> javac -g Ex0.java
~~~

Quit the debugger with the `exit` command and recompile with `-g`. Now let's try again.

~~~
main[1] print args
 args = instance of java.lang.String[2] (id=436)
main[1] print args[0]
 args[0] = "1"
main[1] print args[1]
 args[1] = "0"
main[1] 
~~~

That's better!

Everything seems to be in good shape here, so we want to move on to the next line of code. The `step` command will execute this line of code and stop at the next one.

~~~
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=3 bci=0
3            double result = 1;

main[1] 
~~~

You can see we've jumped into the `exp` function on line 3 now. We can look at the arguments to see what we got.

~~~
main[1] print base
 base = 1.0
main[1] print exponent
 exponent = 0
main[1] 
~~~

If we try to print `result`, we'll get an error because we haven't even declared it yet. We need to step again to see that.

~~~
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=4 bci=2
4            for (int count = 0; count < exponent; count++) {

main[1] print result
 result = 1.0
main[1] 
~~~

Now we're on the `for` loop. If we step from here, we end up at the end of the loop, because the loop doesn't run.

~~~
main[1] step 
> 
Step completed: "thread=main", Ex0.exp(), line=8 bci=21
8            return result;

main[1] 
~~~

You won't be able to see `count` because it's initialized and discarded on a single line. We'll come back around with a larger exponent next time.

~~~
main[1] step
> 
Step completed: "thread=main", Ex0.main(), line=12 bci=18
12            System.out.println(exp(Double.parseDouble(args[0]), Integer.parseInt(args[1])));

main[1] 
~~~

After stepping over the `return` line, we're back at the `println`. After jumping to a different part of the program, `jdb` always returns us to where we jumped from before continuing. In this case, there's still more to do, we need to print the value, but even if line 12 was just the `exp` call, we would still get returned there. We can continue like usual.

~~~
main[1] step
> 1.0

Step completed: "thread=main", Ex0.main(), line=13 bci=21
13        }

main[1] step
> 
The application exited
~~~

The next step just prints the value, and the next one ends the program.

Let's run through the program one more time to see the for loop in action. I'll just post my debugger output here for you to follow along with.

~~~
> jdb Ex0 5 3
Initializing jdb ...
> stop at Ex0:12
Deferring breakpoint Ex0:12.
It will be set after the class is loaded.
> run
run Ex0 5 3
Set uncaught java.lang.Throwable
Set deferred uncaught java.lang.Throwable
> 
VM Started: Set deferred breakpoint Ex0:12

Breakpoint hit: "thread=main", Ex0.main(), line=12 bci=0
12            System.out.println(exp(Double.parseDouble(args[0]), Integer.parseInt(args[1])));

main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=3 bci=0
3            double result = 1;

main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=4 bci=2
4            for (int count = 0; count < exponent; count++) {

main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=5 bci=11
5                result = result * base;

main[1] print count
 count = 0
main[1] print base
 base = 5.0
main[1] print exponent
 exponent = 3
main[1] print result
 result = 1.0
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=4 bci=15
4            for (int count = 0; count < exponent; count++) {

main[1] print result
 result = 5.0
main[1] print count
 count = 0
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=5 bci=11
5                result = result * base;

main[1] print count
 count = 1
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=4 bci=15
4            for (int count = 0; count < exponent; count++) {

main[1] print result
 result = 25.0
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=5 bci=11
5                result = result * base;

main[1] print count
 count = 2
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=4 bci=15
4            for (int count = 0; count < exponent; count++) {

main[1] print result
 result = 125.0
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=8 bci=21
8            return result;

main[1] step
> 
Step completed: "thread=main", Ex0.main(), line=12 bci=18
12            System.out.println(exp(Double.parseDouble(args[0]), Integer.parseInt(args[1])));

main[1] step
> 125.0

Step completed: "thread=main", Ex0.main(), line=13 bci=21
13        }

main[1] step
> 
The application exited
~~~

---

There's a few commands that we didn't cover. You can read the full `jdb` docs online, but here's a quick reference.

* `stop at`

Sets a breakpoint at a class and line number.

* `stop in`

Sets a breakpoint at a class method.

* `run`

Runs the program from the start.

* `step`

Executes the current line and moves to the next, entering functions if possible.

* `next`

Executes the current line and moves to the next, automatically evaluating and stepping over any functions. This is useful if you know a function works and don't need to step through it, you can just `next` to the next line.

* `cont`

Continues program execution until it finishes or hits a breakpoint.

* `print`

Prints an expression.

* `exit`

Stops the program and the debugger early.

Full reference: https://docs.oracle.com/javase/7/docs/technotes/tools/solaris/jdb.html

I'll be the first to admit that using the debugger is weird when you first start. It's a powerful tool and you should learn it, even if it's hard. There is a better option that makes debugging easier to follow, and we will learn that next semester.

The best debugging strategy I have to offer you is to slowly go one line at a time. Before you execute each line, explain what *should* happen when you execute the line. Sometimes it helps to speak aloud to another person, or a rubber duck if no one is available. After this explanation, execute the line and then verify that your explanation is accurate. There are very few bugs that will elude you with this strategy.
