<!DOCTYPE html>
<meta charset="UTF-8">
<html>
<head>
    <link href="../common/prism.css" rel="stylesheet" />
    <link rel="stylesheet" href="../common/styles.css">
</head>
<script src="../common/prism.js"></script>
<script src="../common/settings.js"></script>
<body>
<div class="centered" style="max-width: 55em;">

<h1>
    Lesson 11: Debugging
</h1>

<p>
    <strong>Debugging</strong> is the process of removing bugs from a program. It's likely that you've figured it out from context if you didn't already know, but let's define <strong>bug</strong>. A bug is any error that causes a program to behave contrary to expectations.
</p>

<p>
    The term "bug" has been around for a long time, at least since the 1870s, Thomas Edison used it to describe malfunctions. In 1946, a computer operator at Harvard University found a moth trapped in a relay which was causing an error in the computer. The operator noted that this was the "First actual case of bug being found." This story is true (<a href=https://commons.wikimedia.org/wiki/File:First_Computer_Bug,_1945.jpg>proof</a>), but it is often misattributed to be the origin of the term "bug".
</p>

<p>
    Debugging is an important tool in a programmer's toolbelt, you almost never will write a program that works on the first try. In my entire experience in college, I did exactly one homework problem right on the first try. You will be debugging a lot, perhaps even more than you will be programming. In fact, as complexity increases, you will spend a small minority of your time actually writing new code, most of your time will be planning and debugging.
</p>

<h2>
    Trace Debugging
</h2>

<p>
    Trace Debugging, sometimes called Print Debugging, is a form of debugging that you have likely already discovered. To debug a program, you insert print statements throughout the program, sometimes to display the value of a variable, sometimes just to check if a certain branch or function is running.
</p>

<p>
    This is fine for quick and hacky debugging, but it falls apart quickly, so we won't dedicate official time to learning this.
</p>

<h2>
    Debuggers
</h2>

<p>
    A debugger is a special made program that runs on top of another program to give programmers tools to follow the program and see the data. Using a debugger, you can execute each line of your program one at a time, checking the variables and your data to find where things go wrong. The Java debugger is called <code>jdb</code>.
</p>

<p>
    <code>jdb</code> comes with the JDK, so it should already be installed. You can verify by running it with the <code>-version</code> flag.
</p>

<pre class="lang-xxx"><code>
> jdb -version
This is jdb version 1.8 (Java SE version 1.8.0_251)
</code></pre>

<p>
    Let's learn the basics through an example.
</p>

<h3>
    Example 0
</h3>

<p>
    I'm reusing an old example program, so you can copy / paste here.
</p>

<pre class="language-java line-numbers"><code>
public class Ex0 {
    static double exp(double base, int exponent) {
        double result = 1;
        for (int count = 0; count < exponent; count++) {
            result = result * base;
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(exp(Double.parseDouble(args[0]), Integer.parseInt(args[1])));
    }
}
</code></pre>

<p>
    First compile and run the program to make sure it works.
</p>

<p>
    You can use <code>jdb</code> just like you use the <code>java</code> command to run a program. Try swapping out <code>jdb</code> for <code>java</code> and see what happens.
</p>

<pre class="lang-xxx"><code>
> jdb Ex0 1 0
Initializing jdb ...
> 
</pre></code>

<p>
    It will be more clear on your terminal, but the second prompt is not the shell prompt, but the <code>jdb</code> prompt. <code>jdb</code> functions a bit like a shell, it just has less power. To run the program, we use the <code>run</code> command.
</p>

<pre class="lang-xxx"><code>
> jdb Ex0 1 0
Initializing jdb ...
> run
run Ex0 1 0
Set uncaught java.lang.Throwable
Set deferred uncaught java.lang.Throwable
> 
VM Started: 1.0

The application exited
</code></pre>

<p>
    <code>jdb</code> has exited and our debugging is finished. We can see that it output the result after <code>VM Started</code>. (1.0<sup>0</sup> = 1.0) This isn't very useful, we need to learn some new commands. Restart <code>jdb</code>.
</p>

<pre class="lang-xxx"><code>
> jdb Ex0 1 0
Initializing jdb ...
> stop at Ex0:12
Deferring breakpoint Ex0:12.
It will be set after the class is loaded.
</code></pre>

<p>
    The <code>stop</code> command sets a <strong>breakpoint</strong> at a specific location. <code>at</code> allows us to specify a class:line_number, <code>in</code> allows class:function. Usually the line number is easier to use. After setting this breakpoint, let's run the program again.
</p>

<div class="definition">
    <span class="definition-h">Definition</span>
    <p>
        A <strong>breakpoint</strong> is a point within a program at which the debugger will stop if it reaches. A breakpoint is <em>not</em> a part of the code, only the debugger knows about breakpoints.
    </p>
</div>

<pre class="lang-xxx"><code>
> jdb Ex0 1 0
Initializing jdb ...
> stop at Ex0:12
Deferring breakpoint Ex0:12.
It will be set after the class is loaded.
> run
run Ex0 1 0
Set uncaught java.lang.Throwable
Set deferred uncaught java.lang.Throwable
> 
VM Started: Set deferred breakpoint Ex0:12

Breakpoint hit: "thread=main", Ex0.main(), line=12 bci=0
12            System.out.println(exp(Double.parseDouble(args[0]), Integer.parseInt(args[1])));

main[1] 
</code></pre>

<p>
    The debugger has stopped our program in the middle. It shows us the line that it stopped at. We can use the <code>list</code> command for more context.
</p>

<pre class="lang-xxx"><code>
main[1] list
8            return result;
9        }
10    
11        public static void main(String[] args) {
12 =>         System.out.println(exp(Double.parseDouble(args[0]), Integer.parseInt(args[1])));
13        }
14    }
main[1] 
</code></pre>

<p>
    The <code>print</code> command will evaluate and print any expression that you give it.
</p>

<pre class="lang-xxx"><code>
main[1] print 2 + 2
 2 + 2 = 4
main[1] 
</code></pre>

<p>
    Nifty! The debugger gives us access to all the variables in scope, so we can do things like this.
</p>

<pre class="lang-xxx"><code>
main[1] print args
com.sun.tools.example.debug.expr.ParseException: Name unknown: args
 args = null
main[1] 
</code></pre>

<p>
    Wait, that's not right... Remember that Java is compiled into bytecode? Well that bytecode doesn't remember the names of your variables by default. It's a waste of space, so the default is to shrink everything down. To tell the compiler to keep all this information, you need to use the <code>-g</code> flag when you compile, which stands for "generate all debugging info".
</p>

<pre class="lang-xxx"><code>
> javac -g Ex0.java
</code></pre>

<p>
    Quit the debugger with the <code>exit</code> command and recompile with <code>-g</code>. Now let's try again.
</p>

<pre class="lang-xxx"><code>
main[1] print args
 args = instance of java.lang.String[2] (id=436)
main[1] print args[0]
 args[0] = "1"
main[1] print args[1]
 args[1] = "0"
main[1] 
</code></pre>

<p>
    That's better!
</p>

<p>
    Everything seems to be in good shape here, so we want to move on to the next line of code. The <code>step</code> command will execute this line of code and stop at the next one.
</p>

<pre class="lang-xxx"><code>
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=3 bci=0
3            double result = 1;

main[1] 
</code></pre>

<p>
    You can see we've jumped into the <code>exp</code> function on line 3 now. We can look at the arguments to see what we got.
</p>

<pre class="lang-xxx"><code>
main[1] print base
 base = 1.0
main[1] print exponent
 exponent = 0
main[1] 
</code></pre>

<p>
    If we try to print <code>result</code>, we'll get an error because we haven't even declared it yet. We need to step again to see that.
</p>

<pre class="lang-xxx"><code>
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=4 bci=2
4            for (int count = 0; count < exponent; count++) {

main[1] print result
 result = 1.0
main[1] 
</code></pre>

<p>
    Now we're on the <code>for</code> loop. If we step from here, we end up at the end of the loop, because the loop doesn't run.
</p>

<pre class="lang-xxx"><code>
main[1] step 
> 
Step completed: "thread=main", Ex0.exp(), line=8 bci=21
8            return result;

main[1] 
</code></pre>

<p>
    You won't be able to see <code>count</code> because it's initialized and discarded on a single line. We'll come back around with a larger exponent next time.
</p>

<pre class="lang-xxx"><code>
main[1] step
> 
Step completed: "thread=main", Ex0.main(), line=12 bci=18
12            System.out.println(exp(Double.parseDouble(args[0]), Integer.parseInt(args[1])));

main[1] 
</code></pre>

<p>
    After stepping over the <code>return</code> line, we're back at the <code>println</code>. After jumping to a different part of the program, <code>jdb</code> always returns us to where we jumped from before continuing. In this case, there's still more to do, we need to print the value, but even if line 12 was just the <code>exp</code> call, we would still get returned there. We can continue like usual.
</p>

<pre class="lang-xxx"><code>
main[1] step
> 1.0

Step completed: "thread=main", Ex0.main(), line=13 bci=21
13        }

main[1] step
> 
The application exited
</code></pre>

<p>
    The next step just prints the value, and the next one ends the program.
</p>

<p>
    Let's run through the program one more time to see the for loop in action. I'll just post my debugger output here for you to follow along with.
</p>

<pre class="lang-xxx"><code>
> jdb Ex0 5 3
Initializing jdb ...
> stop at Ex0:12
Deferring breakpoint Ex0:12.
It will be set after the class is loaded.
> run
run Ex0 5 3
Set uncaught java.lang.Throwable
Set deferred uncaught java.lang.Throwable
> 
VM Started: Set deferred breakpoint Ex0:12

Breakpoint hit: "thread=main", Ex0.main(), line=12 bci=0
12            System.out.println(exp(Double.parseDouble(args[0]), Integer.parseInt(args[1])));

main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=3 bci=0
3            double result = 1;

main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=4 bci=2
4            for (int count = 0; count < exponent; count++) {

main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=5 bci=11
5                result = result * base;

main[1] print count
 count = 0
main[1] print base
 base = 5.0
main[1] print exponent
 exponent = 3
main[1] print result
 result = 1.0
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=4 bci=15
4            for (int count = 0; count < exponent; count++) {

main[1] print result
 result = 5.0
main[1] print count
 count = 0
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=5 bci=11
5                result = result * base;

main[1] print count
 count = 1
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=4 bci=15
4            for (int count = 0; count < exponent; count++) {

main[1] print result
 result = 25.0
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=5 bci=11
5                result = result * base;

main[1] print count
 count = 2
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=4 bci=15
4            for (int count = 0; count < exponent; count++) {

main[1] print result
 result = 125.0
main[1] step
> 
Step completed: "thread=main", Ex0.exp(), line=8 bci=21
8            return result;

main[1] step
> 
Step completed: "thread=main", Ex0.main(), line=12 bci=18
12            System.out.println(exp(Double.parseDouble(args[0]), Integer.parseInt(args[1])));

main[1] step
> 125.0

Step completed: "thread=main", Ex0.main(), line=13 bci=21
13        }

main[1] step
> 
The application exited
</code></pre>

<hr>

<p>
    There's a few commands that we didn't cover. You can read the full <code>jdb</code> docs online, but here's a quick reference.
</p>

<p>
    <ul>
        <li><code>stop at</code></li>

        <p>
            Sets a breakpoint at a class and line number.
        </p>

        <li><code>stop in</code></li>

        <p>
            Sets a breakpoint at a class method.
        </p>

        <li><code>run</code></li>

        <p>
            Runs the program from the start.
        </p>

        <li><code>step</code></li>

        <p>
            Executes the current line and moves to the next, entering functions if possible.
        </p>

        <li><code>next</code></li>

        <p>
            Executes the current line and moves to the next, automatically evaluating and stepping over any functions. This is useful if you know a function works and don't need to step through it, you can just <code>next</code> to the next line.
        </p>

        <li><code>cont</code></li>

        <p>
            Continues program execution until it finishes or hits a breakpoint.
        </p>

        <li><code>print</code></li>

        <p>
            Prints an expression.
        </p>

        <li><code>exit</code>

        <p>
            Stops the program and the debugger early.
        </p>
    </ul>
</p>

<p>
    <a href=https://docs.oracle.com/javase/7/docs/technotes/tools/solaris/jdb.html>Full reference here</a>.
</p>

<p>
    I'll be the first to admit that using the debugger is weird when you first start. It's a powerful tool and you should learn it, even if it's hard. There is a better option that makes debugging easier to follow, and we will learn that a bit later.
</p>

<p>
    The best debugging strategy I have to offer you is to slowly go one line at a time. Before you execute each line, explain what <em>should</em> happen when you execute the line. Sometimes it helps to speak aloud to another person, or a rubber duck if no one is available. After this explanation, execute the line and then verify that your explanation is accurate. There are very few bugs that will elude you with this strategy.
</p>

</div>
</body>
</html>
