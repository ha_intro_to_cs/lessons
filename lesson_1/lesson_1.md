# Lesson 1: What is a Computer Program?

## Preface

Welcome to your first real lesson in Computer Science! Programming is a lot of fun, but it is a lot of work too. Don't get discouraged! It is okay to take a break and walk away or ask for help.

This course will teach your the basics of computer programming and set you up for whatever field you are interested in. We will focus specifically on video games here, but all the skills learned in the first semester are applicable to any programming discipline.

## What is a Computer Program?

**A computer program is a set of instructions which tell a computer how to do a specific task.** Generally, a program will accept some kind of input from a user, process that data, and produce some kind of output for the user. All video games are programs. They accept input from the player (the controls), they process the input (determine which action should take place), and display the output (the pixels on the screen).

### Exercise 0: Describing Programs.

Open your text editor (Sublime) and create a new text file. If you haven't already, it would be a good idea to create a folder for this course, and a folder for each lesson within that. Save this file as `ex0.txt`.

For each of the following programs, write what input, processing, and output the program uses.

1. The Calculator App
2. Your web browser (Chrome, Firefox, etc)
3. Any program of your choice

Don't worry about getting the answer "right" here, the point is to get you thinking about programs and give us something to talk about in class.

## Input / Output

We are going to learn a new way of doing input and output in this course. Creating windows and text boxes is actually more complex than you would think and dealing with them causes newbies a lot of strife. We are going to learn input and output the way the very first programmers did, via a **Terminal**. As I mentioned, this method is much easier to work with, but it is also still very common today. Many modern programmer-centric programs are made this way.

What is a Terminal you ask?

***image original terminal***

A terminal is a piece of hardware designed for input and output involving a computer. The original terminals paper and ink like a typewriter to send data to and display data from computers. Crazy, right? These old machines are specifically called teletypewriters, or TTY (TeleTYpewriter).

Using a TTY, a user would type information that would be sent to the computer. The program running on the computer would receive and process that information, and then send it back to the TTY which would print the result on the page. This is the simplest method of managing input and output.

Modern computers have fancy video displays and digital keyboards, we don't have physical TTYs anymore. But none of this modern technology makes input and output any easier, so we have **Terminal Emulators**.

***Emulate definition***

Terminal emulators serve the same purpose, they give us a text-based interface to the computer so you can type in data and see results printed back on the screen. This lets programmers keep their simple interfaces in the modern world.

Today, "Terminal Emulator", "TTY", or simply "Terminal" can be used as synonyms in most cases. There are some specific scenarios where a certain term is used in favor of others, but this is mostly for historical reasons and beyond the scope of this course. For the rest of this course, we will say "terminal".

### Shells

Terminals provide us a way to get the input and send the output, but what about processing it? How do we get our program running on the computer in the first place? 

A **shell** is a program that starts with the terminal that is used to start other programs. This might be confusing, why do we need a program to start other programs?

Terminals do a lot less than you might think. When you enter some text into a terminal, how does the computer know what to do? Remember that a program is a set of instructions which tell a computer how to do something. Computers don't do anything without a program. Rather than asking users to configure their computers to start their program and then rebooting, we use a shell that allows users to start many different programs. 

***This probably needs to be reworked***

In this way, shells and operating systems (Windows, macOS, Linux) are similar. An OS lets you start your programs by double clicking on them with a mouse. You can launch many different programs, and install new ones, all using the OS that was already loaded.

In this course, we will learn Windows PowerShell, but what we are learning is general enough to be applied to most other shells. PowerShell is included in Windows, so you know you will always have it.

Here's a short list of some other shells you may run into:

* `sh` (Bourne SHell)
    * The original shell used in Unix
* `bash` (Bourne-Again Shell)
    - Common on Linux, an updated version of `sh`.
* `zsh` (Z SHell)
    * Recently adopted for macOS, also popular on Linux

If you would like to take a deep dive into the differences between a terminal and a shell, there is a good discussion here: https://unix.stackexchange.com/questions/4126/what-is-the-exact-difference-between-a-terminal-a-shell-a-tty-and-a-con.

### Windows Terminal

We will be using Windows Terminal for our terminal. PowerShell does have a terminal that we could use, confusingly named "PowerShell". Yes, the emulator and the shell are named the same, welcome to Windows! =) This terminal is rather old and lacks modern niceties, so we will use the newer one, but everything you learn in this course will work in either terminal.

***image screenshot caption: Windows Terminal is the terminal emulator that handles our input and output, highlight whole window***

***image screenshot caption: PowerShell consumes the input and generates the output, highlight text within window***

## Using PowerShell

Now it's time for the fun stuff, we're going to learn how to use PowerShell. Open Windows Terminal.

At the bottom of the output, you should see a line that looks like this:

~~~
PS C:\Users\smcfalls> 
~~~

***image screenshot, highlight prompt***

This is called a **prompt**, because it is prompting the user for input. `PS` stands for PowerShell, `C:\Users\smcfalls` is the directory that PowerShell is in, called the "Present Working Directory", or `pwd`. The greater than symbol (`>`) is the prompt symbol, which tells you that your input goes here.

***should this be an exercise?***

Type in the following bits of text and observe how the shell responds. When you've finished typing, press Enter, that tells PowerShell that you have finished your command and you want it to be processed. If you make a typo, you can press Backspace to delete the characters and then you can fix your mistake.

**It is important that you actually *type* the command and not copy / paste it.** If you copy and paste it, you will not learn as well. When you type the text yourself, you remember it better. This rule is general, if I ever ask you to do something, assume that copy and pasting is banned unless I specifically make an exception.

1. `ls`

You should see a list of all the files and directories in the PWD. It's likely you'll see "Documents", "Downloads", and things like that.

By entering `ls`, we've asked PowerShell to run a program called `ls`. To run a program, simply enter its name. PowerShell will search for the program on your hard drive and if it finds it, it will run it.

`ls` is short for "list", meaning "list the directory's content". Let's look at some other programs you can run. Programmers are extraordinarily lazy, someone really thought that the speed difference between typing "list" and "ls" was worth the confusion. You'll find many such shortcuts in the programming world.

2. `date`

You should see the current date and time. This program is fairly self-explanatory, unlike `ls`.

3. `ps`

This lists the status of all running programs. `ps` stands for "process status". You will see a lot of programs that you don't care about, but near to the bottom of the list, you should see "WindowsTerminal".

4. `ls Documents`

This command uses the `ls` program, but it lists the `Documents` directory, even though our PWD is different. How does this work?

Programs can have **arguments** passed to them which they get to interpret. This is one way for users to provide information to a program. The `ls` program can list the contents of any directory, but if it's not the current one, you can tell it which directory by providing its path as the first argument.

Let's see some more commands that use arguments.

5. `echo one two`

`echo` is a program that will print all of the arguments back to you, thus the name. Try this out with as many arguments as you like.

6. `echo "Hello World!"`

You if you need to include a space as part of a single argument, you can usually include it in quotes. Here, you see `echo` printing `Hello World!` in one line, because it only sees one argument.

PowerShell creates the arguments that get passed to programs, so you may see differences here depending on the shell you use.

7. `echo "Hello World!" > hello.txt`

We have seen the `>` in the prompt, but it means something different here. In this context, it means we want to redirect the output from the terminal to a file named "hello.txt". That's why you didn't see any output, it got switched to a file instead. Think of it like a train track switching rails.

Open `hello.txt` in Sublime and look at the contents.

8. `cat hello.txt`

`cat` is a funny command. It prints the contents of files, which is why you saw "Hello World!" on the output. But why `cat`? Why not `see`, or `read`, or something like that? `cat` is short for "concatenate", which means "combine". The `cat` program was actually written to combine the contents of multiple files. But if we only ask it to "combine" one file, it just prints it. So there was never a need for a program that just printed one file.

Try creating some more text files (either with `echo` or Sublime) and `cat`ing them all together.

9. `cd Documents`

`cd` stands for "Change Directory", it changes our PWD to the directory specified in the first argument. You can see this reflected in the prompt.

10. `pwd`

The `pwd` command prints the Present Working Directory. This isn't useful if you already have it in the prompt, but not all shells show that, and most can be configured to not show it, so it's good to be able to access this information another way.

***quiz on the above. Not graded, but still good for checking***

### Quitting a program

^C

## Closing thoughts

This isn't a Windows course, so we won't go too in-depth on using PowerShell to operate your computer. As needed, we will cover more programs and features that will be helpful. This is still a useful skill to have, so I encourage you to read up on it.

Outside of these basics, you are going to find differences on Linux and macOS. It is important for many programmers to understand Linux, but that really deserves a course on it's own. I will do my best to teach the Windows way without making it harder to learn Linux if / when you find yourself needing to learn it.

Now that we've seen some simple programs, it's time for us to start making our own!
