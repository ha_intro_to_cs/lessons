<!DOCTYPE html>
<meta charset="UTF-8">
<html>
<head>
    <link href="../common/prism.css" rel="stylesheet" />
    <link rel="stylesheet" href="../common/styles.css">
</head>
<script src="../common/prism.js"></script>
<script src="../common/settings.js"></script>
<body>
<div class="centered" style="max-width: 55em;">

<h1>
    Lesson 1: What is a Computer Program?
</h1>

<h2>
    Preface
</h2>

<p>
    Welcome to your first real lesson in Computer Science! Programming is a lot of fun, but it is a lot of work too. Don't get discouraged! It is okay to take a break and walk away or ask for help.
</p>

<p>
    This course will teach your the basics of computer programming and set you up for whatever field you are interested in. We will focus specifically on video games here, but all the skills learned in the first semester are applicable to any programming discipline.
</p>

<h2>
    What is a Computer Program?
</h2>

<div class="definition">
    <span class="definition-h">Definition</span>
    <p>
        A <strong>computer program</strong> is a set of instructions which tell a computer how to do a specific task.
    </p>
</div>

<p>
    Generally, a program will accept some kind of input from a user, process that data, and produce some kind of output for the user. All video games are programs. They accept input from the player (the controls), they process the input (determine which action should take place), and display the output (the pixels on the screen).
</p>

<h3>
    Exercise 0: Describing Programs.
</h3>

<p>
    Open your text editor (Sublime) and create a new text file. If you haven't already, it would be a good idea to create a folder for this course, and a folder for each lesson within that. Save this file as <code>ex0.txt</code>.
</p>

<p>
    For each of the following programs, write what input, processing, and output the program uses.
</p>

<ol>
    <li>The Calculator App</li>
    <li>Your web browser (Chrome, Firefox, etc)</li>
    <li>Any program of your choice</li>
</ol>

<p>
    Don't worry about getting the answer "right" here, the point is to get you thinking about programs and give us something to talk about in class.
</p>

<h2>
    Input / Output
</h2>

<p>
    We are going to learn a new way of doing input and output in this course. Creating windows and text boxes is actually more complex than you would think and dealing with them causes newbies a lot of strife. We are going to learn input and output the way the very first programmers did, via a <strong>Terminal</strong>. As I mentioned, this method is much easier to work with, but it is also still very common today. Many modern programmer-centric programs are made this way.
</p>

<p>
    What is a Terminal you ask?
</p>

<br>

<div class="definition">
    <span class="definition-h">Definition</span>
    <p>
        A <strong>terminal</strong> is a piece of hardware designed for managing input to and output from a computer.
    </p>
</div>

<p>
    The original terminals paper and ink like a typewriter to send data to and display data from computers. Crazy, right? These old machines are specifically called teletypewriters, or TTY (TeleTYpewriter).
</p>

<div class="center-children">
    <img src="img/IBM_2741_(I197205).png" style="width: 50%;">
</div>
<div class="center-children"><span><em><a href=https://en.wikipedia.org/wiki/IBM_2741>IBM 2741</a> terminal (keyboard/printer)</em></span></div>

<p>
    Using a TTY, a user would type information that would be sent to the computer. The program running on the computer would receive and process that information, and then send it back to the TTY which would print the result on the page. This is the simplest method of managing input and output.
</p>

<p>
    Modern computers have fancy video displays and digital keyboards, we don't have physical TTYs anymore. But none of this modern technology makes input and output any easier, so we have <strong>Terminal Emulators</strong>.
</p>

<div class="definition">
    <span class="definition-h">Definition</span>
    <p>
        <strong>Emulate</strong>: to imitate another program or device
    </p>
    <p>
        (From <a href=https://www.wordnik.com/words/emulate>Wordnik</a>)
    </p>
</div>

<p>
    Terminal emulators serve the same purpose, they give us a text-based interface to the computer so you can type in data and see results printed back on the screen. This lets programmers keep their simple interfaces in the modern world.
</p>

<p>
    Today, "Terminal Emulator", "TTY", or simply "Terminal" can be used as synonyms in most cases. There are some specific scenarios where a certain term is used in favor of others, but this is mostly for historical reasons and beyond the scope of this course. For our purposes, we will say "terminal".
</p>

<h2>
    Shells
</h2>

<p>
    Terminals provide us a way to get the input and send the output, but what about processing it? How do we get our program running on the computer in the first place? 
</p>

<p>
    When you power on a modern computer, the motherboard will automatically load the first program. This is called a <strong>BIOS</strong> (Basic Input/Output System) or a <strong>UEFI</strong> (Unified Extensible Firmware Interface). This program will attempt to load an <strong>Operating System</strong>.
</p>

<div class="definition">
    <span class="definition-h">Definition</span>
    <p>
        An <strong>Operating System (OS)</strong> is a program that controls all the hardware that a computer interfaces with, like the disk, memory, screen, keyboard and mouse.
    </p>
</div>

<p>
    When an Operating System starts, it will usually launch a <strong>shell</strong>.
</p>

<div class="definition">
    <span class="definition-h">Definition</span>
    <p>
        A <strong>shell</strong> is a program connected to a terminal that starts with the OS and is used to start other programs.
    </p>
</div>

<p>
    That's a long chain! We need to go from UEFI to OS to shell just to launch our programs. Why is it so complicated? This is a good question, and I'll try to justify it here, but I understand if this doesn't make sense.
</p>

<p>
    We need a UEFI to automatically launch when the computer receives power. If we didn't have some program automatically start, we'd have to program our computers with voltage probes every time we wanted to turn it on. Of course this is unreasonable.
</p>

<p>
    You could much more reasonably launch your program from this point, but you wouldn't really want to. Every program you make would have to handle your screen, disk, memory, every piece of hardware your computer needed you use, <strong>you</strong> would have to develop. On top of that, you could only ever run one program at a time. If you needed to look up something, you'd have to power down your computer and then load up your web browser to search the web before going back to your program.
</p>

<p>
    An OS handles all this trouble for us. It handles running multiple programs so you can have your web browser, text editor, music, and your custom program all at the same time. It also lets you not worry about pixels and memory and disk sectors and all that nasty stuff.
</p>

<p>
    The shell is just how the OS lets you tell it what to do. A system that controls everything isn't much good without a way to tell it to do anything. the shell is just the way that is exposed.
</p>

<p>
    It <em>is</em> possible that an OS wouldn't include a shell, it could be managed differently, but I'm not aware of any. Windows, Mac, and all the Linux distributions I know of all include shells. I only mention this so that you don't falsely think that shells are required.
</p>

<p>
    OSes usually give us other ways of controlling the system, like a desktop and a cursor, where we can click on programs to run them. You wouldn't be entirely wrong in calling the Windows desktop a shell, but it would confuse a lot of people if you did that, since usually a shell expected to be used with a terminal.
</p>

<p>
    In this course, we will learn Windows PowerShell, but what we are learning is general enough to be applied to most other shells. PowerShell is included in Windows, so you know you will always have it.
</p>

<p>
    Here's a short list of some other shells you may run into:
</p>

<dl>
    <dt><code>sh</code> (Borne SHell)</dt>
    <dd>- The original shell used in Unix.</dd>
    <dt><code>bash</code> (Borne-Again SHell</dt>
    <dd>- Common on Linux, an updated version of <code>sh</code></dd>
    <dt><code>zsh</code> (Z SHell)</dt>
    <dd>- Recently adopted for macOS, also popular on Linux</dd>
</dl>

<p>
    If you would like to take a deep dive into the differences between a terminal and a shell, there is a good discussion <a href=https://unix.stackexchange.com/questions/4126/what-is-the-exact-difference-between-a-terminal-a-shell-a-tty-and-a-con>here</a>.
</p>

<h3>
    Windows Terminal
</h3>

<p>
    We will be using Windows Terminal for our terminal. PowerShell does have a terminal that we could use, confusingly named "PowerShell". Yes, the emulator and the shell are named the same, welcome to Windows! This terminal is rather old and lacks modern conveniences, so we will use the newer one, but everything you learn in this course will work in either terminal.
</p>

<div class="center-children">
    <img src="img/terminal_highlight.png" style="width: 75%;">
</div>
<div class="center-children"><span><em>Windows Terminal is the terminal emulator that handles our input and output.</em></span></div>

<div class="center-children">
    <img src="img/shell_highlight.png" style="width: 75%;">
</div>
<div class="center-children"><span><em>PowerShell consumes the input and generates the output. PS stands for PowerShell.</em></span></div>

<h2>
    Using PowerShell
</h2>

<p>
    Now it's time for the practical stuff, we're going to learn how to use PowerShell.
</p>

<h3>
    Exercise 1
</h3>

<p>
    Open Windows Terminal. At the bottom of the output, you should see a line that looks like this:
</p>

<pre class="lang-xxx"><code>
    PS C:\Users\Sam> 
</code></pre>

<p>
    This is called a <strong>prompt</strong>, because it is prompting the user for input. <code>PS</code> stands for PowerShell, <code>C:\Users\Sam</code> is the directory that PowerShell is in, called the "Present Working Directory", or <code>pwd</code>. The greater than symbol (<code>&gt;</code>) is the prompt symbol, which tells you that your input goes here.
</p>

<p>
    Type in the following bits of text and observe how the shell responds. When you've finished typing, press Enter, that tells PowerShell that you have finished your command and you want it to be processed. If you make a typo, you can press Backspace to delete the characters and then you can fix your mistake.
</p>

<p>
    <strong>It is important that you actually <em>type</em> the command and not copy / paste it.</strong> If you copy and paste it, you will not learn as well. When you type the text yourself, you remember it better. This rule is general, if I ever ask you to do something, assume that copy and pasting is banned unless I specifically make an exception.
</p>

<ol start="1">
    <li><code>ls</code></li>
</ol>

<p>
    You should see a list of all the files and directories in the PWD. On my system, the result looks like this. Yours won't be identical, but it should be similar.
</p>

<pre class="lang-xxx"><code>
    PS C:\Users\Sam> ls


    Directory: C:\Users\Sam


    Mode                 LastWriteTime         Length Name
    ----                 -------------         ------ ----
    d-r---          8/8/2020   4:44 PM                3D Objects
    d-r---          8/8/2020   4:44 PM                Contacts
    d-r---          8/9/2020   8:50 PM                Desktop
    d-r---          8/9/2020   3:45 PM                Documents
    d-r---          8/9/2020   3:00 PM                Downloads
    d-r---          8/8/2020   4:44 PM                Favorites
    d-r---          8/8/2020   4:44 PM                Links
    d-r---          8/8/2020   4:44 PM                Music
    d-r---          8/8/2020   4:47 PM                OneDrive
    d-r---          8/9/2020   3:17 PM                Pictures
    d-r---          8/8/2020   4:44 PM                Saved Games
    d-r---          8/8/2020   4:46 PM                Searches
    d-r---          8/8/2020   4:44 PM                Videos


    PS C:\Users\Sam>
</code></pre>

<p>
    By entering <code>ls</code>, we've asked PowerShell to run a program called <code>ls</code>. <strong>To run a program, simply enter its name.</strong> PowerShell will search for the program on your hard drive and if it finds it, it will run it.
</p>

<p>
    <code>ls</code> is short for "list", meaning "list the directory's content". Let's look at some other programs you can run. Programmers are extraordinarily lazy, someone really thought that the speed difference between typing "list" and "ls" was worth the confusion. You'll find many such shortcuts in the programming world.
</p>

<ol start="2">
    <li><code>date</code></li>
</ol>

<p>
    You should see the current date and time. This program is fairly self-explanatory, unlike <code>ls</code>.
</p>

<ol start="3">
    <li><code>ps</code></li>
</ol>

<p>
    This lists the status of all running programs. <code>ps</code> stands for "process status". You will see a lot of programs that you don't care about, but near to the bottom of the list, you should see "WindowsTerminal".
</p>

<ol start="4">
    <li><code>ls Documents</code></li>
</ol>

<p>
    This command uses the <code>ls</code> program, but it lists the <code>Documents</code> directory, even though our PWD is different. How does this work?
</p>

<p>
    Programs can have <strong>arguments</strong> passed to them which they get to interpret. This is one way for users to provide information to a program. The <code>ls</code> program can list the contents of any directory, but if it's not the current one, you can tell it which directory by providing its path as the first argument.
</p>

<p>
    Let's see some more commands that use arguments.
</p>

<ol start="5">
    <li><code>echo one two</code></li>
</ol>

<p>
    <code>echo</code> is a program that will print all of the arguments back to you, thus the name. Try this out with as many arguments as you like.
</p>

<ol start="6">
    <li><code>echo "Hello World!</code></li>
</ol>

<p>
    If you need to include a space as part of a single argument, you can usually include it in quotes. Here, you see <code>echo</code> printing <code>Hello World!</code> in one line, because it only sees one argument.
</p>

<p>
    PowerShell creates the arguments that get passed to programs, so you may see differences here depending on the shell you use.
</p>

<ol start="7">
    <li><code>echo "Hello World!" > hello.txt</code></li>
</ol>

<p>
    We have seen the <code>></code> in the prompt, but it means something different here. In this context, it means we want to redirect the output from the terminal to a file named "hello.txt". That's why you didn't see any output, it got switched to a file instead. Think of it like a train track switching rails.
</p>

<p>
    Open <code>hello.txt</code> in Sublime and look at the contents.
</p>

<ol start="8">
    <li><code>cat hello.txt</code></li>
</ol>

<p>
    <code>cat</code> is a funny command. It prints the contents of files, which is why you saw "Hello World!" on the output. But why <code>cat</code>? Why not <code>see</code>, or <code>read</code>, or something like that? <code>cat</code> is short for "concatenate", which means "combine". The <code>cat</code> program was actually written to combine the contents of multiple files. But if we only ask it to "combine" one file, it just prints it. So there was never a need for a program that just printed one file.
</p>

<p>
    Try creating some more text files (either with <code>echo</code> or Sublime) and <code>cat</code>'ing them all together.
</p>

<ol start="9">
    <li><code>cd Documents</code></li>
</ol>

<p>
    <code>cd</code> stands for "Change Directory", it changes our PWD to the directory specified in the first argument. You can see this reflected in the prompt.
</p>

<ol start="10">
    <li><code>pwd</code></li>
</ol>

<p>
    The <code>pwd</code> command prints the Present Working Directory. This isn't useful if you already have it in the prompt, but sometimes you want to pass your PWD as an argument to a program
</p>

<h3>
    Quitting programs
</h3>

<p>
    All of the programs we've looked at so far run very quickly, but there are plenty that are long running, and we may want to abort the program without closing the entire terminal. As an example, run this command.
</p>

<p>
    <code>ping -n 20 127.0.0.1</code>
</p>

<p>
    This should take a long time to finish, something like 20 seconds.
</p>

<p>
    Run it again. You can recall a previous command using the up arrow on your keyboard. Before the program terminates, press the Control and C keys at the same time. (Sometimes this is abbreviated to Control-C, or ^C) PowerShell automatically kills the program. Good to know!
</p>

<h2>
    Closing thoughts
</h2>

<p>
    This isn't a Windows course, so we won't go too in-depth on using PowerShell to operate your computer. As needed, we will cover more programs and features that will be helpful. This is still a useful skill to have, so I encourage you to read up on it.
</p>

<p>
    Outside of these basics, you are going to find differences on Linux and macOS. It is important for many programmers to understand Linux, but that really deserves a course on it's own. I will do my best to teach the Windows way without making it harder to learn Linux if / when you find yourself needing to learn it.
</p>

<p>
    Now that we've seen how to use programs in a shell, it's time for us to start making our own!
</p>

</div>
</body>
</html>
