import java.io.UnsupportedEncodingException;

public class Ex3 {
    public static void main(String[] args) throws java.io.UnsupportedEncodingException {
        String simple = "Hello";
        System.out.println(simple);
        System.out.println("Simple \"Character\" Length: " + simple.length());
        System.out.println("Simple Byte Length: " + simple.getBytes("UTF-16").length);

        String japanese = "Hello ネコ";
        System.out.println(japanese);
        System.out.println("Japanese \"Character\" Length: " + japanese.length());
        System.out.println("Japanese Byte Length: " + japanese.getBytes("UTF-16").length);

        String emoji = "Hello 👍";
        System.out.println(emoji);
        System.out.println("Emoji \"Character\" Length: " + emoji.length());
        System.out.println("Emoji Byte Length: " + emoji.getBytes("UTF-16").length);
        System.out.println("Thumbs up code point: " + emoji.codePointAt(6));

        String bigEmoji = "Hello 👍🏻";
        System.out.println(bigEmoji);
        System.out.println("Big Emoji \"Character\" Length: " + bigEmoji.length());
        System.out.println("Big Emoji Byte Length: " + bigEmoji.getBytes("UTF-16").length);
    }
}
