<!DOCTYPE html>
<meta charset="UTF-8">
<html>
<head>
    <link href="../common/prism.css" rel="stylesheet" />
    <link rel="stylesheet" href="../common/styles.css">
</head>
<script src="../common/prism.js"></script>
<script src="../common/settings.js"></script>
<body>
<div class="centered" style="max-width: 55em;">

<h1>
    Lesson 10: Files
</h1>

<p>
    We have already learned a lot about data storage from our discussion on memory, but we're about to learn a lot more as we learn about files on hard disks.
</p>

<h2>
    Accessing the disk
</h2>

<p>
    Accessing memory is easy, CPUs come with instructions to write to / read from memory. Disks are not like this, there is no CPU instruction to read from or write to a disk. Someone has to make a disk and then write a program, called a <strong>driver</strong> to send commands to the disk to make it perform certain actions, much like we send text to standard out.
</p>

<div class="definition">
    <span class="definition-h">Definition</span>
    <p>
        A <strong>driver</strong> is a program that tells a computer how to talk to a piece of hardware.
    </p>
</div>

<p>
    Disks are ubiquitous devices and all modern operating systems include drivers for them. However, different OSes implement things differently, so some details about accessing disks varies between OSes. Java makes our life easy here and handles those differences, but it's important to remember that they do exist, other languages do not make it so easy.
</p>

<p>
    In order to access a file, we need to know the <strong>path</strong> to the file.
</p>

<div class="definition">
    <span class="definition-h">Definition</span>
    <p>
        A <strong>Path</strong> is formatted text which refers to the location of a specific file or directory, for example, <code>C:\Users\smcfalls\Documents\apples.txt</code>.
    </p>
</div>

<p>
    We have been working with paths for a while without formally defining them. On Windows, you can see the path to the currently open directory at the top of File Explorer windows. Java has a special class called <code>Path</code> to help us work with paths. You might think we could just use a <code>String</code>, and that mostly works, but <code>Path</code> exists to formalize what a path is and provides functions that are helpful when working with them.
</p>

<h3>
    Exercise 0
</h3>

<pre class="language-java line-numbers"><code>
    import java.nio.file.Paths;
    import java.nio.file.Path;

    public class Ex0 {
        public static void main(String[] args) {
            Path testPath = Paths.get("test.txt");
            System.out.println(testPath);
            System.out.println("Absolute: " + testPath.isAbsolute());
            Path testAbsolutePath = testPath.toAbsolutePath();
            System.out.println(testAbsolutePath);
            System.out.println("Root: " + testAbsolutePath.getRoot());
            System.out.println("Parent: " + testAbsolutePath.getParent());
        }
    }

</code></pre>

<p>
    To create a <code>Path</code>, we use <code>Paths.get</code>, which takes 1 or more <code>String</code> arguments. When we print out the path, Java implicitly calls <code>toString</code> for us, like it does for integers and other types we've printed.
</p>

<p>
    Paths can be <strong>absolute</strong> or <strong>relative</strong>. Absolute paths are absolutely clear, the start at the file system root, (on Windows, usually C:\), and specify the exact chain of directories to the file. Relative paths are paths that only make sense assuming that they are relative to some specific directory, for example, <code>test.txt</code> would be a valid path if we know we're speaking relative to <code>C:\Users\smcfalls\Documents</code>.
</p>

<p>
    (This is why we can compile our programs with commands like <code>javac Ex0.java</code>. PowerShell assumes that "Ex0.java" is relative to the present working directory, so that's why we <code>cd</code> into the directory containing our code. Otherwise we would have to use long absolute paths every time.)
</p>

<p>
    When we create a path like this, we are creating a path relative to the present working directory. We can use the <code>isAbsoulte</code> function to check this.
</p>

<p>
    Before we proceed, you should verify that you have the proper output. You should see something like this.
</p>

<pre class="lang-xxx"><code>
    > java Ex0
    test.txt
    Absolute: false
    /Users/smcfalls/Documents/Hope Academy/2020/CS/ha_intro_to_cs/lessons/lesson_10/test.txt
    Root: /
    Parent: /Users/smcfalls/Documents/Hope Academy/2020/CS/ha_intro_to_cs/lessons/lesson_10
</code></pre>

<p>
    Now we've confirmed that the path is relative. There's only two options, if it's not absolute, it must be relative.
</p>

<p>
    Relative paths are nice for typing, but that's about it, normally we want to see the full path so we know exactly what's going on. Java provides the <code>toAbsolutePath</code> function which lets us get the absolute path.
</p>

<p>
    Java also gives us <code>getRoot</code> to get the root of the file system and <code>getParent</code> to get the path of the containing directory.
</p>

<p>
    All of these methods are useful, especially if we get the path from a user and we don't know exactly where on the file system the file is.
</p>

<p>
    For a complete reference on Paths, see the documentation.
</p>

<p>
    <a href=https://docs.oracle.com/javase/8/docs/api/java/nio/file/Path.html>Path</a>
</p>

<p>
    <a href=https://docs.oracle.com/javase/8/docs/api/java/nio/file/Paths.html>Paths</a>
</p>

<hr>

<p>
    The <code>Path</code> is just the name of the file. To access the file itself, we need an input or output stream, just like we saw with <code>System.out</code> and <code>System.in</code>.
</p>

<p>
    Let's do another exercise to demonstrate.
</p>

<h3>
    Exercise 1
</h3>

<pre class="language-java line-numbers"><code>
    import java.nio.file.Paths;
    import java.nio.file.Path;
    import java.io.FileWriter;
    import java.io.IOException;

    public class Ex1 {
        public static void main(String[] args) throws IOException {
            Path testPath = Paths.get("test.txt");
            FileWriter testWriter = new FileWriter(testPath.toFile());
            testWriter.write("Hello there, friend" + System.lineSeparator());
            testWriter.write("This is how to write to a text file!" + System.lineSeparator());
            testWriter.close();
        }
    }

</code></pre>

<p>
    Compile and run this program. You can <code>cat test.txt</code> (or open it in Sublime) to see the file contents and verify that it worked.
</p>

<p>
    The first concept I want to point out is on line 10. The <code>Files</code> class offers the <code>Exists</code> function (among others). This is important because when you write to a file, you're destroying the old contents, so sometimes it's a good idea to check before you wipe out a file!
</p>

<p>
    Line 11 introduces the <code>FileWriter</code>. It's used to... (drum-roll please) write to files! Nothing crazy there. To create a <code>FileWriter</code>, we need to give the constructor one argument, the file to write to. We don't use the <code>Path</code> directly, the path to the file and the file itself are two different things! But if we have the path, it's easy to get the file (that's it's job), the <code>toFile</code> function gives us that.
</p>

<p>
    If the file doesn't exist, creating a <code>FileWriter</code> will create the file automatically.
</p>

<p>
    Next, on line 12, we use the <code>write</code> function to write a <code>String</code> to the file. That's simple enough. The only thing that is weird is the <code>System.lineSeparator()</code> function. We add that to the end of a <code>String</code> to create a line break. In the past, we just used <code>System.out.println()</code> if we wanted to include a line break. <code>System.lineSeparator</code> exists because different OSes use different line separators, the function returns the correct string based on the OS.
</p>

<p>
    After we're done writing, we need to close the file (the last line). If we don't do that, some or all of our changes may not actually get saved. Don't forget this important step!
</p>

<p>
    Creating the <code>FileWriter</code>, <code>write</code>, and <code>close</code> can throw <code>IOException</code>s. Java <em>requires</em> us to handle these (actually, most) exceptions. Rather than handling each one with three separate <code>try</code> and <code>catch</code> blocks, we can just add the exception to the method signature (the definition). This means that we're allowing this function to pass along exceptions of that type. In this case, our program just crashes like normal. This is a bit lazy here, we really should handle each case, but for this example, we'd rather the program just crash so we can keep things simple.
</p>

<h3>
    Exercise 2
</h3>

<pre class="language-java line-numbers"><code>
    import java.nio.file.Paths;
    import java.nio.file.Path;
    import java.util.Scanner;
    import java.nio.file.Files;
    import java.io.IOException;

    public class Ex2 {
        public static void main(String[] args) throws IOException {
            Path testPath = Paths.get("test.txt");
            System.out.println("Exists: " + Files.exists(testPath));
            if (!Files.exists(testPath)) {
                System.out.println("The file does not exist!");
                return;
            }

            Scanner testReader = new Scanner(testPath.toFile());
            while (testReader.hasNextLine()) {
                System.out.println(testReader.nextLine());
            }
        }
    }

</code></pre>

<p>
    You should see this if you copied correctly and did Exercise 1 first.
</p>

<pre class="lang-xxx"><code>
    > java Ex2
    Exists: true
    Hello there, friend!
    This is how to write to a text file!
</code></pre>

<p>
    On line 10, we see that we can check if a File exists or not before we try to read from it. The <code>Files.exist</code> function will return <code>true</code> if the path points to a real file, and <code>false</code> if it doesn't.
</p>

<p>
    You can find the complete documentation for <code>Files</code> <a href=https://docs.oracle.com/javase/8/docs/api/java/nio/file/Files.html>here</a>.
</p>

<p>
    In lines 11-14, we see how this is useful. We can give the user a useful error message if the file doesn't exist rather than crashing with a cryptic exception description. We use <code>return</code> on line 13 to exit the program early. If you want, delete <code>test.txt</code> and re-run the program. You should see this:
</p>

<pre class="lang-xxx"><code>
    > java Ex2
    Exists: false
    The file does not exist!
</code></pre>

<p>
    On line 16, we see that we can use a <code>Scanner</code> to read files! This is a friendly sight. In the past, we only used <code>Scanner</code>s to read from the standard input, but here we can use it to read from a file. It provides the same benefits we're used to having, which is a big plus! Just as in Exercise 1, we convert the path to a file and use that to make the <code>Scanner</code>.
</p>

<p>
    The last bit is a simple while loop that prints out every line in the file. We can use the <code>Scanner</code>'s <code>hasNextLine</code> function to determine if another line is available to read. When that returns false, the loop will exit and the program will end.
</p>

<h3>
    Exercise 3
</h3>

<p>
    Let's do one last example to show a more realistic scenario for reading and writing files.
</p>

<pre class="language-java line-numbers"><code>
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;
import java.util.InputMismatchException;

public class Ex3a {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("usage: Ex3 <file> <mode>");
            return;
        }

        Path path = Paths.get(args[0]);

        if (args[1].equals("--write")) {
            System.out.println("Writing to "
                + path.toAbsolutePath().toString());
            Scanner inputScanner = new Scanner(System.in);
            System.out.print("Enter your character's name: ");
            String name = inputScanner.nextLine();
            System.out.print("Enter your character's HP: ");
            double hp = inputScanner.nextDouble();
            System.out.print("Enter your character's attack power: ");
            double attackPower = inputScanner.nextDouble();
            System.out.print(
                "Enter the number of gems your character has collected: ");
            int gems = inputScanner.nextInt();

            try {
                FileWriter fileWriter = new FileWriter(path.toFile());
                fileWriter.write(name);
                fileWriter.write(System.lineSeparator());
                fileWriter.write(String.valueOf(hp));
                fileWriter.write(System.lineSeparator());
                fileWriter.write(String.valueOf(attackPower));
                fileWriter.write(System.lineSeparator());
                fileWriter.write(String.valueOf(gems));
                fileWriter.write(System.lineSeparator());
                fileWriter.close();
            }
            catch (IOException e) {
                System.out.println("Failed to save your file!");
                return;
            }

            System.out.println("Saved");
        }
        else if (args[1].equals("--read")) {
            System.out.println("Reading from "
                + path.toAbsolutePath().toString());
            if (!Files.exists(path)) {
                System.out.println("The file does not exist!");
                return;
            }

            String name;
            double hp;
            double attackPower;
            int gems;
            try {
                Scanner fileScanner = new Scanner(path.toFile());
                name = fileScanner.nextLine();
                hp = fileScanner.nextDouble();
                attackPower = fileScanner.nextDouble();
                gems = fileScanner.nextInt();
            }
            catch (IOException e) {
                System.out.println("Your file cannot be read!");
                return;
            }
            catch (InputMismatchException e) {
                System.out.println(
                    "Your file is formatted improperly and cannot be read!");
                return;
            }

            System.out.println("Here's your character:");
            System.out.println("Name: " + name);
            System.out.println("HP: " + hp);
            System.out.println("Attack Power: " + attackPower);
            System.out.println("Gems: " + gems);
        }
        else {
            System.out.println("Unsupported option");
        }
    }
}

</code></pre>

<p>
    This is a big program, but it doesn't have much new in it.
</p>

<p>
    This program requires two arguments. The first is the file to write to or read from, the second is whether we want to write or read. If we choose to write, we can create a character and it will be saved to the file. If we choose to read, the program will read a saved character and display it.
</p>

<p>
    To test that it's working, I recommend creating a character by running <code>java Ex3 character.sav --write</code> and then reading it out with <code>java Ex3 character.sav --read</code>. You should see the same info that you supplied the first time.
</p>

<p>
    Because of the size of this program, I'm just going to go over the new bits, but <strong>please</strong> ask a question if something doesn't make sense and I don't cover it here.
</p>

<p>
    Firstly, beginning on line 35, we manually convert the <code>double</code> variable into a <code>String</code>. In the past we haven't had to do this. Normally we're just printing it out. Printing is extremely common so Java includes a lot of helpers there. Writing to files is less common, so there's less help for you there. You can't write a <code>double</code> or an <code>int</code> directly to a file, so you need to convert it to <code>String</code> first.
</p>

<p>
    Next, on line 59, note that we declare the variables before assigning to them. If we had declared them inside the <code>try</code> block, they wouldn't be accessible later on when we needed to print them out.
</p>

<p>
    On line 71, we can see that you can actually have multiple <code>catch</code> blocks to catch different kinds of errors! This works just like multiple <code>else if</code>'s. There's actually one more case that isn't handled. Can you look at the docs for <code>Scanner</code> and find what's missing?
</p>

<p>
    Once you're comfortable with how the program works, save a character to a file and open the file in Sublime. Can you edit the file and then read it back to say something different than it was originally saved? It's actually possible to do this for lots of video games...
</p>

<h3>
    Other File Reading / Writing Methods
</h3>

<p>
    Java provides numerous classes that allow you to read and write files. Each one has some different benefits and costs. What I've shown you here is what I believe to be the simplest method. If you search the Internet for "Java write to file", you'll find a number of different options. Feel free to try them out, but make sure you read everything completely, there may be some things you don't understand. Eventually, you may work on a project where those differences cause huge performance differences in your app. In that case, you'll need to be sure to select the right tools for the job.
</p>

<p>
    You shouldn't need anything more than what I've shown you for this class, but I want you to be aware of what's out there if you continue to program beyond this class.
</p>

<h2>
    <code>Path</code> vs <code>String</code>
</h2>

<p>
    Some people tragically use <code>String</code> instead of <code>Path</code>, and you will see this in a lot of code.
</p>

<p>
    <code>String</code> does not encompass all that a file path truly is. Using just a <code>String</code>, it is difficult to write code that runs on multiple OSes. For example, Windows separates directories with <code>\</code>, but UNIX systems use <code>/</code>.
</p>

<p>
    It's also common to see students hacking together code to split a file path or get the parent directory, that kind of code is awfully error prone, especially if you need to support multiple OSes, and it's a waste of time since we already have <code>Path</code> to do the work for us.
</p>

<p>
    The only reason I even tell you about it is because it's so common you will almost certainly need to use someone else's code that accepts just a string. My advice here is to still create a <code>Path</code>, but use the <code>toString</code> method.
</p>

<h2>
    Text Encoding
</h2>

<p>
    We've talked a lot about how computers represent numbers, but not much about text. Now that we have more ability to store text, we should fill in this gap.
</p>

<p>
    The easiest way to represent a character is to cheat and use a number. We can give each character it's own number, <code>0</code> for <code>a</code>, <code>1</code> for <code>b</code>, etc. In the early days, the lawless times, each computer maker had their own encoding. This was terrible, we don't talk about this time anymore. Shortly after, we got everyone to agree to the ASCII standard. This encoding has 128 different characters you can represent. I've included a basic sample of the encoding, but you can read more <a href=https://www.techonthenet.com/ascii/chart.php>here</a>.
</p>

<table>
<thead>
  <tr>
    <th>Code</th>
    <th>Character</th>
    <th>Code</th>
    <th>Character</th>
    <th>Code</th>
    <th>Character</th>
    <th>Code</th>
    <th>Character</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>0</td>
    <td>NUL</td>
    <td>32</td>
    <td>Space</td>
    <td>64</td>
    <td>@</td>
    <td>96</td>
    <td>`</td>
  </tr>
  <tr>
    <td>1</td>
    <td>SOH</td>
    <td>33</td>
    <td>!</td>
    <td>65</td>
    <td>A</td>
    <td>97</td>
    <td>a</td>
  </tr>
  <tr>
    <td>2</td>
    <td>STX</td>
    <td>34</td>
    <td>"</td>
    <td>66</td>
    <td>B</td>
    <td>98</td>
    <td>b</td>
  </tr>
  <tr>
    <td>3</td>
    <td>ETX</td>
    <td>35</td>
    <td>#</td>
    <td>67</td>
    <td>C</td>
    <td>99</td>
    <td>c</td>
  </tr>
  <tr>
    <td>4</td>
    <td>EOT</td>
    <td>36</td>
    <td>$</td>
    <td>68</td>
    <td>D</td>
    <td>100</td>
    <td>d</td>
  </tr>
  <tr>
    <td>5</td>
    <td>ENQ</td>
    <td>37</td>
    <td>%</td>
    <td>69</td>
    <td>E</td>
    <td>101</td>
    <td>e</td>
  </tr>
  <tr>
    <td>6</td>
    <td>ACK</td>
    <td>38</td>
    <td>&amp;</td>
    <td>70</td>
    <td>F</td>
    <td>102</td>
    <td>f</td>
  </tr>
  <tr>
    <td>7</td>
    <td>BEL</td>
    <td>39</td>
    <td>'</td>
    <td>71</td>
    <td>G</td>
    <td>103</td>
    <td>g</td>
  </tr>
  <tr>
    <td>8</td>
    <td>BS</td>
    <td>40</td>
    <td>(</td>
    <td>72</td>
    <td>H</td>
    <td>104</td>
    <td>h</td>
  </tr>
  <tr>
    <td>9</td>
    <td>HT</td>
    <td>41</td>
    <td>)</td>
    <td>73</td>
    <td>I</td>
    <td>105</td>
    <td>i</td>
  </tr>
  <tr>
    <td>10</td>
    <td>LF</td>
    <td>42</td>
    <td>*</td>
    <td>74</td>
    <td>J</td>
    <td>106</td>
    <td>j</td>
  </tr>
  <tr>
    <td>11</td>
    <td>VT</td>
    <td>43</td>
    <td>+</td>
    <td>75</td>
    <td>K</td>
    <td>107</td>
    <td>k</td>
  </tr>
  <tr>
    <td>12</td>
    <td>FF</td>
    <td>44</td>
    <td>,</td>
    <td>76</td>
    <td>L</td>
    <td>108</td>
    <td>l</td>
  </tr>
  <tr>
    <td>13</td>
    <td>CR</td>
    <td>45</td>
    <td>-</td>
    <td>77</td>
    <td>M</td>
    <td>109</td>
    <td>m</td>
  </tr>
  <tr>
    <td>14</td>
    <td>SO</td>
    <td>46</td>
    <td>.</td>
    <td>78</td>
    <td>N</td>
    <td>110</td>
    <td>n</td>
  </tr>
  <tr>
    <td>15</td>
    <td>SI</td>
    <td>47</td>
    <td>/</td>
    <td>79</td>
    <td>O</td>
    <td>111</td>
    <td>o</td>
  </tr>
  <tr>
    <td>16</td>
    <td>DLE</td>
    <td>48</td>
    <td>0</td>
    <td>80</td>
    <td>P</td>
    <td>112</td>
    <td>p</td>
  </tr>
  <tr>
    <td>17</td>
    <td>DC1</td>
    <td>49</td>
    <td>1</td>
    <td>81</td>
    <td>Q</td>
    <td>113</td>
    <td>q</td>
  </tr>
  <tr>
    <td>18</td>
    <td>DC2</td>
    <td>50</td>
    <td>2</td>
    <td>82</td>
    <td>R</td>
    <td>114</td>
    <td>r</td>
  </tr>
  <tr>
    <td>19</td>
    <td>DC3</td>
    <td>51</td>
    <td>3</td>
    <td>83</td>
    <td>S</td>
    <td>115</td>
    <td>s</td>
  </tr>
  <tr>
    <td>20</td>
    <td>DC4</td>
    <td>52</td>
    <td>4</td>
    <td>84</td>
    <td>T</td>
    <td>116</td>
    <td>t</td>
  </tr>
  <tr>
    <td>21</td>
    <td>NAK</td>
    <td>53</td>
    <td>5</td>
    <td>85</td>
    <td>U</td>
    <td>117</td>
    <td>u</td>
  </tr>
  <tr>
    <td>22</td>
    <td>SYN</td>
    <td>54</td>
    <td>6</td>
    <td>86</td>
    <td>V</td>
    <td>118</td>
    <td>v</td>
  </tr>
  <tr>
    <td>23</td>
    <td>ETB</td>
    <td>55</td>
    <td>7</td>
    <td>87</td>
    <td>W</td>
    <td>119</td>
    <td>w</td>
  </tr>
  <tr>
    <td>24</td>
    <td>CAN</td>
    <td>56</td>
    <td>8</td>
    <td>88</td>
    <td>X</td>
    <td>120</td>
    <td>x</td>
  </tr>
  <tr>
    <td>25</td>
    <td>EM</td>
    <td>57</td>
    <td>9</td>
    <td>89</td>
    <td>Y</td>
    <td>121</td>
    <td>y</td>
  </tr>
  <tr>
    <td>26</td>
    <td>SUB</td>
    <td>58</td>
    <td>:</td>
    <td>90</td>
    <td>Z</td>
    <td>122</td>
    <td>z</td>
  </tr>
  <tr>
    <td>27</td>
    <td>ESC</td>
    <td>59</td>
    <td>;</td>
    <td>91</td>
    <td>[</td>
    <td>123</td>
    <td>{</td>
  </tr>
  <tr>
    <td>28</td>
    <td>FS</td>
    <td>60</td>
    <td>&lt;</td>
    <td>92</td>
    <td>\</td>
    <td>124</td>
    <td>|</td>
  </tr>
  <tr>
    <td>29</td>
    <td>GS</td>
    <td>61</td>
    <td>=</td>
    <td>93</td>
    <td>]</td>
    <td>125</td>
    <td>}</td>
  </tr>
  <tr>
    <td>30</td>
    <td>RS</td>
    <td>62</td>
    <td>&gt;</td>
    <td>94</td>
    <td>^</td>
    <td>126</td>
    <td>~</td>
  </tr>
  <tr>
    <td>31</td>
    <td>US</td>
    <td>63</td>
    <td>?</td>
    <td>95</td>
    <td>_</td>
    <td>127</td>
    <td>DEL</td>
  </tr>
</tbody>
</table>

<p>
    This works pretty great for English, there isn't much you can't communicate. Each character fits in a byte, which means we can have up to 256 different characters.That should be more than enough, right?
</p>

<p>
    Wrong. Very wrong. Think about characters like Greek letters, (α, β, γ, δ, ε, ζ, ...), Japanese, (ネコ), Chinese (貓), and more. Computer Scientists have justified almost 150,000 unique characters. Well beyond what can be stored in a single byte.
</p>

<p>
    Today, we have Unicode. Even Unicode has a couple slightly different encodings, UTF-8 is the most common, but Java uses UTF-16. Unicode works the same in concept as ASCII, each character has a special numerical value, the computer handles displaying it properly. The first 128 characters are exactly the same as ASCII, so a program expecting Unicode can handle ASCII input just fine.
</p>

<h3>
    Text Without Encoding Is Meaningless
</h3>

<p>
    This discussion is important because you need to know what encoding you're dealing with before you can do anything meaningful with text. When we're just dealing with text that we generate with Java code, the default is UTF-16, so everything is cool. But when we start reading files which may come from sketchy back alleys, we can't assume anything about the encoding.
</p>

<p>
    If you look at the documentation for <code>newBufferedReader</code> and <code>newBufferedWriter</code>, you will see that they actually accept an additional argument, <code>Charset cs</code>, which allows us to specify the encoding.
</p>

<p>
    I'm not going to make you work with files in different encodings in this class, but it's important to remember this, in the real world, you need to at least be prepared to handle an error if the file isn't encoded like you expect.
</p>

</div>
</body>
</html>
