import java.nio.file.Paths;
import java.nio.file.Path;

public class Ex0 {
    public static void main(String[] args) {
        Path testPath = Paths.get("test.txt");
        System.out.println(testPath);
        System.out.println("Absolute: " + testPath.isAbsolute());
        Path testAbsolutePath = testPath.toAbsolutePath();
        System.out.println(testAbsolutePath);
        System.out.println("Root: " + testAbsolutePath.getRoot());
        System.out.println("Parent: " + testAbsolutePath.getParent());
    }
}
