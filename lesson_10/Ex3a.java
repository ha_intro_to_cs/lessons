import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;
import java.util.InputMismatchException;

public class Ex3a {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("usage: Ex3 <file> <mode>");
            return;
        }

        Path path = Paths.get(args[0]);

        if (args[1].equals("--write")) {
            System.out.println("Writing to "
                + path.toAbsolutePath().toString());
            Scanner inputScanner = new Scanner(System.in);
            System.out.print("Enter your character's name: ");
            String name = inputScanner.nextLine();
            System.out.print("Enter your character's HP: ");
            double hp = inputScanner.nextDouble();
            System.out.print("Enter your character's attack power: ");
            double attackPower = inputScanner.nextDouble();
            System.out.print(
                "Enter the number of gems your character has collected: ");
            int gems = inputScanner.nextInt();

            try {
                FileWriter fileWriter = new FileWriter(path.toFile());
                fileWriter.write(name);
                fileWriter.write(System.lineSeparator());
                fileWriter.write(String.valueOf(hp));
                fileWriter.write(System.lineSeparator());
                fileWriter.write(String.valueOf(attackPower));
                fileWriter.write(System.lineSeparator());
                fileWriter.write(String.valueOf(gems));
                fileWriter.write(System.lineSeparator());
                fileWriter.close();
            }
            catch (IOException e) {
                System.out.println("Failed to save your file!");
                return;
            }

            System.out.println("Saved");
        }
        else if (args[1].equals("--read")) {
            System.out.println("Reading from "
                + path.toAbsolutePath().toString());
            if (!Files.exists(path)) {
                System.out.println("The file does not exist!");
                return;
            }

            String name;
            double hp;
            double attackPower;
            int gems;
            try {
                Scanner fileScanner = new Scanner(path.toFile());
                name = fileScanner.nextLine();
                hp = fileScanner.nextDouble();
                attackPower = fileScanner.nextDouble();
                gems = fileScanner.nextInt();
            }
            catch (IOException e) {
                System.out.println("Your file cannot be read!");
                return;
            }
            catch (InputMismatchException e) {
                System.out.println(
                    "Your file is formatted improperly and cannot be read!");
                return;
            }

            System.out.println("Here's your character:");
            System.out.println("Name: " + name);
            System.out.println("HP: " + hp);
            System.out.println("Attack Power: " + attackPower);
            System.out.println("Gems: " + gems);
        }
        else {
            System.out.println("Unsupported option");
        }
    }
}
