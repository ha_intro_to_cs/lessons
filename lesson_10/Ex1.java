import java.nio.file.Paths;
import java.nio.file.Path;
import java.io.FileWriter;
import java.io.IOException;

public class Ex1 {
    public static void main(String[] args) throws IOException {
        Path testPath = Paths.get("test.txt");
        FileWriter testWriter = new FileWriter(testPath.toFile());
        testWriter.write("Hello there, friend" + System.lineSeparator());
        testWriter.write("This is how to write to a text file!" + System.lineSeparator());
        testWriter.close();
    }
}
