<!DOCTYPE html>
<meta charset="UTF-8">
<html>
<head>
    <link href="../common/prism.css" rel="stylesheet" />
    <link rel="stylesheet" href="../common/styles.css">
</head>
<script src="../common/prism.js"></script>
<script src="../common/settings.js"></script>
<body>
<div class="centered" style="max-width: 55em;">

<h1>
    Appendix A
</h1>

<h2>
    A Character Is Not A Byte
</h2>

<p>
    Because of ASCII and what came before it, the myth that characters and bytes are the same has arisen. This is not true on any level and has not been for decades now. In spite of this, it's likely that you will see college professors and colleagues acting as if the myth is true.
</p>

<p>
    Frequently when processing text, you want to split a string up into characters, either to count them, or replace them, or read them and make some decisions. According the the myth, you can just break the string into bytes and iterate over the bytes. This would work if characters were bytes, but they are not.
</p>

<p>
    So what <em>exactly</em> is a character?
</p>

<p>
    To talk coherently about this, we need to define several terms. I've stolen the following definitions from <a href=https://stackoverflow.com/a/27331885/3287359>here</a>.
</p>

<p>
    > "Character" is an overloaded term than can mean many things.
</p>

<p>
    > A "code point" is the atomic unit of information. "Text" is a sequence of code points. Each code point is a number which is given meaning by the Unicode standard.
</p>

<p>
    > A "code unit" is the unit of storage of a part of an encoded code point. In UTF-8 this means 8-bits, in UTF-16 this means 16-bits. A single code unit may represent a full code point, or part of a code point. For example, the snowman glyph (☃) is a single code point but 3 UTF-8 code units, and 1 UTF-16 code unit.
</p>

<p>
    > A "grapheme" is a sequence of one or more code points that are displayed as a single, graphical unit that a reader recognizes as a single element of the writing system. For example, both a and ä are graphemes, but they may consist of multiple code points (e.g. ä may be two code points, one for the base character a followed by one for the diaresis; but there's also an alternative, legacy, single code point representing this grapheme). Some code points are never part of any grapheme (e.g. the zero-width non-joiner, or directional overrides).
</p>

<p>
    > A "glyph" is an image, usually stored in a font (which is a collection of glyphs), used to represent graphemes or parts thereof. Fonts may compose multiple glyphs into a single representation, for example, if the above ä is a single code point, a font may chose to render that as two separate, spatially overlaid glyphs. For OTF, the font's GSUB and GPOS tables contain substitution and positioning information to make this work. A font may contain multiple alternative glyphs for the same grapheme, too.
</p>

<p>
    When most people say "character", they really mean "grapheme". As you can see in the above definition, a grapheme can be comprised of more than one code points.
</p>

<p>
    Returning to the original problem, we now know that we want to process graphemes instead of characters. How can we do this?
</p>

<p>
    Java is not our friend here, as of version 1.8, Java does not provide a method to deal with graphemes within a <code>String</code>. Java does better than some languages, if we naively break a <code>String</code> into an array of <code>char</code>s, we get an array of UTF-16 code points. This works for a lot of text, but still has two problems.
</p>

<p>
    First, some code points are more than two bytes. If your text has any of those code points, your code will not work correctly if you use <code>char</code>s. Java solves this problem with the <code>String#codePoints</code> and <code>String#codePointAt</code> methods, allowing you to use code points directly.
</p>

<p>
    Secondly, some graphemes are more than one code point. Java <strong>does not</strong> offer a solution to this problem. Swift is the only language I'm aware of that does, most languages do not. If your application needs to non-trivially process Unicode, you will need to find a library created to operate on graphemes.
</p>

<p>
    Let's do an example to cement our understanding of how Java works.
</p>

<h4>
    Exercise 3
</h4>

<p>
    You may copy and paste this program, it's long and there are some characters that are hard to type.
</p>

<pre class="language-java line-numbers"><code>
    import java.io.UnsupportedEncodingException;

    public class Ex3 {
        public static void main(String[] args) throws java.io.UnsupportedEncodingException {
            String simple = "Hello";
            System.out.println(simple);
            System.out.println("Simple \"Character\" Length: " + simple.length());
            System.out.println("Simple Byte Length: " + simple.getBytes("UTF-16").length);

            String japanese = "Hello ネコ";
            System.out.println(japanese);
            System.out.println("Japanese \"Character\" Length: " + japanese.length());
            System.out.println("Japanese Byte Length: " + japanese.getBytes("UTF-16").length);

            String emoji = "Hello 👍";
            System.out.println(emoji);
            System.out.println("Emoji \"Character\" Length: " + emoji.length());
            System.out.println("Emoji Byte Length: " + emoji.getBytes("UTF-16").length);
            System.out.println("Thumbs up code point: " + emoji.codePointAt(6));

            String bigEmoji = "Hello 👍🏻";
            System.out.println(bigEmoji);
            System.out.println("Big Emoji \"Character\" Length: " + bigEmoji.length());
            System.out.println("Big Emoji Byte Length: " + bigEmoji.getBytes("UTF-16").length);
        }
    }

</code></pre>

Here's the output.

<pre class="lang-xxx"><code>
    > java Ex3
    Hello
    Simple "Character" Length: 5
    Simple Byte Length: 12
    Hello ネコ
    Japanese "Character" Length: 8
    Japanese Byte Length: 18
    Hello 👍
    Emoji "Character" Length: 8
    Emoji Byte Length: 18
    Thumbs up code point: 128077
    Hello 👍🏻
    Big Emoji "Character" Length: 10
    Big Emoji Byte Length: 22
</code></pre>

<p>
    The <code>simple</code> <code>String</code> contains 5 graphemes. We can see that the number of <code>chars</code> is 5 as well, so it worked fine here.
</p>

<p>
    The <code>japanese</code> <code>String</code> has 8 graphemes (spaces count), and the <code>char</code> count is 8 as well, so again we're still fine. Many languages would already be broken here.
</p>

<p>
    The <code>emoji</code> <code>String</code> has 7 graphemes, but 8 <code>char</code>s. Now our length measurement is inaccurate which will lead to bugs. Java needs two code units to represent the thumbs up emoji, its code point is <code>128077</code>, but the biggest you can have with 16-bits is <code>65535</code> (2<sup>16</sup> - 1).
</p>

<p>
    The <code>bigEmoji</code> <code>String</code> also has 7 graphemes, but has <em>10</em> <code>char</code>s. Why so many? The <code>👍🏻</code> emoji is actually comprised of two code points, <code>👍</code> + <code>🏻</code>. The numerical values are <code>128077</code>, <code>127995</code>. Both of these code points require 2 code units, so we need 4 <code>char</code>s to represent the one grapheme.
</p>

<p>
    I also included the byte lengths so you can definitely see that bytes are not characters, don't try to make it true.
</p>

<hr>

<p>
    So what's the big deal? If I haven't already convinced you that this is a real problem, let's see an actual bug caused by this.
</p>

<h3>
    Exercise 4
</h3>

<pre class="language-java line-numbers"><code>
    public class Ex4 {
        public static void main(String[] args) {
            String input = args[0];
            char first = input.charAt(0);
            System.out.println(first);
        }
    }

</code></pre>

<p>
    This <em>should</em> be an extremely simple program that just prints out the first character of the <code>String</code> that the user gives us in the first argument.
</p>

<p>
    If we try with a simple <code>String</code>, it works fine.
</p>

<pre class="lang-xxx"><code>
    > java Ex4 test
    t
</code></pre>

<p>
    But if we use emoji, it falls apart.
</p>

<pre class="lang-xxx"><code>
    > java Ex4 👍👍👍
    ?
</code></pre>

<p>
    My computer gave up trying to render the first "character". This bug happened because instead of getting the first grapheme, we are actually getting the first UTF-16 code unit. Because of this, it's trying to render code point <code>55357</code>, which belongs to an unprintable range of characters called <a href=https://unicode-table.com/en/blocks/high-surrogates>High Surrogates</a>.
</p>

<p>
    All it takes for someone to break your app is entering an emoji, which you know is going to happen.
</p>

<hr>

<p>
    Like I said, there is no way to fix this problem without an external library. Since I'm not going to make you use external libraries this semester, an intellectual understanding is all I'm asking for. In this course, we will avoid weird characters, stick to UTF-16, and use naive <code>char</code>-based text processing.
</p>

<p>
    In the real world, if you find yourself creating a text processing program or function, I want you to be aware of how to properly handle graphemes. If you at least consider reaching for a library in this situation, I will be happy with what you learned.
</p>

<h3>
    Further Reading
</h3>

<p>
    Below is a list of articles that informed this section. They're worth reading if you found this interesting.
</p>

<p>
    https://www.joelonsoftware.com/2003/10/08/the-absolute-minimum-every-software-developer-absolutely-positively-must-know-about-unicode-and-character-sets-no-excuses/
</p>

</div>
</body>
</html>
