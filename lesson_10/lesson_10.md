# Lesson 10: Files

We have already learned a lot about data storage from our discussion on memory, but we're about to learn a lot more as we learn about files on hard disks.

## Accessing the disk

Accessing memory is easy, CPUs come with instructions to write to / read from memory. Disks are not like this, there is no CPU instruction to read from or write to a disk. Someone has to make a disk and then write a program, called a **driver** to send commands to the disk to make it perform certain actions, much like we send text to standard out. Drivers are programs that tell computers to talk to hardware.

Disks are ubiquitous devices and all modern operating systems include drivers for them. However, different OSes implement things differently, so some details about accessing disks varies between OSes. Java makes our life easy here and handles those differences, but it's important to remember that they do exist, other languages do not make it so easy.

In order to access a file, we need to know the **path** to the file. On Windows, you can see the path at the top of File Explorer windows. Usually you'll have something like `C:\Users\smcfalls\Documents\apples.txt`. Java has a special class called `Path` to help us work with paths.

### Exercise 0

~~~java
import java.nio.file.Paths;
import java.nio.file.Path;

public class Ex0 {
    public static void main(String[] args) {
        Path testPath = Paths.get("test.txt");
        System.out.println(testPath);
        System.out.println("Absolute: " + testPath.isAbsolute());
        Path testAbsolutePath = testPath.toAbsolutePath();
        System.out.println(testAbsolutePath);
        System.out.println("Root: " + testAbsolutePath.getRoot());
        System.out.println("Parent: " + testAbsolutePath.getParent());
    }
}
~~~

To create a `Path`, we use `Paths.get`, which takes 1 or more `String` arguments. When we print out the path, Java implicitly calls `toString` for us, like it does for integers and other types we've printed.

Paths can be **absolute** or **relative**. Absolute paths are absolutely clear, the start at the file system root, (on Windows, usually C:\\), and specify the exact chain of directories to the file. Relative paths are paths that only make sense assuming that they are relative to some other absolute path, for example, `test.txt` would be a valid path if we know we're speaking relative to `C:\Users\smcfalls\Documents`.

When we create a path like this, we are creating a path relative to the present working directory. We can use the `isAbsoulte` function to check this.

Before we proceed, you should verify that you have the proper output. You should see something like this.

~~~
> java Ex0
test.txt
Absolute: false
/Users/smcfalls/Documents/Hope Academy/2020/CS/ha_intro_to_cs/lessons/lesson_10/test.txt
Root: /
Parent: /Users/smcfalls/Documents/Hope Academy/2020/CS/ha_intro_to_cs/lessons/lesson_10
~~~

Now we've confirmed that the path is relative. There's only two options, if it's not absolute, it must be relative.

Relative paths are nice for typing, but that's about it, normally we want to see the full path so we know exactly what's going on. Java provides the `toAbsolutePath` function which lets us get the absolute path.

Java also gives us `getRoot` to get the root of the file system and `getParent` to get the path of the containing directory.

All of these methods are useful, especially if we get the path from a user and we don't know exactly where on the file system the file is.

For a complete reference on Paths, see the documentation.

https://docs.oracle.com/javase/8/docs/api/java/nio/file/Path.html

https://docs.oracle.com/javase/8/docs/api/java/nio/file/Paths.html

---

The `Path` is just the name of the file. To access the file itself, we need an input or output stream, just like we saw with `System.out` and `System.in`.

Let's do another example to demonstrate.

### Exercise 1

~~~java
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.io.BufferedWriter;
import java.io.IOException;

public class Ex1 {
    public static void main(String[] args) throws IOException {
        Path testPath = Paths.get("test.txt");
        BufferedWriter testWriter = Files.newBufferedWriter(testPath);
        testWriter.write("Hello there, friend!\n");
        testWriter.write("This is how to write to a text file!\n");
        testWriter.flush();
    }
}
~~~

***cover exceptions before this, probably in input***

We can create a new `BufferedWriter` which accesses the file using `Files.newBufferedWriter`. We can write `String`s to the file using the `write` method. The `\n` at the end is a **newline** character. The next section is dedicated to understanding this.

The last line, `flush`, means exactly what it sounds like, it flushes the bytes from the buffer into the file. This is what actually writes the bytes to the file, `write` technically just writes to the buffer. If you leave out this line, the file will not actually be written to.

All of `newBufferedWriter`, `write`, and `flush` can throw `IOException`s. Rather than handling each one, we can just add it to the method signature. This is lazy and generally a bad idea, but it lets us write code quickly.

Navigate to your example directory and open `test.txt` in Sublime, you should be able to see the file contents if you did it right.

### Exercise 2

~~~java
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.io.BufferedReader;
import java.io.IOException;

public class Ex2 {
    public static void main(String[] args) throws IOException {
        Path testPath = Paths.get("test.txt");
        System.out.println("Exists: " + Files.exists(testPath));
        BufferedReader testReader = Files.newBufferedReader(testPath);
        String line;
        while ((line = testReader.readLine()) != null) {
            System.out.println(line);
        }
    }
}
~~~

This example should only have one surprise, line 13. `line = testReader.readLine()` means to read a line from the file and store it in `line`, easy enough. From the documentation, `readLine` will return `null` when there is no more input to read. When we surround that statement with parenthesis, we can turn it into an expression, which has the value of `line`. So when the loop runs, we store a line in `line`, and then compare that to `null`. The loop stops if `line` ever equals `null`, which means we've read everything in the file.

You should see this if you copied correctly and did Exercise 1 first.

~~~
> java Ex2
Exists: true
Hello there, friend!
This is how to write to a text file!
~~~

`Files.exists` lets us check if the file exists before we try to access it. Try to change the path to `test2.txt`, or something else that doesn't exist and see what happens. The program will crash, but we will see `Exists: false`. This means we can handle the error without catching exceptions, which is preferred where possible.

---

You can find the complete documentation for `Files` here: https://docs.oracle.com/javase/8/docs/api/java/nio/file/Files.html

## `Path` vs `String`

Some people tragically use `String` instead of `Path`, and you will see this in a lot of code.

`String` does not encompass all that a file path truly is. Using just a `String`, it is difficult to write code that runs on multiple OSes. For example, Windows separates directories with `\`, but UNIX systems use `/`.

It's also common to see students hacking together code to split a file path or get the parent directory, that kind of code is awfully error prone, especially if you need to support multiple OSes, and it's a waste of time since we already have `Path` to do the work for us.

The only reason I even tell you about it is because it's so common you will almost certainly need to use someone else's code that accepts just a string. My advice here is to still create a `Path`, but use the `toString` method.

## Escape Sequences

In exercise 1, we had some weird characters in the `String` that we wrote to the file. The characters `\n` represent a newline character. This is useful to include characters that a hard to type. The `\` character generally known as a **backslash** (because it leans backwards), but in a `String`, it is called the **escape character**. A sequence of characters following the escape character is called an **escape sequence**. `\n` is a newline, `\t` is a tab, `\"` let's you type a quote symbol inside of a `String`. If you need an actual backslash in your text, there is a special escape sequence for it, `\\`. Fun!

## Text Encoding

We've talked a lot about how computers represent numbers, but not much about text. Now that we have more ability to store text, we should fill in this gap.

The easiest way to represent a character is to cheat and use a number. We can give each character it's own number, `0` for `a`, `1` for `b`, etc. In the early days, the lawless times, each computer maker had their own encoding. This was terrible, we don't talk about this time anymore. Shortly after, we got everyone to agree to the ASCII standard.

***ascii table***

This works pretty great for English, there isn't much you can't communicate. Each character is a byte, which means we can have 256 different characters, which is more than the ASCII standard defined, so we should be fine, right?

Wrong. Very wrong. Think about characters like Greek letters, (α, β, γ, δ, ε, ζ, ...), Japanese, (ネコ), Chinese (貓), and more. Computer Scientists have justified almost 150,000 unique characters. Well beyond what can be stored in a single byte.

Today, we have Unicode. Even Unicode has a couple different encodings, UTF-8 is the most common, but Java uses UTF-16. Unicode works the same in concept as ASCII, each character has a special numerical value, the computer handles displaying it properly.

### Text Without Encoding Is Meaningless

This discussion is important because you need to know what encoding you're dealing with before you can do anything meaningful with text. When we're just dealing with text that we generate with Java code, the default is UTF-16, so everything is cool. But when we start reading files which may come from sketchy back alleys, we can't assume anything about the encoding.

If you look at the documentation for `newBufferedReader` and `newBufferedWriter`, you will see that they actually accept an additional argument, `Charset cs`, which allows us to specify the encoding.

I'm not going to make you work with files in different encodings in this class, but it's important to remember this, in the real world, you need to at least be prepared to handle an error if the file isn't encoded like you expect.

### A Character Is Not A Byte

Because of ASCII and what came before it, the myth that characters and bytes are the same has arisen. This is not true on any level and has not been for decades now. In spite of this, it's likely that you will see college professors and colleagues acting as if the myth is true.

Frequently when processing text, you want to split a string up into characters, either to count them, or replace them, or read them and make some decisions. According the the myth, you can just break the string into bytes and iterate over the bytes. This would work if characters were bytes, but they are not.

***example***

So what *exactly* is a character?

To talk coherently about this, we need to define several terms. I've stolen the following definitions from here: https://stackoverflow.com/a/27331885/3287359

> "Character" is an overloaded term than can mean many things.

> A "code point" is the atomic unit of information. "Text" is a sequence of code points. Each code point is a number which is given meaning by the Unicode standard.

> A "code unit" is the unit of storage of a part of an encoded code point. In UTF-8 this means 8-bits, in UTF-16 this means 16-bits. A single code unit may represent a full code point, or part of a code point. For example, the snowman glyph (☃) is a single code point but 3 UTF-8 code units, and 1 UTF-16 code unit.

> A "grapheme" is a sequence of one or more code points that are displayed as a single, graphical unit that a reader recognizes as a single element of the writing system. For example, both a and ä are graphemes, but they may consist of multiple code points (e.g. ä may be two code points, one for the base character a followed by one for the diaresis; but there's also an alternative, legacy, single code point representing this grapheme). Some code points are never part of any grapheme (e.g. the zero-width non-joiner, or directional overrides).

> A "glyph" is an image, usually stored in a font (which is a collection of glyphs), used to represent graphemes or parts thereof. Fonts may compose multiple glyphs into a single representation, for example, if the above ä is a single code point, a font may chose to render that as two separate, spatially overlaid glyphs. For OTF, the font's GSUB and GPOS tables contain substitution and positioning information to make this work. A font may contain multiple alternative glyphs for the same grapheme, too.

When most people say "character", they really mean "grapheme". As you can see in the above definition, a grapheme can be comprised of more than one code points.

Returning to the original problem, we now know that we want to process graphemes instead of characters. How can we do this?

Java is not our friend here, as of version 1.8, Java does not provide a method to deal with graphemes within a `String`. Java does better than some languages, if we naively break a `String` into an array of `char`s, we get an array of UTF-16 code points. This works for a lot of text, but still has two problems.

First, some code points are more than two bytes. If your text has any of those code points, your code will not work correctly if you use `char`s. Java solves this problem with the `String#codePoints` and `String#codePointAt` methods, allowing you to use code points directly.

Secondly, some graphemes are more than one code point. Java **does not** offer a solution to this problem. Swift is the only language I'm aware of that does, most languages do not. If your application needs to non-trivially process Unicode, you will need to find a library created to operate on graphemes.

Let's do an example to cement our understanding of how Java works.

#### Exercise 3

You may copy and paste this program, it's long and there are some characters that are hard to type.

~~~java
import java.io.UnsupportedEncodingException;

public class Ex3 {
    public static void main(String[] args) throws java.io.UnsupportedEncodingException {
        String simple = "Hello";
        System.out.println(simple);
        System.out.println("Simple \"Character\" Length: " + simple.length());
        System.out.println("Simple Byte Length: " + simple.getBytes("UTF-16").length);

        String japanese = "Hello ネコ";
        System.out.println(japanese);
        System.out.println("Japanese \"Character\" Length: " + japanese.length());
        System.out.println("Japanese Byte Length: " + japanese.getBytes("UTF-16").length);

        String emoji = "Hello 👍";
        System.out.println(emoji);
        System.out.println("Emoji \"Character\" Length: " + emoji.length());
        System.out.println("Emoji Byte Length: " + emoji.getBytes("UTF-16").length);
        System.out.println("Thumbs up code point: " + emoji.codePointAt(6));

        String bigEmoji = "Hello 👍🏻";
        System.out.println(bigEmoji);
        System.out.println("Big Emoji \"Character\" Length: " + bigEmoji.length());
        System.out.println("Big Emoji Byte Length: " + bigEmoji.getBytes("UTF-16").length);
    }
}
~~~

Here's the output.

~~~
> java Ex3
Hello
Simple "Character" Length: 5
Simple Byte Length: 12
Hello ネコ
Japanese "Character" Length: 8
Japanese Byte Length: 18
Hello 👍
Emoji "Character" Length: 8
Emoji Byte Length: 18
Thumbs up code point: 128077
Hello 👍🏻
Big Emoji "Character" Length: 10
Big Emoji Byte Length: 22
~~~

The `simple` `String` contains 5 graphemes. We can see that the number of `chars` is 5 as well, so it worked fine here.

The `japanese` `String` has 8 graphemes (spaces count), and the `char` count is 8 as well, so again we're still fine. Many languages would already be broken here.

The `emoji` `String` has 7 graphemes, but 8 `char`s. Now our length measurement is inaccurate which will lead to bugs. Java needs two code units to represent the thumbs up emoji, its code point is `128077`, but the biggest you can have with 16-bits is `65535` (2 ^ 16 - 1).

The `bigEmoji` `String` also has 7 graphemes, but has *10* `char`s. Why so many? The `👍🏻` emoji is actually comprised of two code points, `👍` + `🏻`. The numerical values are `128077`, `127995`. Both of these code points require 2 code units, so we need 4 `char`s to represent the one grapheme.

I also included the byte lengths so you can definitely see that bytes are not characters, don't try to make it true.

---

So what's the big deal? If I haven't already convinced you that this is a real problem, let's see an actual bug caused by this.

### Exercise 4

~~~java
public class Ex4 {
    public static void main(String[] args) {
        String input = args[0];
        char first = input.charAt(0);
        System.out.println(first);
    }
}
~~~

This *should* be an extremely simple program that just prints out the first character of the `String` that the user gives us in the first argument.

If we try with a simple `String`, it works fine.

~~~
> java Ex4 test
t
~~~

But if we use emoji, it falls apart.

~~~
> java Ex4 👍👍👍
?
~~~

My computer gave up trying to render the first "character". This bug happened because instead of getting the first grapheme, we are actually getting the first UTF-16 code unit. Because of this, it's trying to render code point `55357`, which belongs to an unprintable range of characters called High Surrogates (https://unicode-table.com/en/blocks/high-surrogates/)

All it takes for someone to break your app is entering an emoji, which you know is going to happen.

---

Like I said, there is no way to fix this problem without an external library. Since I'm not going to make you use external libraries this semester, an intellectual understanding is all I'm asking for. In this course, we will avoid weird characters, stick to UTF-16, and use naive `char`-based text processing.

In the real world, if you find yourself creating a text processing program or function, I want you to be aware of how to properly handle graphemes. If you at least consider reaching for a library in this situation, I will be happy with what you learned.

### Further Reading

Below is a list of articles that informed this section. They're worth reading if you found this interesting.

https://www.joelonsoftware.com/2003/10/08/the-absolute-minimum-every-software-developer-absolutely-positively-must-know-about-unicode-and-character-sets-no-excuses/
