import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.Scanner;
import java.nio.file.Files;
import java.io.IOException;

public class Ex2 {
    public static void main(String[] args) throws IOException {
        Path testPath = Paths.get("test.txt");
        System.out.println("Exists: " + Files.exists(testPath));
        if (!Files.exists(testPath)) {
            System.out.println("The file does not exist!");
            return;
        }

        Scanner fileScanner = new Scanner(testPath.toFile());
        while (fileScanner.hasNextLine()) {
            System.out.println(fileScanner.nextLine());
        }
    }
}
