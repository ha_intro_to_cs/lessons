<!DOCTYPE html>
<meta charset="UTF-8">
<html>
<head>
    <link href="../common/prism.css" rel="stylesheet" />
    <link rel="stylesheet" href="../common/styles.css">
</head>
<script src="../common/prism.js"></script>
<script src="../common/settings.js"></script>
<body>
<div class="centered" style="max-width: 55em;">

<h1>
    Lesson 7: Git
</h1>

<p>
    All of the assignments so far are been short and straightforward. Project 1 is not so, it is much more complex, you probably won't be able to do it in one sitting. You probably will accidentally break things that used to work. You will run into this more and more the longer you program.
</p>

<p>
    Linus Torvalds created Git to help deal with these problems. Git is a kind of Version Control System, or VCS. It allows you to save a version of your program in a specific state so that if you mess up, you can roll back to a known good state.
</p>

<p>
    Git is also a staple of modern software development, anything you do with computer programming is likely to heavily rely on it. Unfortunately, many college students graduate without ever learning about it, which is really a tragedy. Learning Git will make your life as a student much easier as well as prepare you for the real world.
</p>

<p>
    If this lesson goes over your head, don't worry about it, I'm not going to require you to use Git and there's no homework on this. If you find that you are really enjoying programming and you have spare brain power, this is worth your time to understand. If not, you can just skip it.
</p>

<h2>
    Philosophy
</h2>

<p>
    Git stores "snapshots" of your project files called <strong>commits</strong>.
</p>

<div class="definition">
    <span class="definition-h">Definition</span>
    <p>
        A Git <strong>commit</strong> is a complete copy of your project files as they were at a certain point in time.
    </p>
</div>

<div class="center-children">
    <img src="img/git_base.svg" style="width: 75%;">
</div>
<div class="center-children"><span><em>Example Git commits</em></span></div>

<div class="center-children">
    <img src="img/git_add.svg" style="width: 100%;">
</div>
<div class="center-children"><span><em>You can make new changes</em></span></div>

<div class="center-children">
    <img src="img/git_remove.svg" style="width: 100%;">
</div>
<div class="center-children"><span><em>...and then abandon them</em></span></div>

<div class="center-children">
    <img src="img/git_branch.svg" style="width: 75%;">
</div>
<div class="center-children"><span><em>You can also <strong>branch</strong> off to test changes you think are risky.</em></span></div>

<p>
    The main branch is referred to as the <strong>master</strong> branch.
</p>

<h2>
    Installing Git
</h2>

<p>
    Visit the <a href=https://git-scm.com/downloads>Git homepage</a> and download the installer for your system. 
</p>

<p>
    Follow the steps and refer to the screenshots.
</p>

<p>
    <ol>
        <li>Click "Next" to begin.</li>
        <li>Select the install location (default should be fine).</li>
        <li>Select the components to install. Default should be Git LFS, Associate .git, Associate .sh, which is fine.</li>
        <li>Click "Next" to create a Start Menu folder.</li>
        <li>Choose Sublime Text as your default editor.</li>
        <li>Select the second option to add Git to your PATH.</li>
        <li>Select the first option to use OpenSSL.</li>
        <li>Select the third option to not mess with line endings.</li>
        <li>Select the first option to use MinTTY</li>
        <li>Select the first option to use the default pull configuration.</li>
        <li>Accept the default option for credential management (this doesn't really matter).</li>
        <li>Make sure the first box is checked to allow file system caching.</li>
        <li>Do not check the box for any experimental options. Click "Install".</li>
        <li>Click "Next" to finish.</li>
        <li>
            Open a Terminal and use the following commands to set your user name and email address (obviously insert your own instead of mine). The last command may not be necessary, but it probably won't hurt.

            <pre class="lang-xxx"><code>
                > git config --global user.name "Sam McFalls"
                > git config --global user.email "samuel.mcfalls@hopeacademyhbc.org"
                > git config core.editor
                "C:\\Program Files\\Sublime Text 3\\subl.exe" -w
            </code></pre>
        </li>
    </ol>
</p>

<div style="display: flex;">
    <img src="img/git_install_1.png" alt="Step 1" style="flex: 50%; padding: 5px;">
    <img src="img/git_install_2.png" alt="Step 2" style="flex: 50%; padding: 5px;">
</div>

<div style="display: flex;">
    <img src="img/git_install_3.png" alt="Step 1" style="flex: 50%; padding: 5px;">
    <img src="img/git_install_4.png" alt="Step 2" style="flex: 50%; padding: 5px;">
</div>

<div style="display: flex;">
    <img src="img/git_install_5a.png" alt="Step 1" style="flex: 50%; padding: 5px;">
    <img src="img/git_install_5.png" alt="Step 2" style="flex: 50%; padding: 5px;">
</div>

<div style="display: flex;">
    <img src="img/git_install_6.png" alt="Step 1" style="flex: 50%; padding: 5px;">
    <img src="img/git_install_7.png" alt="Step 2" style="flex: 50%; padding: 5px;">
</div>

<div style="display: flex;">
    <img src="img/git_install_8.png" alt="Step 1" style="flex: 50%; padding: 5px;">
    <img src="img/git_install_9.png" alt="Step 2" style="flex: 50%; padding: 5px;">
</div>

<div style="display: flex;">
    <img src="img/git_install_10.png" alt="Step 1" style="flex: 50%; padding: 5px;">
    <img src="img/git_install_11.png" alt="Step 2" style="flex: 50%; padding: 5px;">
</div>

<div style="display: flex;">
    <img src="img/git_install_12.png" alt="Step 1" style="flex: 50%; padding: 5px;">
    <img src="img/git_install_13.png" alt="Step 2" style="flex: 50%; padding: 5px;">
</div>

<div style="display: flex;">
    <img src="img/git_install_14.png" alt="Step 1" style="flex: 50%; padding: 5px;">
    <img src="img/git_install_15.png" alt="Step 2" style="flex: 50%; padding: 5px;">
</div>

<h2>
    Exercise 0: Using Git
</h2>

<p>
    Make a new directory, <code>lesson_7/ex0</code>. Create a simple hello world program named <code>Ex0.java</code>.
</p>

<p>
    In your terminal, change directories to your <code>ex0</code> directory. To make a new Git repository, type <code>git init</code>. This is short for "initialize". You should see something like this.
</p>

<pre class="lang-xxx"><code>
    > git init
    Initialized empty Git repository in C:/Users/Sam/Documents/ha_intro_to_cs/lessons/lesson_7/ex0/.git/
</code></pre>

<p>
    List the directory with <code>ls -Force</code>. You should see a new directory called <code>.git</code>. Git keeps all of its special information here, the commits, branches, etc.
</p>

<div class="note">
    <span class="note-h">
        Note
    </span>
    <p>
        The <code>-Force</code> option is a Windows only option that will show files and directories that are normally hidden. On Linux and macOS you would use <code>-a</code> (for "all"). Windows loves to be different.
    </p>
</div>

<p>
    The most common git command you will use is <code>git status</code>, try it out now. It will tell you what is going on in the repository.
</p>

<pre class="lang-xxx"><code>
> git status
On branch master

No commits yet

Untracked files:
  (use "git add &lt;file&gt;..." to include in what will be committed)

    Ex0.java

nothing added to commit but untracked files present (use "git add" to track)
</code></pre>

<p>
    There's already a lot to break down, but it's not complicated.
</p>

<p>
    <code>On branch master</code>
</p>

<p>
    The default branch is called <code>master</code>. That's the branch we start on.
</p>

<p>
    <code>No commits yet</code>
</p>

<p>
    This shouldn't be surprising, we just created the repository.
</p>

<pre class="lang-xxx"><code>
Untracked files:
  (use "git add &lt;file&gt;..." to include in what will be committed)

    Ex0.java
</code></pre>

<p>
    This is pretty straightforward, Git sees a file that is "untracked", meaning that Git is not tracking any changes you make to that file.
</p>

<p>
    <code>nothing added to commit but untracked files present (use "git add" to track)</code>
</p>

<p>
    Also simple, git helps us out and tells us how to track a file. Let's try it out.
</p>

<pre class="lang-xxx"><code>
> $ git add Ex0.java
>
</code></pre>

<p>
    No output means that we didn't have any errors. Let's check the status again to see where we're at.
</p>

<pre class="lang-xxx"><code>
> git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached &lt;file&gt;..." to unstage)

    new file:   Ex0.java
</code></pre>

<p>
    The only difference is the bottom section, "Changes to be committed". When we added the file, we have <strong>staged</strong> it. We can stage multiple files to be added to a single commit. We only have one file, so we're ready to make a commit.
</p>

<pre class="lang-xxx"><code>
    > git commit
</code></pre>

<p>
    Git opens a new sublime text window (or tab) that lets us write a message that goes with our commit. This can be any text, but it's usually a good idea to write a summary line, followed by an empty line, followed by an in depth explanation if one is necessary. Let's write a message like this:
</p>

<pre class="lang-xxx"><code>
    Add Ex0.java

    Added first Java file
</code></pre>

<p>
    Save the file and close the sublime window (or tab). After that, the commit will finish. You should see output similar to this.
</p>

<pre class="lang-xxx"><code>
    $ git commit
    [master (root-commit) a76b8e6] Add Ex0.java
     1 file changed, 5 insertions(+)
     create mode 100644 Ex0.java
</code></pre>

<p>
    We can check <code>git status</code> again to see what we've done.
</p>

<pre class="lang-xxx"><code>
    > git status
    On branch master
    nothing to commit, working tree clean
</code></pre>

<p>
    This is good, we have no staged or unstaged changes.
</p>

<p>
    To see our commit history, we use <code>git log</code>.
</p>

<pre class="lang-xxx"><code>
$ git log
commit a76b8e63c4bbf5cea40cb9db3be6c82fc81ab407 (HEAD -> master)
Author: Sam McFalls &lt;samuel.mcfalls@hopeacademyhbc.org&gt;
Date:   Tue Jul 14 21:17:34 2020 -0400

    Add Ex0.java
    
    Added first Java file
</code></pre>

<p>
    We only have one commit so the entire history fits on our screen. If it doesn't, you can use k to scroll down, j to scroll up, and q to exit.
</p>

<p>
    Each commit is named with a <strong>hash</strong>, shown at the top of each commit description. Each commit will have a unique hash, mine was <code>5cea40cb9db3be6c82fc81ab407</code>, your's will be different.
</p>

<div class="definition">
    <span class="definition-h">Definition</span>
    <p>
        A commit <strong>hash</strong> is a simplified representation of a commit. It is generated based on the commit's contents and is very likely to be unique. They can be used to refer to a specific commit easily.
    </p>
</div>

<p>
    Just to check our understanding, let's make an edit to our file and add that as a new commit.
</p>

<p>
    Let's add another line of output, it doesn't matter what. Then we can look at the status.
</p>

<pre class="lang-xxx"><code>
> git status
On branch master
Changes not staged for commit:
  (use "git add &lt;file&gt;..." to update what will be committed)
  (use "git checkout -- &lt;file&gt;..." to discard changes in working directory)

    modified:   Ex0.java

no changes added to commit (use "git add" and/or "git commit -a")
</code></pre>

<p>
    This should largely make sense.
</p>

<p>
    We can see details of our change using <code>git diff</code>.
</p>

<pre class="lang-xxx"><code>
> git diff
diff --git a/Ex0.java b/Ex0.java
index 4e0fb07..3beec32 100644
--- a/Ex0.java
+++ b/Ex0.java
@@ -1,5 +1,6 @@
 public class Ex0 {
     public static void main(String[] args) {
         System.out.println("Hello World");
+        System.out.println("Meaningless");
     }
 }
</code></pre>

<p>
    We can see the new line we added with the <code>+</code> notation.
</p>

<p>
    Let's add the file.
</p>

<pre class="lang-xxx"><code>
> git add Ex0.java 
> git status
On branch master
Changes to be committed:
  (use "git reset HEAD &lt;file&gt;..." to unstage)

    modified:   Ex0.java
</code></pre>

<p>
    This should also make sense.
</p>

<p>
    We can commit the file like normal. The message doesn't matter.
</p>

<pre class="lang-xxx"><code>
> git commit
[master 1fc5e84] Add new output
 1 file changed, 1 insertion(+)
</code></pre>

<p>
    We can check the history to see our new commit.
</p>

<pre class="lang-xxx"><code>
> git log
commit 1fc5e84b40850fa82058068a997ed8dad7c1aad6 (HEAD -> master)
Author: Sam McFalls &lt;samuel.mcfalls@hopeacademyhbc.org&gt;
Date:   Tue Jul 14 21:38:23 2020 -0400

    Add new output

commit a76b8e63c4bbf5cea40cb9db3be6c82fc81ab407
Author: Sam McFalls &lt;samuel.mcfalls@hopeacademyhbc.org&gt;
Date:   Tue Jul 14 21:17:34 2020 -0400

    Add Ex0.java
    
    Added first Java file
</code></pre>

<p>
    We can see the previous commit is of course still in the history.
</p>

<p>
    This is great for documentation, but otherwise this has mostly just been busywork. We really want the ability to undo or roll back changes, so let's discuss how we might do that.
</p>

<p>
    Let's make some more changes.
</p>

<pre class="lang-xxx"><code>
> git diff
diff --git a/Ex0.java b/Ex0.java
index 3beec32..4ece717 100644
--- a/Ex0.java
+++ b/Ex0.java
@@ -2,5 +2,8 @@ public class Ex0 {
     public static void main(String[] args) {
         System.out.println("Hello World");
         System.out.println("Meaningless");
+        if (true) {
+            System.out.println("I'm a bug");
+        }
     }
 }
</code></pre>

<p>
    If we haven't made a commit yet, it's easy to discard our changes. Git status tells us how.
</p>

<pre class="lang-xxx"><code>
> git status
On branch master
Changes not staged for commit:
  (use "git add &lt;file&gt;..." to update what will be committed)
  (use "git checkout -- &lt;file..." to discard changes in working directory)

    modified:   Ex0.java

no changes added to commit (use "git add" and/or "git commit -a")
</code></pre>

<p>
    The specific line that tells us what to do is here:
</p>

<pre class="lang-xxx"><code>
    use "git checkout -- &lt;file&gt;..." to discard changes in working directory
</code></pre>

<pre class="lang-xxx"><code>
> git checkout -- Ex0.java
> git diff
> git status
On branch master
nothing to commit, working tree clean
</code></pre>

<p>
    The changes we made are gone forever, completely unrecoverable.
</p>

<p>
    What if we already committed something bad?
</p>

<pre class="lang-xxx"><code>
> git log
commit f40612eb9c2174087d27f8fa5004209fbd3936b2 (HEAD -> master)
Author: Sam McFalls &lt;samuel.mcfalls@hopeacademyhbc.org&gt;
Date:   Tue Jul 14 21:45:58 2020 -0400

    Introduce a bug

commit 1fc5e84b40850fa82058068a997ed8dad7c1aad6
Author: Sam McFalls &lt;samuel.mcfalls@hopeacademyhbc.org&gt;
Date:   Tue Jul 14 21:38:23 2020 -0400

    Add new output

commit a76b8e63c4bbf5cea40cb9db3be6c82fc81ab407
Author: Sam McFalls &lt;samuel.mcfalls@hopeacademyhbc.org&gt;
Date:   Tue Jul 14 21:17:34 2020 -0400

    Add Ex0.java
    
    Added first Java file
</code></pre>

<p>
    <code>git reset</code> will completely change the state of our repository to a specific commit. If we want to roll back to <code>1fc5e84b40850fa82058068a997ed8dad7c1aad6</code>, the last version with no bugs, we would use this command.
</p>

<pre class="lang-xxx"><code>
    git reset --hard 1fc5e84b40850fa82058068a997ed8dad7c1aad6
</code></pre>

<pre class="lang-xxx"><code>
> git reset --hard 1fc5e84b40850fa82058068a997ed8dad7c1aad6
HEAD is now at 1fc5e84 Add new output
> git status
On branch master
nothing to commit, working tree clean
> git log
commit 1fc5e84b40850fa82058068a997ed8dad7c1aad6 (HEAD -> master)
Author: Sam McFalls &lt;samuel.mcfalls@hopeacademyhbc.org&gt;
Date:   Tue Jul 14 21:38:23 2020 -0400

    Add new output

commit a76b8e63c4bbf5cea40cb9db3be6c82fc81ab407
Author: Sam McFalls &lt;samuel.mcfalls@hopeacademyhbc.org&gt;
Date:   Tue Jul 14 21:17:34 2020 -0400

    Add Ex0.java
    
    Added first Java file
</code></pre>

<p>
    Commit <code>f40612eb9c2174087d27f8fa5004209fbd3936b2</code> is now gone forever, completely unrecoverable.
</p>

<div class="note">
    <span class="note-h">
        Note
    </span>
    <p>
        Any time you need to reference a commit hash in Git, you can just use the first few characters, you don't need to use the entire thing. In the previous steps, we could have just used <code>1fc5e8</code> and it would have been fine. Hashes are often abbreviated this way.
    </p>
</div>

<p>
    Both of these are great if those were terrible awful changes that we want to forget about, but if we just want to compare behavior between the new and old versions, or investigate something, we don't want to delete the current changes, we need something more sophisticated.
</p>

<p>
    If you get this far, and want to learn this, let me know, I'm happy to help out. I don't think it's worth spending more time here if you're not going to use it for a while.
<p>

<p>
    If you found this lesson confusing, you can try the official <a href=https://git-scm.com/book/en/v2>Git book</a> (It's free). It's more in-depth but might make more sense. (Also good to read if you just want to know more about Git.) If it's still not making sense, this is an advanced topic and isn't required for any assignments, so don't worry too much.
</p>

</div>
</body>
</html>
