# Lesson 7: Git

All of the assignments so far are been short and straightforward. Project 1 is not so, it is much more complex, you probably won't be able to do it in one sitting. You probably will accidentally break things that used to work. You will run into this more and more the longer you program.

Linus Torvalds created Git to help deal with these problems. Git is a kind of Version Control System, or VCS. It allows you to save a version of your program in a specific state so that if you mess up, you can roll back to a known good state.

Git is also a staple of modern software development, anything you do with computer programming is likely to heavily rely on it. Unfortunately, many college students graduate without ever learning about it, which is really a tragedy. Learning Git will make your life as a student much easier as well as prepare you for the real world.

## Philosophy

Git stores "snapshots" of files called **commits**. A commit is not a complete copy of all the code, but the differences from the last commit.

***graphic of line of commits***

You can make new changes

***graphic***

and then abandon them.

***graphic***

You can also **branch** off to test changes you think are risky.

***graphic***

The main branch is referred to as the **master** branch.

## Installing Git

Visit the Git homepage and download the installer for your system. https://git-scm.com/downloads 

***installation screenshots***

***test installation with version***

***make sure to configure settings and use sublime as editor***

## Using Git (Exercise 0)

Make a new directory, `lesson_7/ex0`. Create a simple hello world program named `Ex0.java`.

In your terminal, change directories to your `ex0` directory. To make a new Git repository, type `git init`. This is short for "initialize". You should see something like this.

~~~
> git init
Initialized empty Git repository in /Users/smcfalls/Documents/Hope Academy/2020/CS/ha_intro_to_cs/lessons/lesson_7/ex0/.git/
~~~

List the directory with `ls`. You should see a new directory called `.git`. Git keeps all of its special information here, the commits, branches, etc.

The most common git command you will use is `git status`, try it out now. It will tell you what is going on in the repository.

~~~
> git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)

    Ex0.java

nothing added to commit but untracked files present (use "git add" to track)
~~~

There's already a lot to break down, but it's not complicated.

`On branch master`

The default branch is called `master`. That's the branch we start on.

`No commits yet`

This shouldn't be surprising, we just created the repository.

~~~
Untracked files:
  (use "git add <file>..." to include in what will be committed)

    Ex0.java
~~~

This is pretty straightforward, Git sees a file that is "untracked", meaning that Git is not tracking any changes you make to that file.

`nothing added to commit but untracked files present (use "git add" to track)`

Also simple, git helps us out and tells us how to track a file. Let's try it out.

~~~
> $ git add Ex0.java
>
~~~

No output means that we didn't have any errors. Let's check the status again to see where we're at.

~~~
> git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

    new file:   Ex0.java
~~~

The only difference is the bottom section, "Changes to be committed". When we added the file, we have **staged** it. We can stage multiple files to be added to a single commit. We only have one file, so we're ready to make a commit.

~~~
> git commit
~~~

Git opens a new sublime text window that lets us write a message that goes with our commit. This can be any text, but it's usually a good idea to write a summary line, followed by an empty line, followed by an in depth explanation if one is necessary. Let's write a message like this:

~~~
Add Ex0.java

Added first Java file
~~~

Save the file and close the sublime window. After that, the commit will finish. You should see output similar to this.

~~~
$ git commit
[master (root-commit) a76b8e6] Add Ex0.java
 1 file changed, 5 insertions(+)
 create mode 100644 Ex0.java
~~~

We can check `git status` again to see what we've done.

~~~
> git status
On branch master
nothing to commit, working tree clean
~~~

This is good, we have no staged or unstaged changes.

To see our commit history, we use `git log`.

~~~
$ git log
commit a76b8e63c4bbf5cea40cb9db3be6c82fc81ab407 (HEAD -> master)
Author: TechnoSam <technosam4jc@gmail.com>
Date:   Tue Jul 14 21:17:34 2020 -0400

    Add Ex0.java
    
    Added first Java file
~~~

We only have one commit so the entire history fits on our screen. If it doesn't, you can use k to scroll down, j to scroll up, and q to exit.

Each commit is named with a **hash**, shown at the top of each commit description. Each commit will have a unique hash, mine was `5cea40cb9db3be6c82fc81ab407`, your's will be different.

Just to check our understanding, let's make an edit to our file and add that as a new commit.

Let's add another line of output, it doesn't matter what. Then we can look at the status.

~~~
> git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

    modified:   Ex0.java

no changes added to commit (use "git add" and/or "git commit -a")
~~~

This should largely make sense.

We can see details of our change using `git diff`.

~~~
> git diff
diff --git a/Ex0.java b/Ex0.java
index 4e0fb07..3beec32 100644
--- a/Ex0.java
+++ b/Ex0.java
@@ -1,5 +1,6 @@
 public class Ex0 {
     public static void main(String[] args) {
         System.out.println("Hello World");
+        System.out.println("Meaningless");
     }
 }
~~~

We can see the new line we added with the `+` notation.

Let's add the file.

~~~
> git add Ex0.java 
> git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    modified:   Ex0.java
~~~

This should also make sense.

We can commit the file like normal. The message doesn't matter.

~~~
> git commit
[master 1fc5e84] Add new output
 1 file changed, 1 insertion(+)
~~~

We can check the history to see our new commit.

~~~
> git log
commit 1fc5e84b40850fa82058068a997ed8dad7c1aad6 (HEAD -> master)
Author: TechnoSam <technosam4jc@gmail.com>
Date:   Tue Jul 14 21:38:23 2020 -0400

    Add new output

commit a76b8e63c4bbf5cea40cb9db3be6c82fc81ab407
Author: TechnoSam <technosam4jc@gmail.com>
Date:   Tue Jul 14 21:17:34 2020 -0400

    Add Ex0.java
    
    Added first Java file
~~~

We can see the previous commit is of course still in the history.

This is great for documentation, but otherwise this has mostly just been busywork. We really want the ability to undo or roll back changes, so let's discuss how we might do that.

Let's make some more changes.

~~~
> git diff
diff --git a/Ex0.java b/Ex0.java
index 3beec32..4ece717 100644
--- a/Ex0.java
+++ b/Ex0.java
@@ -2,5 +2,8 @@ public class Ex0 {
     public static void main(String[] args) {
         System.out.println("Hello World");
         System.out.println("Meaningless");
+        if (true) {
+            System.out.println("I'm a bug");
+        }
     }
 }
~~~

If we haven't made a commit yet, it's easy to discard our changes. Git status tells us how.

~~~
> git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

    modified:   Ex0.java

no changes added to commit (use "git add" and/or "git commit -a")
~~~

`use "git checkout -- <file>..." to discard changes in working directory`

~~~
> git checkout -- Ex0.java
> git diff
> git status
On branch master
nothing to commit, working tree clean
~~~

The changes we made are gone forever, completely unrecoverable.

What if we already committed something bad?

~~~
> git log
commit f40612eb9c2174087d27f8fa5004209fbd3936b2 (HEAD -> master)
Author: TechnoSam <technosam4jc@gmail.com>
Date:   Tue Jul 14 21:45:58 2020 -0400

    Introduce a bug

commit 1fc5e84b40850fa82058068a997ed8dad7c1aad6
Author: TechnoSam <technosam4jc@gmail.com>
Date:   Tue Jul 14 21:38:23 2020 -0400

    Add new output

commit a76b8e63c4bbf5cea40cb9db3be6c82fc81ab407
Author: TechnoSam <technosam4jc@gmail.com>
Date:   Tue Jul 14 21:17:34 2020 -0400

    Add Ex0.java
    
    Added first Java file
~~~

`git reset` will completely change the state of our repository to a specific commit. If we want to roll back to `1fc5e84b40850fa82058068a997ed8dad7c1aad6`, the last version with no bugs, we would use this command.

`git reset --hard 1fc5e84b40850fa82058068a997ed8dad7c1aad6`

~~~
> git reset --hard 1fc5e84b40850fa82058068a997ed8dad7c1aad6
HEAD is now at 1fc5e84 Add new output
> git status
On branch master
nothing to commit, working tree clean
> git log
commit 1fc5e84b40850fa82058068a997ed8dad7c1aad6 (HEAD -> master)
Author: TechnoSam <technosam4jc@gmail.com>
Date:   Tue Jul 14 21:38:23 2020 -0400

    Add new output

commit a76b8e63c4bbf5cea40cb9db3be6c82fc81ab407
Author: TechnoSam <technosam4jc@gmail.com>
Date:   Tue Jul 14 21:17:34 2020 -0400

    Add Ex0.java
    
    Added first Java file
~~~

Commit 1fc5e84b40850fa82058068a997ed8dad7c1aad6 is now gone forever, completely unrecoverable.

Both of these are great if those were terrible awful changes that we want to forget about, but if we just want to compare behavior between the new and old versions, or investigate something, we don't want to delete the current changes, we need something more sophisticated.

***branch, checkout master, reset, change, merge***

If you haven't figured it out, Git is very complex, there are lots of features and lots of commands. There are different / better standard practices than what I taught you here, but I have taught you the basics and you will learn the more complex use cases as you go.

***maybe have an advanced git lesson for later***
