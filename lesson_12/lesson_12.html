<!DOCTYPE html>
<meta charset="UTF-8">
<html>
<head>
    <link href="../common/prism.css" rel="stylesheet" />
    <link rel="stylesheet" href="../common/styles.css">
</head>
<script src="../common/prism.js"></script>
<script src="../common/settings.js"></script>
<body>
<div class="centered" style="max-width: 55em;">

<h1>
    Lesson 12: IDEs
</h1>

<p>
    Until now, we have been programming with a text editor and a terminal. In the real world, not many people program in this way. I taught you to use these tools because many programmers who start with the advanced tools use them as a crutch and do not properly understand what they are doing.
</p>

<p>
    By now, you should have properly understood the fundamentals, so I can introduce new tools without harming you. We are also about to get into some more complex topics and you will appreciate having to do less work to manage old stuff.
</p>

<p>
    The new tool that I will teach you to use is called an <strong>IDE</strong>.
</p>

<div class="definition">
    <span class="definition-h">Definition</span>
    <p>
        An <strong>IDE</strong> (Integrated Development Environment) is a single application intended to be the only program needed to create, compile, and run programs.
    </p>
</div>

<p>
    IDEs include a text editor, compiler, and a terminal so you can do everything without switching windows. Because they integrate with the compiler, they are also able to check your code for errors as you type it, kind of like how Microsoft Word checks your spelling.
</p>

<p>
    There are many good IDE choices available, but we will use IntelliJ IDEA. The company that makes it (JetBrains) has IDEs for many other languages, not just Java. If you like IntelliJ, it's worth checking them out if you find yourself programming in a different language.
</p>

<p>
    To install IntelliJ, please refer to Appendix A.
</p>

<h2>
    Create a project
</h2>

<p>
    Before we can do anything we need to create a project. Open IntelliJ. You should see a screen like this:
</p>
<div class="center-children">
    <img src="img/create_project_1.png" style="width: 50%;">
</div>
<p>
    Click "New Project".
</p>
<p>
    Make sure that "Java" is selected on the left. Since you already have Java installed, "Project SDK" should say "1.8". If it doesn't, Java might not be installed correctly. You don't need to worry about any of the other stuff. Click "Next".
</p>
<div class="center-children">
    <img src="img/create_project_2.png" style="width: 50%;">
</div>
<p>
    Check the "Create project from template" box and choose "Command Line App".
</p>
<div class="center-children">
    <img src="img/create_project_3.png" style="width: 50%;">
</div>
<p>
    Finally, you need to choose your project name, location, and base package.
</p>
<p>
    The name can be anything you want. It should be the name that I give for the assignment, like "<code>assignment_120</code>" or "<code>project_2</code>".
</p>
<p>
    The location is the path to the folder where IntelliJ will create the project. It doesn't matter too much where you choose because IntelliJ will keep track of your recent projects, but it would still be a good idea to make sure you know where it is. By default it suggests a folder called "IntelliJ Projects". If you want to use that, you can, but use Windows Explorer to find the folder so that you won't lose track of your files.
</p>
<p>
    The base package can be anything, but it usually makes sense to do "org.name" (use your actual name of course). We haven't talked about packages much, but they are an important part of how Java programs work. We'll talk a bit more about this later.
</p>
<div class="center-children">
    <img src="img/create_project_4.png" style="width: 50%;">
</div>
<p>
    If the directory given in the location doesn't exist, IntelliJ will warn you about this with a pop up. This is fine.
</p>
<div class="center-children">
    <img src="img/create_project_5.png" style="width: 50%;">
</div>

<p>
    When the project opens, IntelliJ will open the tip window. There are lots of good tips and tricks in there, so you may wish to keep it, but it can be annoying to see it every time you open a project. You can disable it with the checkbox in the lower left corner. Close the window to move on.
</p>
<div class="center-children">
    <img src="img/create_project_6.png" style="width: 50%;">
</div>

<p>
    Now your new project is open. There is a little loading bar on the bottom that indicates IntelliJ is doing work. It's usually a good idea to wait for this to disappear before you start typing. As long as it's there, IntelliJ is processing a bunch of different things and it will be very slow receive any code you type.
</p>
<div class="center-children">
    <img src="img/create_project_7.png" style="width: 100%;">
</div>

<h2>
    Whitespace
</h2>

<p>
    Once your first project is open, we should configure IntelliJ to show whitespace like Sublime does. IntelliJ is smart and uses spaces by default, so we don't have to do anything there.
</p>

<p>
    To open the settings, click "File" > "Settings..." or press CTRL + ALT + S.
</p>
<div class="center-children">
    <img src="img/settings_1.png" style="width: 100%;">
</div>

<p>
    Navigate to "Editor" > "General" > "Appearance", and check the "Show whitespaces" box. Then click "Apply" and "OK".
</p>
<div class="center-children">
    <img src="img/settings_2.png" style="width: 100%;">
</div>

<h2>
    Compiling and Running
</h2>

<p>
    Add your <code>System.out.println("Hello World!");</code> to <code>main</code>, and then click the green hammer icon in the upper right to compile your program. If it compiles without errors, you'll see a message telling that the "Build completed successfully".
</p>
<div class="center-children">
    <img src="img/build.png" style="width: 100%;">
</div>

<p>
    If there are any errors, you'll see a list of each error, and you can click on it to be taken to the offending line.
</p>
<div class="center-children">
    <img src="img/build_2.png" style="width: 100%;">
</div>

<p>
    To run your program, click the green arrow icon in the upper right. This will also automatically compile your program if you haven't already. You can see the output in the "Run" frame.
</p>
<div class="center-children">
    <img src="img/run.png" style="width: 100%;">
</div>

<h2>
    Running your program with arguments
</h2>

<p>
    Many of the programs we create in this class require arguments to be passed to the program. How can we do that if we are using the big green button to run the program and not a terminal?
</p>

<p>
    To add the arguments, you need to edit the Run Configuration. Click the down arrow next to "Main" (this is the name of the configuration, so it will change if you rename your main class) and choose "Edit Configurations..."
</p>
<div class="center-children">
    <img src="img/args_1.png" style="width: 100%;">
</div>

<p>
    In the middle of the screen, there is an input field in the middle of the screen labeled "Program arguments". Type your arguments in there, then click "Apply", then "OK". You may find yourself doing this a lot if you are testing your program a lot of different ways.
</p>
<div class="center-children">
    <img src="img/args_2.png" style="width: 100%;">
    <img src="img/args_3.png" style="width: 100%;">
</div>

<p>
    Now if you run a program that requires arguments, it will work.
</p>
<div class="center-children">
    <img src="img/args_4.png" style="width: 100%;">
</div>

<h2>
    Where does the class file go?
</h2>

<p>
    In the past, we have been able to see the class file output from the compiler. Now the file is no where to be found. Where did it go?
</p>

<p>
    IntelliJ does a lot of high level project organization, which includes hiding the compile output from you. If you look for it, you should be able to find it in a folder called "<code>out</code>".
</p>

<p>
    This does make it harder to share your code, now someone needs to install IntelliJ to run your program, not just Java. That clearly isn't idea. The solution is to share your program as a JAR file, like I do with my reference implementations. IntelliJ helps you make these, but we will cover that in another lesson.
</p>

<h2>
    Renaming the main class
</h2>

<p>
    IntelliJ helps us out by automatically creating a main class for us, but it's not often that we actually want our main class to be named "<code>Main</code>". If we just change the name and run it, Java gives us an error.
</p>
<div class="center-children">
    <img src="img/rename_1.png" style="width: 100%;">
</div>

<p>
    The solution is to rename the file as well, but IntelliJ can actually do that for us. If you click on the <code>public class IntelliJ</code> line, you will see a red light-bulb pop up, IntelliJ is offering solutions to our problem. If you click the light-bulb, you can chose one of several solutions. "Rename file" is exactly what we want.
</p>
<div class="center-children">
    <img src="img/rename_2.png" style="width: 100%;">
</div>

<p>
    Lastly, our old run configuration will no longer work. It expects the main class to be named "<code>Main</code>". IntelliJ also offers and easy solution to that problem. You can click the green arrow next to the main function to run the program which will automatically create a new configuration.
</p>
<div class="center-children">
    <img src="img/rename_3.png" style="width: 100%;">
</div>
<p>
    Take care to notice that this is a <em>brand new</em> configuration! That means if you previously entered any program arguments, they are not going to be applied to the new configuration, you will have to do that step again.
</p>

<h2>
    Opening an existing project
</h2>

<p>
    If you didn't use IntelliJ to create a project, that's okay, you can still use IntelliJ!
</p>

<p>
    Click "File" > "Open..." and choose the directory your program is in.
</p>

<p>
    IntelliJ will not automatically create a Run Configuration for you, but just like when we talked about renaming the main class, we can easily add a run configuration by opening the file and clicking the green arrow next to the <code>main</code> function.
</p>

<h2>
    Debugging
</h2>

<p>
    This section is not intended to be a complete tutorial on debugging, for that, read Lesson 11. However I will briefly go over the concepts and how to use the tools provided by IntelliJ.
</p>

<p>
    First, make sure you have your program open and make sure you have a working Run Configuration. If you can't run your program normally, you're not going to be able to debug it!
</p>

<p>
    Next, click on margin right next to the line number to create a breakpoint at that line. Remember that a breakpoint is a place that the debugger will <strong>stop</strong> (or break) the program at if it hits that line. If you don't set a breakpoint, debugging will just be like running your program normally!
</p>
<p>
    Click the red dot to remove the breakpoint when you no longer need it.
</p>
<div class="center-children">
    <img src="img/debug_1.png" style="width: 100%;">
</div>

<p>
    Click the green bug icon to debug the program. It may be hard to tell that it looks like a bug, so it's the button directly to the right of the run button.
</p>

<p>
    You may get a pop up telling you that IntelliJ needs permission to get through the firewall. This is because Java actually uses networking to debug applications. You may need administrator access to allow this. If any parents have questions about what exactly this is, I can talk to them. If you are really concerned about security, you may not want to grant it access on public networks, but then you should set the Hope Academy network as a private network so that we can use the debugger in class.
</p>

<div class="center-children">
    <img src="img/debug_2.png" style="width: 50%;">
</div>

<p>
    When the debugger stops at your breakpoint, you will want to look at the Debug frame. There are a few important buttons.
</p>

<p>
    First, the "step over" button. This will execute the current line of code and jump to the next one. You will see the highlighted line move and the variables update to the right.
</p>
<div class="center-children">
    <img src="img/debug_3.png" style="width: 50%;">
</div>

<p>
    Next is the "step into" button. This will jump inside the function call on the current line, if there is one. This will let us get inside our own functions so we can look at problems inside, not just in main.
</p>
<div class="center-children">
    <img src="img/debug_4.png" style="width: 50%;">
</div>

<p>
    Finally is the "continue" button. This will resume the program normally until it hits another breakpoint (or possibly the same point again if it's in a loop). This is useful to move through code more quickly if you want to jump through large blocks at a time, or if you want to look at something each time at the beginning of a loop, or something like that.
</p>
<div class="center-children">
    <img src="img/debug_5.png" style="width: 50%;">
</div>

<p>
    In this example, you can see that the <code>counter</code> is at 6, but that value hasn't been printed yet.
</p>
<div class="center-children">
    <img src="img/debug_6.png" style="width: 100%;">
</div>

<p>
    If you switch to the Console tab, you can verify that.
</p>
<div class="center-children">
    <img src="img/debug_7.png" style="width: 100%;">
</div>

<p>
    Effective use of the debugger will be essential for some of the remaining assignments in the class.
</p>

<h2>
    Extra Goodies
</h2>

<p>
    Using an IDE like IntelliJ comes with several advantages.
</p>

<ul>
    <li>
        <p>
            Everything is in one place. Your terminal, code, file browser, it's all here. You won't really run into problems like trying to compile your program from the wrong directory, or forgetting where you saved a project. It will all be easily accessible.
        </p>
    </li>
    <li>
        <p>
            IntelliJ automatically saves everything you type. No more forgetting to save before you compile!
        </p>
    </li>
    <li>
        <p>
            IntelliJ automatically compiles before running. No more forgetting to compile!
        </p>
    </li>
    <li>
        <p>
            Smarter auto complete! Sublime offers suggestions based on what you previously typed, but IntelliJ can actually read code and determine what options are real. This makes spelling errors much less likely.
        </p>
        <div class="center-children">
            <img src="img/suggestions_1.png" style="width: 100%;">
            <img src="img/suggestions_2.png" style="width: 100%;">
        </div>
    </li>
    <li>
        <p>
            IntelliJ can check your code as you type. If you accidentally try to use a variable that doesn't exist, it will underline it in red. It will even suggest the proper fix.
        </p>
    </li>
    <li>
        You can hold CTRL while you click on a function, and it will take you directly to that function. You can press CTRL + ALT + LEFT to go back to where you where. This is much better than scrolling up and down.
    </li>
    <li>
        <p>
            If you click on a function and press CTRL + Q, it will pop up the documentation for the function without taking you away from where you are. This is useful if you forget which arguments you need to use, and it also helps you learn more because you can read the official documentation for a function right in your editor.
        </p>
        <div class="center-children">
            <img src="img/docs_1.png" style="width: 100%;">
        </div>
    </li>
</ul>

<p>
    There are many, many more shortcuts available. I can't cover them all because it would be overwhelming. But this short list should give you a taste of what is possible and greatly simplify your work
</p>

<h2>
    Downsides
</h2>

<p>
    For all the good, there are some downsides to using IDEs.
</p>

<p>
    I've already mentioned the biggest one. Many developers use IDEs as a crutch. They forget how to compile programs, or how to run them outside the IDE. The IDE is for <strong>you and you only</strong>. You don't want your users to have to install or see it, so you need to be familiar with the fundamentals.
</p>

<p>
    In the same vein, some programmers become lazy. When the IDE will check for typos, suggest fixes, manage your compile, sometimes your problem solving ability can degrade. Sometimes the fix the IDE suggests is actually <strong>not</strong> correct. You have to be smart enough to recognize these scenarios and create the proper solution yourself. The IDE cannot program for you.
</p>

<p>
    As we saw, managing program arguments can be cumbersome. Editing the Run Configuration is a multi-step process. It's much easier to just run the program from the terminal and change the arguments.
</p>

<p>
    The IDE hides the output files from you. We already talked about how this makes it harder to share the program, but another cost is that you cannot really go "back" to just using the terminal. To compile an IntelliJ project from the terminal, you need to understand the package system, and possibly the classpath system. These are not trivial and we haven't covered them yet. If anyone is interested, I may make an appendix that covers this. Once I teach you how to make your own JARs, I think the need for that will mostly disappear, so it might not be a problem.
</p>

<h2>
    Closing
</h2>

<p>
    An IDE can hide a lot of complexity from you. This is both good and bad. Most of the time it's good. Use them to get more done, but be responsible and don't become lazy! Make sure you understand the fundamentals!
</p>

</div>
</body>
</html>
