import java.util.Scanner;

public class Ex4 {
    public static void main(String[] args) {
        Scanner inputScanner = new Scanner(System.in);

        System.out.print("Enter 1 to choose the red potion, "
            + "or 2 to choose the blue: ");
        int choice = inputScanner.nextInt();
        String potion;
        if (choice == 1) {
            potion = "red";
        }
        else if (choice == 2) {
            potion = "blue";
        }
        else {
            System.out.println("Must enter 1 or 2!");
            return;
        }

        System.out.println("You chose the " + potion + " potion");
    }
}
