public class Ex3 {
    public static void main(String[] args) {
        try {
            System.out.println(args[0]);
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("You didn't give me any arguments!");
            System.out.println("Exception: " + e);
            System.out.println("Message: " + e.getMessage());
        }
    }
}
