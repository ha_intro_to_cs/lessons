import java.util.Random;

public class CoinFlip {
    public static void main(String[] args) {
        Random rng = new Random();
        int roll = rng.nextInt(100) + 1;
        if (roll <= 50) {
            System.out.println("Heads!");
        }
        else {
            System.out.println("Tails!");
        }
    }
}
