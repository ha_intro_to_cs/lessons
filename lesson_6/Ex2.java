import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
        System.out.println("Welcome to the character creator");

        Scanner in = new Scanner(System.in);

        System.out.print("Enter your character's name: ");
        String name = in.nextLine();

        System.out.print("Enter your character's attack power: ");
        double attackPower = in.nextDouble();

        System.out.print("Enter your character's HP: ");
        int hp = in.nextInt();

        System.out.println();
        System.out.println("This is the character you created.");
        System.out.println("Name: " + name);
        System.out.println("Attack Power: " + attackPower);
        System.out.println("HP: " + hp);

        int monsterHp = 100;

        if (name.equals("Gandalf")) {
            System.out.println("Gandalf always wins!");
        }
        else {
            if (attackPower >= monsterHp) {
                System.out.println("You defeated the monster!");
            }
            else {
                System.out.println("The monster beat you!");
            }
        }
    }
}
