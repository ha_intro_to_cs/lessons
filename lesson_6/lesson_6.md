# Lesson 6: Making Decisions

Doing math is cool and plenty helpful, but that's still clearly missing some real power. Last lesson, we made a character creator that let us specify an attack power, but without the ability to check if that's more than an opponent's defense power, it doesn't get us anything useful.

## Boolean Algebra

George Boole created a new branch of algebra in the mid 1800s using only the values `True` and `False`. To keep it in math terms, you can equivalently say `1` and `0`. The primitive type `boolean` gets its name from him.

Like in normal math, there are operators we can use to combine values. Here are the operators we will be using in this course.

| Operator | Symbol |
|:--------:|:------:|
|    not   |    !   |
|    and   |   &&   |
|    or    |  \|\|  |

Of course there are more operators than this, but this is all we need for now.

Let's go through what each of these operators do.

* `not`

The `not` operator is a unary operator, meaning that it operates on only one value. `not true` is `false` and `not false` is `true`. We can show this as a table.

| x | !x |
|:-:|:--:|
| 0 |  1 |
| 1 |  0 |

There is no doubt in boolean logic, everything is either completely true or completely false. How nice!

* `and`

The `and` operator is an "n-ary" operation which means it takes any number of arguments. The result of an `and` operation is `true` if *all* of the arguments are true and `false` otherwise. Here is a table for two arguments.

| x | y | x && y |
|:-:|:-:|:------:|
| 0 | 0 |    0   |
| 0 | 1 |    0   |
| 1 | 0 |    0   |
| 1 | 1 |    1   |

* or

The `or` operator is an n-ary operation which evaluates to `true` if *any* of the arguments are true, and only `false` if none are.

| x | y | x \|\| y |
|:-:|:-:|:--------:|
| 0 | 0 |     0    |
| 0 | 1 |     1    |
| 1 | 0 |     1    |
| 1 | 1 |     1    |

### Truth Tables

The tables I have been showing you are called truth tables. Given a binary expression, you can calculate all possible values by filling out a truth table.

To practice what we've learned, let's make some tables combining these operations.

### Exercise 0

Let's say we want to make a truth table for the expression `(x && y) || z`.

The first step is to create an empty table. How many possible combinations of values are there? To figure it out, we can count the number of arguments. A boolean function with `n` arguments has `2^n` possible input combinations. When you take statistics, you will learn more about why this is, but you can also think of this as a binary number with `n` positions. This expression has 3 variables, so we have 8 possible combinations.

| x | y | z | (x && y) \|\| z |
|:-:|:-:|:-:|:---------------:|
|   |   |   |                 |
|   |   |   |                 |
|   |   |   |                 |
|   |   |   |                 |
|   |   |   |                 |
|   |   |   |                 |
|   |   |   |                 |
|   |   |   |                 |

Step two is to fill in all possible inputs. This is really easy if we treat the inputs as a binary number with `n` positions. We have 8 positions, so we can count from 0 to 7. In binary, this is `000`, `001`, `010`, `011`, `100`, `101`, `110`, `111`. If you look, you'll see that this covers all possible values.

| x | y | z | (x && y) \|\| z |
|:-:|:-:|:-:|:---------------:|
| 0 | 0 | 0 |                 |
| 0 | 0 | 1 |                 |
| 0 | 1 | 0 |                 |
| 0 | 1 | 1 |                 |
| 1 | 0 | 0 |                 |
| 1 | 0 | 1 |                 |
| 1 | 1 | 0 |                 |
| 1 | 1 | 1 |                 |

The last step is to calculate the value of the expression for each combination. This is the hardest part. Let's start with the first row.

| x | y | z | (x && y) \|\| z |
|:-:|:-:|:-:|:---------------:|
| 0 | 0 | 0 |                 |

We can plug in the values into the expression.

`(0 && 0) || 0`

We evaluate the parenthesis first. `0 && 0` is `0`. You can look at the truth table for `&&` if you need to.

`0 || 0`

`0 || 0` is `0`. Same deal, you can look at the truth table for `||` if you need. If you work with binary logic enough, eventually you will remember these without trying.

Now we can fill in the first line.

| x | y | z | (x && y) \|\| z |
|:-:|:-:|:-:|:---------------:|
| 0 | 0 | 0 |        0        |

To complete the table, we just need to repeat for each row. I will do a few more.

`(0 && 0) || 1`

`0 || 1`

`1`

| x | y | z | (x && y) \|\| z |
|:-:|:-:|:-:|:---------------:|
| 0 | 0 | 0 |        0        |
| 0 | 0 | 1 |        1        |

`(0 && 1) || 0`

`0 || 0`

`0`

| x | y | z | (x && y) \|\| z |
|:-:|:-:|:-:|:---------------:|
| 0 | 0 | 0 |        0        |
| 0 | 0 | 1 |        1        |
| 0 | 1 | 0 |        0        |

`(0 && 1) || 1`

`0 || 1`

`1`

| x | y | z | (x && y) \|\| z |
|:-:|:-:|:-:|:---------------:|
| 0 | 0 | 0 |        0        |
| 0 | 0 | 1 |        1        |
| 0 | 1 | 0 |        0        |
| 0 | 1 | 1 |        1        |

You might notice we can take a shortcut. Because we're `||`'ing the expression on the left with `z`, we know the entire expression will be `1` whenever `z` is `1`. So we can shortcut a few more answers.

| x | y | z | (x && y) \|\| z |
|:-:|:-:|:-:|:---------------:|
| 0 | 0 | 0 |        0        |
| 0 | 0 | 1 |        1        |
| 0 | 1 | 0 |        0        |
| 0 | 1 | 1 |        1        |
| 1 | 0 | 0 |                 |
| 1 | 0 | 1 |        1        |
| 1 | 1 | 0 |                 |
| 1 | 1 | 1 |        1        |

For the last two, I'll do the work in my head.

| x | y | z | (x && y) \|\| z |
|:-:|:-:|:-:|:---------------:|
| 0 | 0 | 0 |        0        |
| 0 | 0 | 1 |        1        |
| 0 | 1 | 0 |        0        |
| 0 | 1 | 1 |        1        |
| 1 | 0 | 0 |        0        |
| 1 | 0 | 1 |        1        |
| 1 | 1 | 0 |        1        |
| 1 | 1 | 1 |        1        |

Try this expression on your own. The answer is provided below, try to do it before you look at the answer.

`(x || y) && z`

| x | y | z | (x \|\| y) && z |
|:-:|:-:|:-:|:---------------:|
| 0 | 0 | 0 |        0        |
| 0 | 0 | 1 |        0        |
| 0 | 1 | 0 |        0        |
| 0 | 1 | 1 |        1        |
| 1 | 0 | 0 |        0        |
| 1 | 0 | 1 |        1        |
| 1 | 1 | 0 |        0        |
| 1 | 1 | 1 |        1        |

## Boolean Expressions in Java

We have been working with boolean expressions so far using only boolean values, but on their own they're not so useful to us. (Actually, we could do everything with booleans if we wanted, remember that everything in computers is a series of 0s and 1s, so technically we could just do all the extra work ourselves, but that would be terrible...)

Here are some more Java operators that produce boolean values from numerical types.

| Symbol |          Name         |
|:------:|:---------------------:|
|   ==   |        Equality       |
|   !=   |       Inequality      |
|    >   |      Greater than     |
|    <   |       Less than       |
|   >=   | Greater than or equal |
|   <=   |   Less than or equal  |

All of these are binary operators which let us compare numerical values.

`10 == 10` evaluates to `True`, and `10 != 10` evaluates to `False`. You can see why `=` is used for assignment, it's more common than comparison.

Let's do a quick exercise to make sure we have a grasp on all of these.

### Exercise 1

~~~java
public class Ex1 {
    public static void main(String[] args) {
        System.out.println(10 == 10);
        System.out.println(10 != 10);
        System.out.println(10 != 11);
        System.out.println(10 == 10.0);

        System.out.println(10 > 5);
        System.out.println(10 < 10);
        System.out.println(10 <= 10);
        System.out.println(10 < 20);
        System.out.println(10 > 10);
        System.out.println(10 >= 10);

        System.out.println((1.0 / 3.0) == 0.333);
        System.out.println(0.5 == 0.499999999999999999);
    }
}
~~~

As usual, try to predict what each line will print. There are some "gotchas" in here.

Here's the output you should get.

~~~
> java Ex1
true
false
true
true
true
false
true
true
false
true
false
true
~~~

`10 < 10` equals `false`. 10 is not less than 10, it is equal to 10.

`(1.0 / 3.0)` is not equal to `0.333`, `1/3` has an infinite number of of digits.

`0.5 == 0.499999999999999999` actually evaluates to `true`! That's strange. This is the precision I warned you about a few lessons ago. There is a limit on how precise a `double` can be, at a certain point they are represented by the same bits.

The two examples at the end show how you should be careful when testing equality of floating point numbers. Usually it's better to check that they are within a certain threshold for your application, i.e.

`(float1 - float2) < 0.01`

---

What about non-numerical types, like `String`? Java provides an `equals` function to check if two variables have the same value, but that's it. You should always use this function to compare non-primitive types. If a custom type needs to support the idea of greater than or less than, you'll have to read the documentation to see how the creator handled it.

## Using Boolean Expressions

We have a lot of tools to determine if a set of conditions is `True` or `False`, but now we need to use them to conditionally execute statements. Enter the `if` keyword.

The `if` keyword, like a function definition, creates a block of statements that will only run if a condition is true. This block of statements is referred to as a **branch**. The syntax is like this.

~~~java
if (condition) {
    statements;
}
~~~

So to check if our attack power is greater than an enemies defense, we might do something like this.

~~~java
if (playerAttack > enemyDefense) {
    damage = (playerAttack - enemyDefense);
    enemyHp = enemyHp - damage;
}
~~~

Using the `else` keyword, we can supply statements to execute if the condition was *not* true.

~~~java
if (playerAttack > enemyDefense) {
    damage = (playerAttack - enemyDefense);
    enemyHp = enemyHp - damage;
}
else {
    System.out.println("Your attack was too weak!");
}
~~~

Finally, you can combine an `else` and an `if` to make a chain of conditions that will execute only the first branch where the condition is true, checked in order.

~~~java
if (playerAttack > enemyDefense) {
    damage = (playerAttack - enemyDefense);
    enemyHp = enemyHp - damage;
}
else if (playerMagicAttack > enemyDefense) {
    damage = (playerMagicAttack - enemyDefense);
    enemyHp = enemyHp - damage;
}
else {
    System.out.println("Your attack was too weak!");
}
~~~

(This is pretty bad code, you'd want to use the largest damage value, this will always take regular attack if it's higher than the enemy defense. I just intend this to illustrate a point which I think it does.)


## Exercise 2

Let's take this opportunity to improve our character creator to see how he stacks up against a monster.

You can copy and paste your previous program here, you don't have to retype it.

~~~java
import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
        System.out.println("Welcome to the character creator");

        Scanner in = new Scanner(System.in);

        System.out.print("Enter your character's name: ");
        String name = in.nextLine();

        System.out.print("Enter your character's attack power: ");
        double attackPower = in.nextDouble();

        System.out.print("Enter your character's HP: ");
        int hp = in.nextInt();

        System.out.println();
        System.out.println("This is the character you created.");
        System.out.println("Name: " + name);
        System.out.println("Attack Power: " + attackPower);
        System.out.println("HP: " + hp);

        int monsterHp = 100;

        if (name.equals("Gandalf")) {
            System.out.println("Gandalf always wins!");
        }
        else {
            if (attackPower >= monsterHp) {
                System.out.println("You defeated the monster!");
            }
            else {
                System.out.println("The monster beat you!");
            }
        }
    }
}
~~~

There shouldn't be any surprises here. You might be surprised that you "nest" `if` statements like this, but it should make sense, there are no conditions on the contents of an `if` branch, they can contain any statements the parent block can contain.
