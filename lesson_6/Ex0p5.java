public class Ex0p5 {
    public static void main(String[] args) {
        boolean x;
        boolean y;
        boolean z;
        boolean result;

        System.out.println("x     y     z       result");

        // 0 0 0
        x = false;
        y = false;
        z = false;
        result = (x && y) || z;
        System.out.println(x + " " + y + " " + z + " -> " + result);

        // 0 0 1
        x = false;
        y = false;
        z = true;
        result = (x && y) || z;
        System.out.println(x + " " + y + " " + z + " -> " + result);

        // 0 1 0
        x = false;
        y = true;
        z = false;
        result = (x && y) || z;
        System.out.println(x + " " + y + " " + z + " -> " + result);

        // 0 1 1
        x = false;
        y = true;
        z = true;
        result = (x && y) || z;
        System.out.println(x + " " + y + " " + z + " -> " + result);

        // 1 0 0
        x = true;
        y = false;
        z = false;
        result = (x && y) || z;
        System.out.println(x + " " + y + " " + z + " -> " + result);

        // 1 0 1
        x = true;
        y = false;
        z = true;
        result = (x && y) || z;
        System.out.println(x + " " + y + " " + z + " -> " + result);

        // 1 1 0
        x = true;
        y = true;
        z = false;
        result = (x && y) || z;
        System.out.println(x + " " + y + " " + z + " -> " + result);

        // 1 1 1
        x = true;
        y = true;
        z = true;
        result = (x && y) || z;
        System.out.println(x + " " + y + " " + z + " -> " + result);

    }
}
