public class Ex1 {
    public static void main(String[] args) {
        System.out.println(10 == 10);
        System.out.println(10 != 10);
        System.out.println(10 != 11);
        System.out.println(10 == 10.0);

        System.out.println(10 > 5);
        System.out.println(10 < 10);
        System.out.println(10 <= 10);
        System.out.println(10 < 20);
        System.out.println(10 > 10);
        System.out.println(10 >= 10);

        System.out.println((1.0 / 3.0) == 0.333);
        System.out.println(0.5 == 0.499999999999999999);
    }
}
