# Lesson 4: Data, Memory, and Variables

We've seen that computers handle numbers differently than we do, so let's dive in to fully explore how Java helps us handle data.

## How to store data

When a computer wants to store any kind of data, it uses **memory**. This is normally referred to as RAM, or Random Access Memory. Memory is different from your hard drive, that is referred to as **storage**. Random Access means that you can access any piece of memory at random in the same amount of time. This qualifier is not as useful as it used to be, most modern hard drives are SSDs, which are also random access.

In reality, the only difference is speed. RAM is still much faster than SSDs. If you tried to run your computer using only an SSD, it would be extremely slow.

You might ask, "why not make everything out of RAM if it's so much faster?" The answer is primarily cost. (RAM also loses its contents without being constantly powered.) 16GB of DDR4 RAM costs about as much as 500GB of SSD. As it turns out, you CPU also has built in memory called **cache** which is even faster than RAM. Very expensive CPUs have only around 16MB of cache.

Compilers and operating systems hide these differences from us, so we don't really have to worry about it, we can talk about memory as a black box.

We don't need to cover the mechanics of exactly how memory holds data, but we need to understand a few points.

All data is stored as a series of 1s or 0s. This is measured by the presence or absence of electrical charge in a memory cell. A single cell of memory is referred to as a **bit**. Bits can be *on* or *off*, 1 or 0.

If you want to read from or write to a bit in memory, you're not typically able to change just one bit directly. Bits are grouped into sets called **bytes**, which are the smallest addressable units of memory. The number of bits in a byte has varied wildly historically, but in the modern era is almost always is **8 bits**. Sometimes you will see the word **octet** used instead to eliminate confusion.

### Using bytes

How do we use bytes to store numbers? In order to answer that question, we must first understand our own system for working with numbers.

The number system Americans use is called *decimal*, or *base-ten*. We have ten numerals to represent values, `0123456789`. These symbols are arbitrary, we could have chosen anything to represent the idea of three, or five, but we settled on these.

To express a number greater than 9, we have to use **positional notation**. We call it that because the position of a digit tells us its value.

Consider the number 2156. How do you know what that means?

***svg breaking down system***

The value of the number is the sum of the digits multiplied by their exponent of the base. Position 0's multiplier is 10^0. Position 1's multiplier is 10^1. Position 2's multiplier is 10^2. Continue as much as needed.

So to evaluate 2156, we understand that this means 2 * 10^3 + 1 * 10^2 + 5 * 10^1 + 6 * 10^0.

This also works for fractional numbers too. Any positions to the right of the decimal point simply have negative positions.

3.14 = 3 * 10^0 + 1 * 10^-1 + 4 * 10^-2.

The base-ten system is pretty great. But it's got one catch that prevents us from using it in computers... we need ten distinct symbols for our digits. We only have two, `0` and `1`. How can we represent `2`?

The answer is to use base-two, also called binary. All the math is exactly the same, but we use base-two instead so that we have enough symbols.

***svg breaking down system similar to above***

If we wanted to write 2156 in base-two, we would write 100001101100. Let's break this down.

1 * 2^11 + 0 * 2^10 + 0 * 2^9 + 0 * 2^8 + 0 * 2^7 + 1 * 2^6 + 1 * 2^5 + 0 * 2^4 + 1 * 2^3 + 1 * 2^2 + 0 * 2^1 + 0 * 2^0.

Try this on a calculator, it adds up.

This is how positive integers are stored in computers. Of course, there is more to data than positive integers. It is worth your time to look into these other formats online, but positive integers is the only type I'll require you to understand.

Non-integers can be stored two different ways. The method we discussed for decimal numbers is called **fixed point**. This is simple enough that you don't actually have to change your data format, just how you use the data. Unless specific precision is needed, this isn't two common. The other method is called **floating point**. This is much more complex and requires it's own formatting scheme. If you don't care about any specific amount of precision, this works very well and Java makes it easy for us to do that.

Negative integers are stored in **two's complement** representation. Again, Java automatically handles this for us, so we don't have to worry about it, but it's good to understand.

All of these data formats have been numbers, and almost everything in computer programs is a number in some form, but for some data it is not helpful to think of it as a number. Text is one example of this. Text encoding can be very complex, and we will cover it in more detail in lesson 8. If you would like to know more, Java defaults to **Unicode**.

### Terminology

#### Memory size

Terminology used to talk about memory is all over the place and needlessly complicated. I'm going to try to boil it down as simple as I can, but bear with me if this isn't super clear.

When we discuss a large number of bytes, it is helpful to use the SI prefixes, kilo, mega, giga, etc.

A kilobyte is 1,000 bytes, or 10^3 bytes.

A megabyte is 1,000,000 bytes, or 10^6 bytes.

A gigabyte is 1,000,000,000 bytes, or 10^9 bytes.

This should match with what your already know about units, for example a kilometer is 1000 meters.

This all seems reasonable until we remember that the SI system is designed around base ten, everything is a power of ten. Computers are not based around ten, they are based around two. Therefore, computer scientists invented new prefixes, kibi, mebi, gibi, etc.

A kibibyte is 1,024 bytes, or 2^10 bytes.

A mebibyte is 1,048,576, or 2^20 bytes.

A gibibyte is 1,073,741,824, or 2^30 bytes.

**Unfortunately**, it is common for both kilobytes and kibibytes to be abbreviated as `KB`, meaning that it is ambiguous which is being referred to.

In this course, `KB` will always refer to the SI version, 10^3. `KiB` will be used to denote kibibytes.

Outside of this course, if you see `KiB`, or `GiB`, it's always clear what the person is talking about, but if you just see `KB`, or `GB`, you have to understand the context to understand what they're talking about.

#### Bits and Bytes

Bits are abbreviated with a `b`, bytes with `B`. Thus `Mbps` is megabits per second, `MBps` is megabytes per second, and `MiBps` is mebibytes per second.

#### Bases

When discussing numbers with different bases, it can quickly become confusing which base a number is in.

To denote that 1010 is "one thousand ten", we could write `0d1010`, or `(1010)_10`. (The `0d` means "decimal".)

If we mean 1010 to represent "ten", we could write `0b1010`, or `(1010)_2`. (The `0b` means "binary".)

## Exercise 0

Practice converting to and from binary.

`System.out.println(0b1011010);`
`System.out.println(Integer.toBinaryString(242));`

## Types of data

If we want to store some data, we need to tell Java what **type** of data it is so it can figure out how to put it into memory. Here are a few important types that Java offers:

|   Type  |      Description      | Size in bits |
|:-------:|:---------------------:|:------------:|
|   byte  |        integer        |       8      |
|  short  |        integer        |      16      |
|   int   |        integer        |      32      |
|   long  |        integer        |      64      |
|  float  | decimal approximation |      32      |
|  double | decimal approximation |      64      |
| boolean |     True or False     |   Undefined  |
|   char  |   Unicode character   |      16      |
|  String |      Unicode text     |    Dynamic   |

`byte`, `short`, `int`, and `long` all store integer values using varying amounts of memory. The bigger the maximum number you need to support, the more memory you'll need to use. As we discussed above, we can use these for fixed point and negative numbers.

`float` is a single precision floating point number. This is the special type that Java gives us to handle floating point as we discussed previously. `double` is "double precision floating point number", it's the same standard but twice as many bits for twice the precision.

`char` represents a single character of Unicode text, while `String` represents any number of `chars` "strung" together. We're going to see why this is a bit misleading later, but suffice it to say that `String` is for storing text.

Almost every time we deal with data we have to explicitly tell Java what type the data is. The only exception is when dealing with literals, Java will automatically use `int`, `double`, `boolean`, or `String`.

## Variables

If we want to keep track of something over time instead of a constant literal, we need to create a **variable**. When we create a variable, we tell the computer to reserve us some memory that we put a name on so we can refer to that memory later.

***svg variable***

Declaring a variable is simple.

~~~java
int daysUntilSummer;
~~~

First we write the type, which is a keyword for primitive types, but an identifier for all others. Then we choose our own identifier for this new variable. It has to follow all the rules of identifiers, and it also can't be the name of another variable that has already been declared. Then we end with a semicolon because this is a statement, we are asking the computer to reserve some memory for us.

If we want to do anything with this memory, we need to learn the **assignment** operator.

~~~java
daysUntilSummer = 160;
~~~

This looks confusing at first, in math,the `=` means equality. In Java, `=` is an operator that assigns the value on the right to the variable on the left. You can read this as "`daysUntilSummer` becomes equal to 160".

You can also assign a variable when you declare it.

~~~java
int daysUntilSummer = 160;
~~~

This is good practice because it's more concise and leaves less room for confusion. There are situations where you can't assign to a variable at declaration, so don't worry too much if you can't.

Variables can be reassigned to.

~~~java
int daysUntilSummer = 160;
System.out.println("There are " + daysUntilSummer + "days until summer");
daysUntilSummer = 155;
System.out.println("Now there are " + daysUntilSummer + "days until summer");
~~~

It gets cooler. Variables can be reassigned to using their current value.

~~~java
int daysUntilSummer = 160;
System.out.println("There are " + daysUntilSummer + "days until summer");
daysUntilSummer = daysUntilSummer - 10;
System.out.println("Now there are " + daysUntilSummer + "days until summer");
~~~

On line 3, we can see that the right hand side of the operation is an expression, which evaluates to `150`. That result is then assigned to `daysUntilSummer`.

## Exercise 1

Let's put our new skills to use. Time for another program to copy.

~~~java
public class Ex1 {
    public static void main(String[] args) {
        String name = "Mr. McFalls";
        int age = 24;
        double height = 1.8796; // m
        double weight = 82.74; // kg
        double bmi = weight / (height * height);
        String eyeColor = "hazel";
        String hairColor = "brown";

        System.out.println("Let's talk about " + name + ".");
        System.out.println(name + " is " + age + " years old, is " + height
            + "m tall and weighs " + weight + "kg.");
        System.out.println("This makes " + name + "'s BMI " + bmi + "!");
        System.out.println(name + " also has " + eyeColor + " eyes and "
            + hairColor + " hair.");
    }
}
~~~

Once you get it compiling and can run it, replace my information with yours. You can see the strength of variables here. We can change the name in just one place and see it updated all over the output. Also we can automatically calculate the BMI based on the input. Now we're starting to see some real power!

Let's make some observations.

* I use`int` for age, which is a 32-bit type, but `double` for height, weight, and BMI, which is 64-bits. Why do I do this? Try changing the type to `float` and see what happens.

* The statements that begin on lines 12 and 15 are split into two lines. This is perfectly allowed, remember I can have as many separators as I want, and doing this keeps my line length short so I can read it in a small window.

* When I want to use two words for a variable, I write is using **camelCase** to distinguish the words. Remember that a space is a separator, so that would split my identifier. camelCase is standard in Java, but other languages have different best practices.

* Even though BMI is an abbreviation, I treat it like a word as far as capitalization goes. So I would also use `myBmi` if it was two or more words. This might look weird, but it is a consistent standard that you will get used to.

## Naming things

> There are only two hard things in Computer Science: cache invalidation, naming things, and off-by-one errors.

The original quote is by Phil Karlton, unknown who added the joke at the end.

Joking aside, naming things is quite hard. If you name all your variables `a`, `b`, or `thingy`, it will be very hard to maintain your code because you won't remember which variable represents what. But the problem goes deeper than that, if you choose a misleading name, you may think you have some type of data but you don't.

For example, in Exercise 1, I used the variable `height`. In truth, this would have been better named as `heightM`, to denote height in meters. If I don't specify, then I will question what units I used two months later when I need to fix a bug.

I can't really grade you on this kind of thing, but it's important to keep an eye on this and choose good variable names. Asking for help on what to name something is not a silly question!
