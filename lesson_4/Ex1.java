public class Ex1 {
    public static void main(String[] args) {
        String name = "Mr. McFalls";
        int age = 24;
        double height = 1.8796; // m
        double weight = 82.74; // kg
        double bmi = weight / (height * height);
        String eyeColor = "hazel";
        String hairColor = "brown";

        System.out.println("Let's talk about " + name + ".");
        System.out.println(name + " is " + age + " years old, is " + height
            + "m tall and weighs " + weight + "kg.");
        System.out.println("This makes " + name + "'s BMI " + bmi + "!");
        System.out.println(name + " also has " + eyeColor + " eyes and "
            + hairColor + " hair.");
    }
}
