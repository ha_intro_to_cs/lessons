# Lesson 5: Input

Now we're getting to the real power. Everything we've done so far is processing and output, without the ability to take input from users, we're missing a key piece of programming. We're going to fix that problem now.

## Command Line Arguments

It is common for programs to handle arguments passed to them when they are started in a shell, we've been doing that with `javac`, `java`, and we saw other examples back in lesson 1.

Java gives us access to these arguments with the `args` argument to the main function. That's a confusing sounding sentence, but when you look at `main`, it makes sense.

### Exercise 0

~~~java
public class Ex0 {
    public static void main(String[] args) {
        System.out.println(args.length);
    }
}
~~~

I mentioned before that `args` is an **array**, which is a fixed size list of values of the same type. Looking at the declaration of `args`, we see the type is `String[]`, which tells you that it is an array of `String`s.

We can see the size of the array by accessing the `length` property. Try running this program a few different ways and see what you get.

~~~
> java Ex0
0
~~~

~~~
> java Ex0 hello
1
~~~

~~~
> java Ex0 hello there
2
~~~

(You can copy and past the following one to get the spaces)
~~~
> java Ex0 general     kenobi !
3
~~~

~~~
> java Ex0 sphinx of black quartz, judge my vow
7
~~~

---

If we want to see the contents of an array, we need the index operator. You can think of each element of a list as it's own variable. The index operator follows the form `args[x]`, where `x` is the position of the element in the list.

***svg array structure***

### Exercise 1

~~~java
public class Ex1 {
    public static void main(String[] args) {
        System.out.println(args[0]);
    }
}
~~~

I've been saying it for a while now, but it really matters here. Programmers start counting at 0. Starting at zero makes sense because of how the memory is laid out, but it also has some nice mathematical benefits that we will see later.

***svg showing pointer + 0***

Using `args[0]` (pronounced "args at 0") accesses the first element in the list. After knowing this, you should be able to see that this program just prints the first argument that it receives. Try it out!

~~~
> java Ex1 hello
hello
~~~

~~~
> java Ex1 too many words
too
~~~

What happens if we don't give it any arguments?

~~~
> java Ex1
Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 0
    at Ex1.main(Ex1.java:3)
~~~

We've created an **exception**! In Java, exceptions are a way of handing errors that occur at runtime and couldn't be identified at compile time. Since we don't know the size of this array at compile time, we can't catch errors like that there, they happen when you run the program.

Here we have an `ArrayIndexOutOfBoundsException`, that will happen if you ever try to access an element that doesn't exist. We will cover how to prevent or properly handle these errors later, for now, we can just let our program crash like this.

### Exercise 2

Let's make a program that takes a decimal number from the user and converts it to binary. That sure would have been handy a few months ago, wouldn't it?

~~~java
public class Ex2 {
    public static void main(String[] args) {
        int number = Integer.parseInt(args[0]);
        System.out.println(Integer.toBinaryString(number));
    }
}
~~~

This program reads the user's input as a `String`, converts it to an `int`, and prints the number in binary using built-in utility functions.

This program is extremely brittle though, if the user types a non-integer, it crashes. If the user doesn't give an argument, it crashes. You the programmer will be able to interpret the error, but users won't, so this is a big flaw that we'll fix later.

### Types of Command Line Arguments

When we want to create a program for use in a shell, we need to understand the conventions so we know what users expect. There are three main kinds of arguments.

* Positional

Positional arguments are named such because they always have the same position in the command. For example, in `Ex2`, the number is the first positional argument. It always has to be there and it has to be first. If we tried to run our program like `java Ex2 text 17`, we'd be out of luck.

* Optional

Optional arguments are optional, they will have default values that the program uses if the user does not specify. Suppose we wanted to add an option to set the user choose the base to convert to. Normally, you would specify the option like `java Ex2 17 --base 16`. The two dashes are used for the name of the option, the value is the next argument.

Sometimes people give their options one character names, and those are usually used with a single dash. `java Ex2 17 -b 16`.

Windows always likes to do things different from everyone else, so some Windows exclusive programs use a backslash. `java Ex2 17 \base 16`.

* Flag (or Switch)

Flag arguments are just like optionals, but they represent boolean values. Using the flag at all means true, omitting it means false, they don't have an extra value associated with them.

Parsing optionals and flags like this is more complex than you might think, by the end of the semester you should be able to handle this, but don't worry about it now. The only important form for the time being is positional arguments.

## Interactive Input

Sometimes you want to get input from the user while the program is already running. It could be super simple like asking for confirmation, or complex like filling out a large form. For those situations, we use interactive input. Let's see an example.

### Exercise 3

~~~java
import java.util.Scanner;

public class Ex3 {
    public static void main(String[] args) {
        System.out.println("Welcome to the character creator");

        Scanner in = new Scanner(System.in);

        System.out.print("Enter your character's name: ");
        String name = in.nextLine();

        System.out.print("Enter your character's attack power: ");
        double attackPower = in.nextDouble();

        System.out.print("Enter your character's HP: ");
        int hp = in.nextInt();

        System.out.println();
        System.out.println("This is the character you created.");
        System.out.println("Name: " + name);
        System.out.println("Attack Power: " + attackPower);
        System.out.println("HP: " + hp);
    }
}
~~~

Compile and run the program. You will see the program waiting for your input. Type whatever you like and press Enter after each prompt to see the result.

There's a lot of new stuff going on here.

First of all, you probably immediately noticed that `import` statement at the top. `import` is a keyword that tells Java to include someone else's code in your program. This is in the standard library and included by default, you can see that it starts with `java`. Technically, `System` is actually called `java.lang.System`, they come from the same place, but we don't need to manually import it.

`System.out` and `System.in` are both **byte streams**, you can guess their purpose. Outputting is much easier than inputting, we give Java the information about what to output and it figures out the bytes to send. When we read input, we need to figure out how many bytes to read, and then what to do with them. I *could* bring the course to a halt so we could learn how to do this, but I don't think that's productive, instead we'll use a standard tool to solve our problem.

If you're really curious, you can read the documentation for the input / output streams here: https://docs.oracle.com/javase/8/docs/api/java/lang/System.html

Next, you should see the `new` keyword. This is needed to create variables that have non-primitive types. Remember that primitive types are built into the language itself. ***(do we remember that?)*** `java.util.Scanner` is a new type created with more Java code. You can read the documentation here: https://docs.oracle.com/javase/8/docs/api/java/util/Scanner.html.

When we create a variable with a primitive type, we can rely on the syntax to let us specify the value. Non-primitive types are at least a little more complicated to create, we use a function called a **constructor** to create them. Java dictates that when we use a constructor we have to say `new` first. I'm not actually sure why this is, other languages don't require it. Without it, Java just thinks we're calling a normal function and it won't be able to find it.

To sum up, use the `new` keyword whenever you're creating a new value of a non-primitive type.

So we create a new `Scanner` to handle our input. The constructor takes one argument, the stream to scan. Here we want to use `System.in`, that's where the shell passes user input. We could use a `Scanner` on a file, or on a network device, or even something custom, so that's why we need to specify.

Next we use `System.out.print` instead of `println`, because we *don't* want a newline here, we want the user's input to be on the same line as the prompt.

`Scanner` gives us nice functions we can use to try to extract types of data from user input. This means we don't have to manually convert like in the previous exercise, but the result is still the same, if the user gives us invalid data, our program will crash. Try it out.

You can read different types of data into variables and then do whatever you want with them. Here we just print them back out, but very soon we will have the ability to do more interesting things.

## Closing

Handling even text-based input is complex, we've barely scratched the surface here. We will cover a few more scenarios later in the course, but don't get discouraged if this feels like magic, there's a lot of complicated stuff that is hidden by Java's helpful tools.

***rework***
