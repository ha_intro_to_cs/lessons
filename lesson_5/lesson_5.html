<!DOCTYPE html>
<meta charset="UTF-8">
<html>
<head>
    <link href="../common/prism.css" rel="stylesheet" />
    <link rel="stylesheet" href="../common/styles.css">
</head>
<script src="../common/prism.js"></script>
<script src="../common/settings.js"></script>
<body>
<div class="centered" style="max-width: 55em;">

<h1>
    Lesson 5: Input
</h1>

<p>
    Now we're getting to the real power. Everything we've done so far is processing and output, without the ability to take input from users, we're missing a key piece of programming. We're going to fix that problem now.
</p>

<h2>
    Command Line Arguments
</h2>

<p>
    It is common for programs to handle arguments passed to them when they are started in a shell, we've been doing that with <code>javac</code>, <code>java</code>, and we saw other examples back in lesson 1.
</p>

<p>
    Java gives us access to these arguments with the <code>args</code> argument to the main function. That's a confusing sounding sentence, but when you look at <code>main</code>, it makes sense.
</p>

<h3>
    Exercise 0
</h3>

<pre class="language-java line-numbers"><code>
    public class Ex0 {
        public static void main(String[] args) {
            System.out.println(args.length);
        }
    }

</code></pre>

<p>
    I mentioned before that <code>args</code> is an <strong>array</strong>, which is a fixed size list of values of the same type. Looking at the declaration of <code>args</code>, we see the type is <code>String[]</code>, which tells you that it is an array of <code>String</code>s.
</p>

<p>
    We can see the size of the array by accessing the <code>length</code> property. Try running this program a few different ways and see what you get.
</p>

<pre class="lang-xxx"><code>
    > java Ex0
    0
</code></pre>

<pre class="lang-xxx"><code>
    > java Ex0 hello
    1
</code></pre>

<pre class="lang-xxx"><code>
    > java Ex0 hello there
    2
</code></pre>

(You can copy and past the following one to get the spaces)
<pre class="lang-xxx"><code>
    > java Ex0 general     kenobi !
    3
</code></pre>

<pre class="lang-xxx"><code>
    > java Ex0 sphinx of black quartz, judge my vow
    7
</code></pre>

<hr>

<p>
    If we want to see the contents of an array, we need the index operator. You can think of each element of a list as it's own variable. The index operator follows the form <code>args[x]</code>, where <code>x</code> is the position of the element in the list. <code>x</code> must be an <code>int</code>, no other type will work. It can be literal, but still needs to be <code>int</code>.
</p>

<div class="center-children">
    <img src="img/array.svg" style="width: 50%;">
</div>
<div class="center-children"><span><em>Depiction of an array</em></span></div>

<h3>
    Exercise 1
</h3>

<pre class="language-java line-numbers"><code>
    public class Ex1 {
        public static void main(String[] args) {
            System.out.println(args[0]);
        }
    }

</code></pre>

<p>
    I've been saying it for a while now, but it really matters here. Programmers start counting at 0, so arrays start at 0. The <em>why</em> is a very interesting question that goes back to the 1960s with a language called BCPL. Do your own research and let's discuss!
</p>

<p>
    As a side note, there are a few languages that start with 1. Most notably MATLAB, which is used by foul mathematicians (you'll never catch <em>me</em> messing with that dark art), and Lua, which is fairly popular in game making. So the truth is that <em>most</em> programmers start at 0, but Java definitely does, so that's what we need to use.
</p>

<blockquote>
    <p>
        To anyone who prefers 1-based indexing: you are the same people who screwed up the calendar, starting at year 1 and calling the 1900s the 20th century (and arguing that the year 2001 ought to be the start of the next millennium). Haven't you done enough damage? :-)
    </p>
    <footer>—<a href=https://python-history.blogspot.com/2013/10/why-python-uses-0-based-indexing.html>Guido van Rossum</a></footer>
</blockquote>

<p>
    Using <code>args[0]</code> (pronounced "args at 0") accesses the first element in the list. After knowing this, you should be able to see that this program just prints the first argument that it receives. Try it out!
</p>

<pre class="lang-xxx"><code>
> java Ex1 hello
hello
</code></pre>

<pre class="lang-xxx"><code>
> java Ex1 too many words
too
</code></pre>

What happens if we don't give it any arguments?

<pre class="lang-xxx"><code>
> java Ex1
Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 0
    at Ex1.main(Ex1.java:3)
</code></pre>

<p>
    We've created an <strong>exception</strong>! In Java, exceptions are a way of handing errors that occur at runtime and couldn't be identified at compile time. Since we don't know the size of this array at compile time, we can't catch errors like that there, they happen when you run the program.
</p>

<p>
    Here we have an <code>ArrayIndexOutOfBoundsException</code>. This will happen if you ever try to access an element that doesn't exist. We will cover how to prevent or properly handle these errors next lesson, for now, we can just let our program crash like this.
</p>

<h3>
    Exercise 2
</h3>

<p>
    Let's make a program that takes a decimal number from the user and converts it to binary. That sure would have been handy a few lessons ago, wouldn't it?
</p>

<pre class="language-java line-numbers"><code>
    public class Ex2 {
        public static void main(String[] args) {
            int number = Integer.parseInt(args[0]);
            System.out.println(Integer.toBinaryString(number));
        }
    }

</code></pre>

<p>
    This program reads the user's input as a <code>String</code>, converts it to an <code>int</code>, and prints the number in binary using built-in utility functions.
</p>

<div class="note">
    <span class="note-h">
        Note
    </span>
    <p>
        Notice that <code>toBinaryString</code> is <code>static</code>
    </p>
</div>

<p>
    This program is extremely brittle though, if the user types a non-integer, it crashes. If the user doesn't give an argument, it crashes. You the programmer will be able to interpret the error, but users won't, so this is a big flaw that we'll fix next lesson.
</p>

<h3>
    Types of Command Line Arguments
</h3>

<p>
    When we want to create a program for use in a shell, we need to understand the conventions so we know what users expect. There are three main kinds of arguments.
</p>

<ul>
    <li>Positional</li>
</ul>

<p>
    Positional arguments are named such because they always have the same position in the command. For example, in <code>Ex2</code>, the number is the first positional argument. It always has to be there and it has to be first. If we tried to run our program like <code>java Ex2 text 17</code>, we'd be out of luck.
</p>

<ul>
    <li>Optional</li>
</ul>

<p>
    Optional arguments are optional, they will have default values that the program uses if the user does not specify. Suppose we wanted to add an option to set the user choose the base to convert to. Normally, you would specify the option like <code>java Ex2 17 --base 16</code>. The two dashes are used for the name of the option, the value is the next argument.
</p>

<p>
    Sometimes people give their options one character names, and those are usually used with a single dash. <code>java Ex2 17 -b 16</code>.
</p>

<p>
    Windows always likes to do things different from everyone else, so some Windows exclusive programs use a backslash. <code>java Ex2 17 \base 16</code>.
</p>

<ul>
    <li>Flag (or Switch)</li>
</ul>

<p>
    Flag arguments are just like optionals, but they represent boolean values. Using the flag at all means true, omitting it means false, they don't have an extra value associated with them.
</p>

<p>
    Parsing optionals and flags like this is more complex than you might think, by the end of the semester you should be able to handle this, but don't worry about it now. The only important form for the time being is positional arguments.
</p>

<h2>
    Interactive Input
</h2>

<p>
    Sometimes you want to get input from the user while the program is already running. It could be super simple like asking for confirmation, or complex like filling out a large form. For those situations, we use interactive input. Let's see an example.
</p>

<h3>
    Exercise 3
</h3>

<pre class="language-java line-numbers"><code>
    import java.util.Scanner;

    public class Ex3 {
        public static void main(String[] args) {
            System.out.println("Welcome to the character creator");

            Scanner in = new Scanner(System.in);

            System.out.print("Enter your character's name: ");
            String name = in.nextLine();

            System.out.print("Enter your character's attack power: ");
            double attackPower = in.nextDouble();

            System.out.print("Enter your character's HP: ");
            int hp = in.nextInt();

            System.out.println();
            System.out.println("This is the character you created.");
            System.out.println("Name: " + name);
            System.out.println("Attack Power: " + attackPower);
            System.out.println("HP: " + hp);
        }
    }

</code></pre>

<p>
    Compile and run the program. You will see the program waiting for your input. Type whatever you like and press Enter after each prompt to see the result.
</p>

<p>
    There's a lot of new stuff going on here.
</p>

<p>
    First of all, you probably immediately noticed that <code>import</code> statement at the top. <code>import</code> is a keyword that tells Java to include code from somewhere else in your program. This is in the standard library and available by default, you can see that it starts with <code>java</code>. Technically, <code>System</code> is actually called <code>java.lang.System</code>, they come from the same place, but Java always automatically imports it.
</p>

<p>
    <code>System.out</code> and <code>System.in</code> are both <strong>byte streams</strong>, you can guess their purpose. Outputting is much easier than inputting, we give Java the information about what to output and it figures out the bytes to send. When we read input, we need to figure out how many bytes to read, and then what to do with them. I <em>could</em> bring the course to a halt so we could learn how to do this, but I don't think that's productive, instead we'll use a standard tool to solve our problem.
</p>

<p>
    If you're really curious, you can read the documentation for the input / output streams <a href=https://docs.oracle.com/javase/8/docs/api/java/lang/System.html>here</a>.
</p>

<p>
    Next, you should see the <code>new</code> keyword with the <code>Scanner</code> class. You can read the documentation <a href=https://docs.oracle.com/javase/8/docs/api/java/util/Scanner.html>here</a>. The Scanner class manages our byte stream and gives us functions that we can use to manipulate that data. This is much easier than doing all that ourselves. Hopefully the benefits of classes will become clearer after this exercise.
</p>

<p>
    So we create a <code>Scanner</code> object to handle our input. The constructor takes one argument, the stream to scan. Here we want to use <code>System.in</code>, that's where the shell passes user input. We could use a <code>Scanner</code> on a file, or on a network device, or even something custom, so that's why we need to specify.
</p>

<p>
    Next we use <code>System.out.print</code> instead of <code>println</code>, because we <em>don't</em> want a newline here, we want the user's input to be on the same line as the prompt.
</p>

<p>
    <code>Scanner</code> gives us nice functions we can use to try to extract types of data from user input. This means we don't have to manually convert like in the previous exercise, but the result is still the same, if the user gives us invalid data, our program will crash. Try it out.
</p>

<p>
    You can read different types of data into variables and then do whatever you want with them. Here we just print them back out, but very soon we will have the ability to do more interesting things.
</p>

<h2>
    More Input Handling?
</h2>

<p>
    Handling even text-based input is complex, we've barely scratched the surface here. We will cover a few more scenarios later in the course, but don't get discouraged if this feels like magic, there's a lot of complicated stuff that is hidden by Java's helpful tools. You will learn these more complicated scenarios when you become advanced enough to need them, without some more experience working with input, you won't be able to understand why we might do different things. Press on and continue learning every day!
</p>

</div>
</body>
</html>
